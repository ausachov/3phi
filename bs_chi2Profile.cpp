#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooExtendPdf.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"


using namespace RooFit;

double bgr(double *x, double *par)  {
        double xx = x[0];
 	return par[0]*(TMath::Exp(par[1]*xx));
}



double FitFunc0(double *x, double *par) 
{
        double val = 0;
	val+=par[0]*TMath::Gaus(x[0],par[1],par[2], kTRUE);	
	val += bgr(x,&par[3]);
	return val;
}

double FitFunc1(double *x, double *par) 
{
        double val = 0;
	val+=par[0]*(par[4]*TMath::Gaus(x[0],par[1],par[2], kTRUE)+(1-par[4])*TMath::Gaus(x[0],par[1],par[2]*par[3], kTRUE));	
	val += bgr(x,&par[5]);
	return val;
}



//0 - 1 DOF
//1 - 2 DOF
//2 - 3 DOF

void bs(int Type){
  
  
	Float_t BsMass = 5366.77; // ±0.24 meV

	Float_t minMass = 5250;
	Float_t maxMass = 5500;
	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar varMass("B_m_scaled", "B_m_scaled", minMass, maxMass, "MeV");
	RooRealVar Counts("Counts","Counts",0,100);
	RooDataSet DataSet("DataSet","DataSet",RooArgSet(varMass,Counts),StoreAsymError(RooArgSet(varMass,Counts)));
  
	Float_t xx[1000], yy[1000], ey1[1000], ey2[1000], xx[1000], yy[1000], ex1[1000], ex2[1000], sigmas[1000];
   	FILE *fp = fopen("triPhiPureOut.txt", "r");
	for(int i=0;i<binN;i++)
	{
	    fscanf(fp,"%f",&xx[i]);
	    fscanf(fp,"%f",&yy[i]);
	    fscanf(fp,"%f",&ex1[i]);
	    fscanf(fp,"%f",&ex2[i]);
	    fscanf(fp,"%f",&ey1[i]);
	    fscanf(fp,"%f",&ey2[i]);
	    varMass = xx[i];
	    varMass.setAsymError(-ex1[i],ex2[i]);
	    
	    Counts = yy[i];
	    Counts.setAsymError(-ey1[i],ey2[i]);
	    
	    DataSet.add(RooArgSet(varMass,Counts));

	}
	fclose(fp);


	RooPlot* frame = varMass.frame(Title("3#phi pure")) ;


	
	RooRealVar varBsNumber("N(B_{s})", "varBsNumber", 30, 0, 10000);
 	RooRealVar varBsSigma("#sigma", "varBsSigma", 13, 1, 40);
//	RooRealVar varBsSigma("#sigma", "varBsSigma", 9.84);
	RooRealVar varBsMass("M(B_{s})", "varBsMass", BsMass, BsMass-10, BsMass+10);
	RooGaussian pdfBsN("pdfBsN", "pdfBsN", varMass, varBsMass, varBsSigma);
	
	RooRealVar FractionN("FractionN","FractionN",0.853);
	RooRealVar SigmaRatio("SigmaRatio","SigmaRatio",2.2);
	
	RooFormulaVar varBsSigmaW("varBsSigmaW","varBsSigmaW","@0*@1",RooArgSet(varBsSigma,SigmaRatio));
	RooGaussian pdfBsW("pdfBsW", "pdfBsW", varMass, varBsMass, varBsSigmaW);
	
	RooAddPdf pdfBs("pdfBs","pdfBs",RooArgList(pdfBsN,pdfBsW),RooArgSet(FractionN));

	RooRealVar varNBgr("varNBgr", "varNBgr", 50, 0, 1e3);
	RooRealVar varE("varE", "varE", 0);
	RooExponential pdfBgr("pdfBgr", "pdfBgr", varMass, varE);

	
//if(Type==1)
//	RooAddPdf pdfModel("pdfModel", "pdfModel", RooArgList(pdfBsN, pdfBgr), RooArgList(varBsNumber, varNBgr));
//if(Type==0)
	RooAddPdf pdfModel("pdfModel", "pdfModel", RooArgList(pdfBs, pdfBgr), RooArgList(varBsNumber, varNBgr));

    //RooChi2Var chi2Var("chi2Var","chi2Var",)
    
	RooFitResult* results = pdfModel.chi2FitTo(DataSet,YVar(Counts),PrintLevel(0),Save(true));
    
    Double_t sigmaFit = varBsSigma.getVal();
    Double_t sigmaFitErrLo = varBsSigma.getErrorLo();
    Double_t sigmaFitErrHi = varBsSigma.getErrorHi();

    Double_t sigmaExternal = 13.64*9.49/11.66;
    
    if(Type==0)
    {
        varBsMass.setVal(5366.13);
        varBsSigma.setVal(sigmaExternal);
        
        varBsSigma.setConstant(kTRUE);
        varBsMass.setConstant(kTRUE);
    }
    
    if(Type==1)
    {
        varBsMass.setVal(5366.13);
        varBsMass.setConstant(kTRUE);
    }
    
    

    results = pdfModel.chi2FitTo(DataSet,YVar(Counts),PrintLevel(0),Save(true));
    Double_t optNLL = results->minNll();


	

    varBsNumber.setConstant(kTRUE);
	

	Float_t x[1000], y[1000];
    Float_t BgDeltaChi2 = 0;
	int NPoints = 100;

	for(int i=0;i<NPoints;i++)
	{ 	  

  	   x[i]=i;
	   varBsNumber.setVal(10*x[i]);
        
        
	   results = pdfModel.chi2FitTo(DataSet,YVar(Counts),PrintLevel(0),Save(true));
       y[i] = results->minNll() -optNLL;
        if(i==0) BgDeltaChi2 = y[i];
        
        
       sigmas[i] = varBsSigma.getVal();
	}
	TGraph* NllGr = new TGraph(NPoints, x, y);
	TCanvas* canvGr = new TCanvas("canvGr", "canvGr", 900, 700);
	NllGr->GetXaxis()->SetRangeUser(x[0],x[NPoints-1]);
	NllGr->SetMinimum(0);

	NllGr->SetLineColor(2);
	NllGr->SetLineWidth(2);
	NllGr->Draw();
	
 	
 	for(int i=0;i<NPoints;i++)cout<<x[i]<<"   "<<y[i]<<"   "<<sigmas[i]<<endl;
  
    
    cout<<"Delta Chi2 = "<<BgDeltaChi2<<endl;
    cout<<"SigmaFit = "<<sigmaFit<<"+"<<sigmaFitErrHi<<sigmaFitErrLo<<";  SigmaExternal = "<<sigmaExternal<<endl;
	
}



