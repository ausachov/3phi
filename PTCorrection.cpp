
using namespace RooFit;


PTCorrection(Int_t nch=4){
  
  
	gROOT->Reset();
	gStyle->SetOptStat(000);
	
	
	
	Double_t range0=0;
	Double_t range1=20e3;
	Double_t binWidth = (range1-range0)/nch;
	
	TChain * DecayTree2phiDataAll=new TChain("DecayTree");
	DecayTree2phiDataAll->Add("../2phi/Data_07_15/All2phi.root");
	
	
	Double_t BsMass = 5366.77; // ±0.24 meV

	Double_t minMass = 5250;
	Double_t maxMass = 5500;
	Double_t BinWidth = 10.;	
	Int_t binN = int((maxMass-minMass)/BinWidth);
	RooRealVar varMass("M(#phi#phi)", "M(#phi#phi)", minMass, maxMass, "MeV");	
	
	RooRealVar varBsNumber("N(B_{s})", "varBsNumber", 2560, 10, 1e6);
	RooRealVar varBsSigma("#sigma", "#sigma", 13,5,40);
	RooRealVar varBsMass("M(Bs)", "M(Bs)", 5366.7);
	RooGaussian pdfBs("pdfBs", "pdfBs", varMass, varBsMass, varBsSigma);

	RooRealVar varBgrNBs("varBgrNBs", "varBgrNBs", 10,1, 1e4);
	RooRealVar varC4("varC4", "varC4", 0);
	RooExponential pdfBsBgr("pdfBsBgr", "pdfBsBgr", varMass, varC4);
//	RooGenericPdf pdfBsBgr("pdfBsBgr", "pdfBsBgr","1+(@0-5200)*@1", RooArgSet(varMass, varC4));

	RooAddPdf pdfModelBs("pdfModelBs", "pdfModelBs", RooArgList(pdfBs, pdfBsBgr), RooArgList(varBsNumber, varBgrNBs));
	
	TH1D *hist2phiData = new TH1D("2#phi Data","2#phi Data", nch, range0, range1);
	
	char filename[100], label[100];
	for(int ii=0;ii<nch;ii++)
	{
	  Double_t low = range0+ii*binWidth;
	  Double_t high = low+binWidth;

	  sprintf(label, "Jpsi_PT>%f&&Jpsi_PT<%f", low, high);
	  
	  TFile *newfile = new TFile("2phi_afterCut.root","recreate");
	  TTree* DecayTree2phiData = DecayTree2phiDataAll->CopyTree(label);
	
	  newfile->Write();
	  newfile->Close();

	
	  gROOT->ProcessLine(".L diPhiPure.cpp");
	  gROOT->ProcessLine("diPhiPure(4,1)");
	  
	  TFile* file = new TFile("bsPureAll.root");
	  TH1D *histJpsiDiPhi  = (TH1D*) file->Get("histJpsiDiPhi");
	  RooDataHist* dset = new RooDataHist("dset", "dset", varMass, histJpsiDiPhi);	
	  pdfModelBs.chi2FitTo(*dset, Extended(kTRUE), Save(true), PrintLevel(0), NumCPU(4));

	  hist2phiData->SetBinContent(ii+1,varBsNumber.getVal());
	  hist2phiData->SetBinError(ii+1,varBsNumber.getError());


	  delete dset;
	  delete histJpsiDiPhi;
	  
 	  file->Close();
	  delete file;
	  delete newfile;
	}

	Double_t Branch_Value;
	
	
	TChain * DecayTree2phiMCAll=new TChain("DecayTree");
	DecayTree2phiMCAll->Add("../MC/CAL_0/Bs13/AllMC_Reduced.root");
	TTree* DecayTree2phiMC = DecayTree2phiMCAll->CopyTree("B_PT>0 && B_PT<25000");
	 
	Int_t NEnMC=DecayTree2phiMC->GetEntries();
	DecayTree2phiMC->SetBranchAddress("B_PT",&Branch_Value);
	TH1D *hist2phiMC = new TH1D("2#phi MC","2#phi MC", nch, range0, range1);
	for (Int_t i=0; i<NEnMC; i++) 
	{
		DecayTree2phiMC->GetEntry(i);
		hist2phiMC->Fill(Branch_Value);
	}


	TCanvas* canvC = new TCanvas("canvC", "canv", 800, 700);



	TChain * DecayTree3phiGenAll=new TChain("MCDecayTreeTuple/MCDecayTree");
	DecayTree3phiGenAll->Add("../MC/GenSpectr/3phi/DVntuple.root");
	TTree* DecayTree3phiGen = DecayTree3phiGenAll->CopyTree("B_s0_TRUEPT>0 && B_s0_TRUEPT<25000");
	Int_t NEnGen=DecayTree3phiGen->GetEntries();
	Double_t Branch_Value1;
	DecayTree3phiGen->SetBranchAddress("B_s0_TRUEPT",&Branch_Value1);
	TH1D *hist3phiGen = new TH1D("3#phi Gen","3#phi Gen", nch, range0, range1);
	for (Int_t i=0; i<NEnGen; i++) 
	{
		DecayTree3phiGen->GetEntry(i);
		hist3phiGen->Fill(Branch_Value1);
	}
	




	
	TChain * DecayTree3phiMCAll=new TChain("DecayTree");
	DecayTree3phiMCAll->Add("../MC/CAL_0/3phi/AllMC_Reduced.root");
	TTree* DecayTree3phiMC = DecayTree3phiMCAll->CopyTree("B_PT>0 && B_PT<25000");
	 
	Int_t NEn3MC=DecayTree3phiMC->GetEntries();
	Double_t Branch_Value2;
	DecayTree3phiMC->SetBranchAddress("B_PT",&Branch_Value2);
	TH1D *hist3phiMC = new TH1D("3#phi MC","3#phi MC", nch, range0, range1);
	TH1D *hist3phiMCNew = new TH1D("3#phi MC New","3#phi MC New", nch, range0, range1);
	
	for (Int_t i=0; i<NEn3MC; i++) 
	{
		DecayTree3phiMC->GetEntry(i);
		hist3phiMC->Fill(Branch_Value2);
	}	
	
	
	TChain * DecayTree2phiGenAll=new TChain("MCDecayTreeTuple/MCDecayTree");
	DecayTree2phiGenAll->Add("../MC/GenSpectr/Bs13/DVntuple.root");
	TTree* DecayTree2phiGen = DecayTree2phiGenAll->CopyTree("B_s0_TRUEPT>0 && B_s0_TRUEPT<25000");
	Int_t NEnGen=DecayTree2phiGen->GetEntries();
	Double_t Branch_Value3;
	DecayTree2phiGen->SetBranchAddress("B_s0_TRUEPT",&Branch_Value3);
	TH1D *hist2phiGen = new TH1D("2#phi Gen","2#phi Gen", nch, range0, range1);
	for (Int_t i=0; i<NEnGen; i++) 
	{
		DecayTree2phiGen->GetEntry(i);
		hist2phiGen->Fill(Branch_Value3);
	}
	
	
    hist2phiMC->Scale(1./hist2phiMC->Integral());
	hist2phiData->Scale(1./hist2phiData->Integral());	
	hist3phiGen->Scale(1./hist3phiGen->Integral());
	hist2phiGen->Scale(1./hist2phiGen->Integral());
	hist3phiMC->Scale(1./hist3phiMC->Integral());
	
	hist2phiGen->SetLineColor(kRed);
	hist2phiGen->Draw();
	hist2phiData->Draw("same");	
	hist2phiMC->Draw("same");	
	
// 
 	Double_t PtCorrection2phi = 0;
	Double_t dPtCorrection2phi = 0;
	
 	Double_t PtCorrection3phi = 0;
	Double_t dPtCorrection3phi = 0;
// 	
// 	
// 	hist2phiData->Draw();
//
//
    
    Double_t N_Lambda = 0;
    Double_t Norm = 0;
    Double_t PT = 0;
    Double_t RelErrSquared = 0;
    Double_t StatErr = 0;
    
    Double_t a = 0.404;
    Double_t b = 0.031;
    Double_t da = TMath::Sqrt(0.017**2+0.027**2+0.105**2);
    Double_t db = TMath::Sqrt(0.004**2+0.003**2);
    

//    Double_t dRs = 0.015/2.;
//    Double_t Rs = 0.5*0.259;
    Double_t dRs = TMath::Sqrt(0.004**2+0.010**2);
    Double_t Rs = 0.134;
    Double_t R_L = 0; Double_t dR_L = 0;
    Double_t fsAv = 0;
    
    
    Double_t dA = 1*TMath::Sqrt(0.016**2+0.024**2), dB = 1*TMath::Sqrt(0.040**2+0.101**2), dC = 1*TMath::Sqrt(0.007**2+0.014**2);
    Double_t A = 0.151, B = -0.573, C=-0.095;
    
	for(int i=0;i<nch;i++)
	{

	  Double_t Data2phiBin = hist2phiData->GetBinContent(i+1);
	  Double_t dData2phiBin = hist2phiData->GetBinError(i+1);
	  Double_t Gen2phiBin  = hist2phiGen->GetBinContent(i+1);
	  Double_t MC2phiBin = hist2phiMC->GetBinContent(i+1);
	  Double_t MC3phiBin = hist3phiMC->GetBinContent(i+1);


      PT = hist2phiData->GetBinCenter(i+1)/1000.;
      
        R_L = (A+TMath::Exp(B+C*PT))/2.;
        dR_L = 0.5*TMath::Sqrt(dA**2 + (dB*TMath::Exp(B+C*PT))**2 + (TMath::Exp(B+C*PT)*PT*dC)**2);
//      R_L = a*(1-b*PT);
//      dR_L = TMath::Sqrt(da*(1-b*PT)*da*(1-b*PT) + a*PT*db*a*PT*db);
        
        cout<<"bin "<<i<<" "<<Rs/(1+Rs+R_L)<<endl;
        
      fsAv+=(Data2phiBin)/(MC2phiBin)*Gen2phiBin*Rs/(1+Rs+R_L);
      StatErr += (Data2phiBin)/(MC2phiBin)*Gen2phiBin*(Data2phiBin)/(MC2phiBin)*Gen2phiBin*((1+R_L)*dRs*(1+R_L)*dRs  + dR_L*dR_L)/(1+Rs+R_L)/(1+Rs+R_L)/(1+Rs+R_L)/(1+Rs+R_L);


      Norm += (Data2phiBin)/(MC2phiBin)*Gen2phiBin;
	  
//      PtCorrection3phi+=(Gen2phiBin)*(Data2phiBin)/(MC2phiBin);
//      dPtCorrection3phi+=(Gen2phiBin)*(dData2phiBin)/(MC2phiBin)*(Gen2phiBin)*(dData2phiBin)/(MC2phiBin);
        
        
	  PtCorrection3phi+=(Data2phiBin)*(MC3phiBin)/(MC2phiBin);
        cout<<i<<"   "<<Data2phiBin/(MC2phiBin)<<endl;
	  dPtCorrection3phi+=(dData2phiBin)*(MC3phiBin)/(MC2phiBin)*(dData2phiBin)*(MC3phiBin)/(MC2phiBin);
	}
    
    fsAv = fsAv/Norm;
    StatErr = TMath::Sqrt(StatErr)/Norm;
    
	Double_t dPtCorrection3phi=sqrt(dPtCorrection3phi);
	cout<<"PtCorrection="<<PtCorrection3phi<<" +- "<<dPtCorrection3phi<<endl;
    cout<<"fs = "<<fsAv<<"  +-   "<<StatErr<<endl;
}

