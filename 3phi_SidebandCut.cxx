// $Id: $
{
  TChain *chain = new TChain("DecayTree");
  chain->Add("DTF_Data/AllStat3phi.root");
  
  
  TCut RightCut("B_MM>5415 && B_MM<5815");
  TCut LeftCut("B_MM>4925 && B_MM<5325");
  TCut TotCut = RightCut || LeftCut;
  
  TFile *newfile = new TFile("3phi_BsSidebandCut.root","recreate");
  TTree *newtree = chain->CopyTree(TotCut);
 
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
  
}

