/*
 * bs.cpp
 *
 *  Created on: May 31, 2013
 *      Author: maksym
 */
using namespace RooFit;

void bs(){
	gROOT->Reset();
	gROOT->SetStyle(00);
	TProof::Open("");

	Float_t BsMass = 5366.77; // ±0.24 meV

	Float_t minMass = 5250;
	Float_t maxMass = 5500;
	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar varMass("B_m_scaled", "B_m_scaled", minMass, maxMass, "MeV");
	RooRealVar phi("phi", "phi", -5, 5);

//	unbinned data set:
	TChain * DecayTree=new TChain("DecayTree");
	DecayTree->Add("Data_NewTrig/AllStat3phi.root");
//	chain->Add("Reduced_MagUp2012.root");
//	chain->Add("Reduced_MagDown2012.root");
	RooDataSet* dset = new RooDataSet("dset", "dset", DecayTree, varMass);

	RooPlot* frame = varMass.frame(Title("Bs candidates"));

	RooRealVar varBsNumber("N(B_{s})", "varBsNumber", 100, 0, 1e7);
	RooRealVar varBsSigma("#sigma", "varBsSigma", 13, 4, 40);
	RooRealVar varBsMass("M(B_{s})", "varBsMass", BsMass, BsMass-10, BsMass+10);
	RooGaussian pdfBs("pdfBs", "pdfBs", varMass, varBsMass, varBsSigma);

	RooRealVar varBgrNBs("varBgrNBs", "varBgrNBs", 0, 1e3);
	RooRealVar varC4("varC4", "varC4", -10, +10);
	RooExponential pdfBsBgr("pdfBsBgr", "pdfBsBgr", varMass, varC4);

	RooAddPdf pdfModelBs("pdfModelBs", "pdfModelBs", RooArgList(pdfBs, pdfBsBgr), RooArgList(varBsNumber, varBgrNBs));
	RooFitResult* results2 = pdfModelBs.fitTo(*dset, Save(true), PrintLevel(0), NumCPU(4));
	RooPlot* frame3 =varMass.frame();

	dset->plotOn(frame3, Binning(binN, minMass, maxMass));
 	pdfModelBs.plotOn(frame3);
	RooArgSet showParams;
	showParams.add(varBsNumber);
	showParams.add(varBsSigma);
	showParams.add(varBsMass);
//	pdfModelBs.paramOn(frame3, Layout(0.65, 0.95, 0.95), Parameters(showParams), ShowConstants(true));

	TCanvas* canvB = new TCanvas("canvB", "canvB", 1000, 600);
//	canvB->Divide(1, 2);
//	canvB->cd(1);
	frame3->Draw();

	
}



