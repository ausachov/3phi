#include <TMath.h>




void Dalitz()
{
  gROOT->Reset();
  
  Double_t range0=4e6;
  Double_t range1=2e7;
  
  Double_t PhiMass = 1019.455;
  Double_t BsMass = 5366.77;
  
  Int_t nch=150;
  gStyle->SetOptStat(111);
  gROOT->ProcessLine(".x lhcbStyle.C");
  
  TChain * DecayTree=new TChain("DecayTree");
  DecayTree->Add("3phi_BsMCut.root");
  const Int_t NEn=DecayTree->GetEntries();
  Double_t PX1, PY1, PZ1, M1, PE1;
  Double_t PX2, PY2, PZ2, M2, PE2;
  Double_t PX3, PY3, PZ3, M3, PE3;
  Double_t PX, PY, PZ, PE;
  
  Float_t k1PX1, k1PY1, k1PZ1, k1PE1;
  Float_t k1PX2, k1PY2, k1PZ2, k1PE2;
  Float_t k1PX3, k1PY3, k1PZ3, k1PE3;
  
  Float_t k2PX1, k2PY1, k2PZ1, k2PE1;
  Float_t k2PX2, k2PY2, k2PZ2, k2PE2;
  Float_t k2PX3, k2PY3, k2PZ3, k2PE3;
  

  
  Float_t k3PX1, k3PY1, k3PZ1, k3PE1;
  Float_t k3PX2, k3PY2, k3PZ2, k3PE2;
  Float_t k3PX3, k3PY3, k3PZ3, k3PE3;
  
  Double_t PX12, PY12, PZ12, PE12, M12;
  Double_t PX13, PY13, PZ13, PE13, M13;
  Double_t PX23, PY23, PZ23, PE23, M23;
  
  Double_t PX, PY, PZ, PE;
  
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PX",&k1PX1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PY",&k1PY1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PZ",&k1PZ1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PE",&k1PE1);

  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_0_PX",&k1PX2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_0_PY",&k1PY2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_0_PZ",&k1PZ2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_0_PE",&k1PE2);
  
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_PX",&k2PX1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_PY",&k2PY1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_PZ",&k2PZ1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_PE",&k2PE1);

  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_0_PX",&k2PX2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_0_PY",&k2PY2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_0_PZ",&k2PZ2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_0_PE",&k2PE2);
  
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_PX",&k3PX1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_PY",&k3PY1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_PZ",&k3PZ1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_PE",&k3PE1);

  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_0_PX",&k3PX2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_0_PY",&k3PY2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_0_PZ",&k3PZ2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_0_PE",&k3PE2);
  
    
  ULong64_t evNumber;  
  DecayTree->SetBranchAddress("eventNumber",&evNumber);
  TH2D * Dalitz_hist = new TH2D("Dalitz 3#phi","Dalitz plot" , nch, range0, range1,nch, range0, range1);
  
  
  Double_t DalX [5000];
  Double_t DalY[5000];
  
  
  Double_t Tav = BsMass/3 - PhiMass;

  
  for (Int_t i=0; i<NEn; i++) 
  {
	  DecayTree->GetEntry(i);
	  
	  
	  
	  PE12 = k1PE1 + k1PE2 + k2PE1 +k2PE2;
	  PZ12 = k1PZ1 + k1PZ2 + k2PZ1 +k2PZ2;
	  PX12 = k1PX1 + k1PX2 + k2PX1 +k2PX2;
	  PY12 = k1PY1 + k1PY2 + k2PY1 +k2PY2;
	  M12 = (PE12*PE12-PX12*PX12-PZ12*PZ12-PY12*PY12);
	  
	  PE13 = k1PE1 + k1PE2 + k3PE1 +k3PE2;
	  PZ13 = k1PZ1 + k1PZ2 + k3PZ1 +k3PZ2;
	  PX13 = k1PX1 + k1PX2 + k3PX1 +k3PX2;
	  PY13 = k1PY1 + k1PY2 + k3PY1 +k3PY2;
	  M13 = (PE13*PE13-PX13*PX13-PZ13*PZ13-PY13*PY13); 
	  
	  PE23 = k3PE1 + k3PE2 + k2PE1 +k2PE2;
	  PZ23 = k3PZ1 + k3PZ2 + k2PZ1 +k2PZ2;
	  PX23 = k3PX1 + k3PX2 + k2PX1 +k2PX2;
	  PY23= k3PY1 + k3PY2 + k2PY1 +k2PY2;
	  M23 = (PE23*PE23-PX23*PX23-PZ23*PZ23-PY23*PY23); 
	  
	  Dalitz_hist->Fill(M12,M13);
	  Dalitz_hist->Fill(M23,M12);
	  Dalitz_hist->Fill(M13,M23);
	  Dalitz_hist->Fill(M13,M12);
	  Dalitz_hist->Fill(M12,M23);
	  Dalitz_hist->Fill(M23,M13);
	  
	  
	  Double_t Tcm1, Tcm2, Tcm3;
	  Double_t a,b,c;
	  
	  a = (-M23+BsMass*BsMass+PhiMass*PhiMass)/(2*BsMass) - PhiMass;
	  b = (-M13+BsMass*BsMass+PhiMass*PhiMass)/(2*BsMass) - PhiMass;
	  c = (-M12+BsMass*BsMass+PhiMass*PhiMass)/(2*BsMass) - PhiMass;
	  
	  
	  switch (i%6) 
	  {
	      case 0:
		Tcm1 = a;
		Tcm2 = b;
		Tcm3 = c;
		break;
	      case 1:
		Tcm1 = a;
		Tcm2 = c;
		Tcm3 = b;
		break;
	      case 2:
		Tcm1 = b;
		Tcm2 = a;
		Tcm3 = c;
		break;
	      case 3:
		Tcm1 = b;
		Tcm2 = c;
		Tcm3 = a
		;
		break;
	      case 4:
		Tcm1 = c;
		Tcm2 = a;
		Tcm3 = b;
		break;
	      case 5:
		Tcm1 = c;
		Tcm2 = b;
		Tcm3 = a;
		break;
	  }
	  
	  
	  
	  
	  DalX[3*i] = (Tcm1-Tcm2)/TMath::Sqrt(3.)/Tav;   DalY[3*i] = (Tcm3)/Tav - 1;
 	  DalX[3*i+1] = (Tcm2-Tcm3)/TMath::Sqrt(3.)/Tav; DalY[3*i+1] = (Tcm1)/Tav - 1;
 	  DalX[3*i+2] = (Tcm3-Tcm1)/TMath::Sqrt(3.)/Tav; DalY[3*i+2] = (Tcm2)/Tav - 1;
// 	  DalX[6*i+3] = (Tcm2-Tcm1)/TMath::Sqrt(3.)/Tav; DalY[6*i+3] = (Tcm3)/Tav - 1;
//  	  DalX[6*i+4] = (Tcm3-Tcm2)/TMath::Sqrt(3.)/Tav; DalY[6*i+4] = (Tcm1)/Tav - 1;
//  	  DalX[6*i+5] = (Tcm1-Tcm3)/TMath::Sqrt(3.)/Tav; DalY[6*i+5] = (Tcm2)/Tav - 1;
  }
  
 
    
  TGraph* Dalitz_gr = new TGraph(3*NEn, DalX, DalY);
  Dalitz_gr->SetTitle("Dalitz Plot of B_{s} region");
  Dalitz_gr->GetXaxis()->SetTitle("X_{D} = #sqrt{3} (T1-T2)/Q");
  Dalitz_gr->GetYaxis()->SetTitle("Y_{D} = 3 T3/Q - 1");
  Dalitz_gr->GetXaxis()->SetLimits(-1.2,1.2);
  Dalitz_gr->SetMinimum(-1.2);
  Dalitz_gr->SetMaximum(1.2);
  Dalitz_gr->Draw("AP");
  
  
  cout<<NEn<<endl;
  //Dalitz_hist->Draw("COLZ");
  
}


void DalitzSB()
{
    gROOT->Reset();
    
    Double_t range0=4e6;
    Double_t range1=2e7;
    
    Double_t PhiMass = 1019.455;
    Double_t BsMass = 5366.77;
    
    Int_t nch=150;
    gStyle->SetOptStat(111);
    gROOT->ProcessLine(".x lhcbStyle.C");
    
    TChain * DecayTree=new TChain("DecayTree");
    DecayTree->Add("3phi_BsSidebandCut.root");
    const Int_t NEn=DecayTree->GetEntries();
    Double_t PX1, PY1, PZ1, M1, PE1;
    Double_t PX2, PY2, PZ2, M2, PE2;
    Double_t PX3, PY3, PZ3, M3, PE3;
    Double_t PX, PY, PZ, PE;
    
    Float_t k1PX1, k1PY1, k1PZ1, k1PE1;
    Float_t k1PX2, k1PY2, k1PZ2, k1PE2;
    Float_t k1PX3, k1PY3, k1PZ3, k1PE3;
    
    Float_t k2PX1, k2PY1, k2PZ1, k2PE1;
    Float_t k2PX2, k2PY2, k2PZ2, k2PE2;
    Float_t k2PX3, k2PY3, k2PZ3, k2PE3;
    
    
    
    Float_t k3PX1, k3PY1, k3PZ1, k3PE1;
    Float_t k3PX2, k3PY2, k3PZ2, k3PE2;
    Float_t k3PX3, k3PY3, k3PZ3, k3PE3;
    
    Double_t PX12, PY12, PZ12, PE12, M12;
    Double_t PX13, PY13, PZ13, PE13, M13;
    Double_t PX23, PY23, PZ23, PE23, M23;
    
    Double_t PX, PY, PZ, PE;
    
    DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PX",&k1PX1);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PY",&k1PY1);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PZ",&k1PZ1);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PE",&k1PE1);
    
    DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_0_PX",&k1PX2);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_0_PY",&k1PY2);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_0_PZ",&k1PZ2);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_0_PE",&k1PE2);
    
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_PX",&k2PX1);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_PY",&k2PY1);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_PZ",&k2PZ1);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_PE",&k2PE1);
    
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_0_PX",&k2PX2);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_0_PY",&k2PY2);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_0_PZ",&k2PZ2);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_0_PE",&k2PE2);
    
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_PX",&k3PX1);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_PY",&k3PY1);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_PZ",&k3PZ1);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_PE",&k3PE1);
    
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_0_PX",&k3PX2);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_0_PY",&k3PY2);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_0_PZ",&k3PZ2);
    DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_0_PE",&k3PE2);
    
    
    ULong64_t evNumber;
    DecayTree->SetBranchAddress("eventNumber",&evNumber);
    TH2D * Dalitz_hist = new TH2D("Dalitz 3#phi","Dalitz plot" , nch, range0, range1,nch, range0, range1);
    
    
    Double_t DalX [20000];
    Double_t DalY[20000];
    
    
    Double_t Tav = BsMass/3 - PhiMass;
    
    
    for (Int_t i=0; i<NEn; i++)
    {
        DecayTree->GetEntry(i);
        
        
        
        PE12 = k1PE1 + k1PE2 + k2PE1 +k2PE2;
        PZ12 = k1PZ1 + k1PZ2 + k2PZ1 +k2PZ2;
        PX12 = k1PX1 + k1PX2 + k2PX1 +k2PX2;
        PY12 = k1PY1 + k1PY2 + k2PY1 +k2PY2;
        M12 = (PE12*PE12-PX12*PX12-PZ12*PZ12-PY12*PY12);
        
        PE13 = k1PE1 + k1PE2 + k3PE1 +k3PE2;
        PZ13 = k1PZ1 + k1PZ2 + k3PZ1 +k3PZ2;
        PX13 = k1PX1 + k1PX2 + k3PX1 +k3PX2;
        PY13 = k1PY1 + k1PY2 + k3PY1 +k3PY2;
        M13 = (PE13*PE13-PX13*PX13-PZ13*PZ13-PY13*PY13);
        
        PE23 = k3PE1 + k3PE2 + k2PE1 +k2PE2;
        PZ23 = k3PZ1 + k3PZ2 + k2PZ1 +k2PZ2;
        PX23 = k3PX1 + k3PX2 + k2PX1 +k2PX2;
        PY23= k3PY1 + k3PY2 + k2PY1 +k2PY2;
        M23 = (PE23*PE23-PX23*PX23-PZ23*PZ23-PY23*PY23);
        
        Dalitz_hist->Fill(M12,M13);
        Dalitz_hist->Fill(M23,M12);
        Dalitz_hist->Fill(M13,M23);
        Dalitz_hist->Fill(M13,M12);
        Dalitz_hist->Fill(M12,M23);
        Dalitz_hist->Fill(M23,M13);
        
        
        Double_t Tcm1, Tcm2, Tcm3;
        Double_t a,b,c;
        
        a = (-M23+BsMass*BsMass+PhiMass*PhiMass)/(2*BsMass) - PhiMass;
        b = (-M13+BsMass*BsMass+PhiMass*PhiMass)/(2*BsMass) - PhiMass;
        c = (-M12+BsMass*BsMass+PhiMass*PhiMass)/(2*BsMass) - PhiMass;
        
        
        switch (i%6)
        {
            case 0:
                Tcm1 = a;
                Tcm2 = b;
                Tcm3 = c;
                break;
            case 1:
                Tcm1 = a;
                Tcm2 = c;
                Tcm3 = b;
                break;
            case 2:
                Tcm1 = b;
                Tcm2 = a;
                Tcm3 = c;
                break;
            case 3:
                Tcm1 = b;
                Tcm2 = c;
                Tcm3 = a
                ;
                break;
            case 4:
                Tcm1 = c;
                Tcm2 = a;
                Tcm3 = b;
                break;
            case 5:
                Tcm1 = c;
                Tcm2 = b;
                Tcm3 = a;
                break;
        }
        
        
        
        
        DalX[3*i] = (Tcm1-Tcm2)/TMath::Sqrt(3.)/Tav;   DalY[3*i] = (Tcm3)/Tav - 1;
        DalX[3*i+1] = (Tcm2-Tcm3)/TMath::Sqrt(3.)/Tav; DalY[3*i+1] = (Tcm1)/Tav - 1;
        DalX[3*i+2] = (Tcm3-Tcm1)/TMath::Sqrt(3.)/Tav; DalY[3*i+2] = (Tcm2)/Tav - 1;
        // 	  DalX[6*i+3] = (Tcm2-Tcm1)/TMath::Sqrt(3.)/Tav; DalY[6*i+3] = (Tcm3)/Tav - 1;
        //  	  DalX[6*i+4] = (Tcm3-Tcm2)/TMath::Sqrt(3.)/Tav; DalY[6*i+4] = (Tcm1)/Tav - 1;
        //  	  DalX[6*i+5] = (Tcm1-Tcm3)/TMath::Sqrt(3.)/Tav; DalY[6*i+5] = (Tcm2)/Tav - 1;
    }
    
    //TCanvas *canv1 = new TCanvas("canv1","Dalitz",500,500);
    
    TGraph* Dalitz_gr = new TGraph(3*NEn, DalX, DalY);
    Dalitz_gr->SetTitle("Dalitz Plot of B_{s} Sideband");
    Dalitz_gr->GetXaxis()->SetTitle("X_{D} = #sqrt{3} (T1-T2)/Q");
    Dalitz_gr->GetYaxis()->SetTitle("Y_{D} = 3 T3/Q - 1");
    Dalitz_gr->GetXaxis()->SetLimits(-1.2,1.2);
    Dalitz_gr->SetMinimum(-1.2);
    Dalitz_gr->SetMaximum(1.2);
    Dalitz_gr->Draw("AP");
    
    
    cout<<NEn<<endl;
    
    //Dalitz_hist->Draw("COLZ");
    
}



void main()
{
     TCanvas *canv1 = new TCanvas("canv1","Dalitz",900,500);
    canv1->Divide(2,1);
    canv1->cd(1);
     Dalitz();
    canv1->cd(2);
    DalitzSB();
    
}