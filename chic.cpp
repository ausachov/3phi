/*
 * bs.cpp
 *
 *  Created on: May 31, 2013
 *      Author: maksym
 */
using namespace RooFit;

void chic(){
	gROOT->Reset();
	gROOT->SetStyle(00);
	TProof::Open("");

	Float_t BsMass = 5366.77; // ±0.24 meV

	Float_t etacMass =	2981.0; // ±1.1 MeV
	Float_t jpsiMass =	3096.92;
	Float_t chi0Mass =	3414.75; // ±0.31 MeV
	Float_t chi1Mass =	3510.66; // ±0.07 MeV <-- refer point
	Float_t hcMass   = 	3525.41; // ±0.16 MeV
	Float_t chi2Mass =	3556.20; // ±0.09 MeV
	Float_t etac2Mass =	3638.9; // ±1.3 MeV
	Float_t psi2sMass =	3686.11;
	Float_t psi3770Mass =	3773.15; // ±0.33 MeV
	Float_t x3872Mass = 	3871.69; // ±0.17 MeV
	Float_t x3915Mass = 	3918.4; // ±2.7 MeV
	
	
	Float_t etacGamma =	29; // ±1.1 MeV
	Float_t chi0Gamma =	10.4; // ±0.31 MeV
	Float_t chi1Gamma =	0.86; // ±0.07 MeV <-- refer point
	Float_t chi2Gamma =	1.98; // ±0.09 MeV
	Float_t etac2Gamma =	11.3; // ±1.3 MeV
	
//	Float_t var3PhiMass = 1019.446*3;

	Float_t minMass = 3000;
	Float_t maxMass = 5000;
	Float_t binWidth = 20.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar varMass("B_m_scaled", "B_m_scaled", minMass, maxMass, "MeV");

//	unbinned data set:
	TChain * DecayTree=new TChain("DecayTree");
	DecayTree->Add("Data_NewTrig/AllStat3phi.root");
//	chain->Add("Reduced_MagUp2012.root");
//	chain->Add("Reduced_MagDown2012.root");
	RooDataSet* dset = new RooDataSet("dset", "dset", DecayTree, varMass);

	RooPlot* frame = varMass.frame(Title("Bs candidates"));

	
	RooArgList listComp, listNorm;
	RooArgSet showParams;
	
	RooRealVar var3PhiMass("var3PhiMass", "var3PhiMass", 3058.38);
	RooRealVar varSigma("varSigma","varSigma",13);
	
// 	RooRealVar varEtacNumber("N(#eta_{c}(1S))", "varEtacNumber", 6800, 0, 5e4);
// 	RooRealVar varEtacMass("M(#eta_{c})", "varEtacMass", etacMass);
// 	RooRealVar varEtacGamma("varEtacGamma", "varEtacGamma", etacGamma);	
// 	RooGenericPdf pdfEtac("pdfEtac", "pdfEtac", "(@0>@1)*sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
// 			      RooArgList(varMass, var3PhiMass, varEtacMass,varSigma,varEtacGamma));
// 	
// 	listComp.add(pdfEtac);
// 	listNorm.add(varEtacNumber);
// 	showParams.add(varEtacNumber);
// //  	showParams.add(varEtacMass);
// //  	showParams.add(varEtacGamma);


	


	RooRealVar varChi0Number("N(#chi_{c0})", "varChi0Number", 1010, 0, 1e4);
	RooRealVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", chi0Mass);
	RooRealVar varChi0Gamma("varChi0Gamma", "varChi0Gamma", chi0Gamma);
	RooVoigtian pdfChi0("pdfChi0", "pdfChi0", varMass, varChi0Mass, varChi0Gamma, varSigma);
	listComp.add(pdfChi0);
	listNorm.add(varChi0Number);
	showParams.add(varChi0Number);
// 	showParams.add(varChi0Mass);

// 	RooRealVar varChi1Number("N(#chi_{c1})", "varChi1Number", 540, 0, 1e3);
//         RooRealVar varChi1Mass("M(#chi_{c1})", "varChi1Mass", chi1Mass);
// 	RooRealVar varChi1Gamma("varChi1Gamma", "varChi1Gamma", chi1Gamma);
// 	RooVoigtian pdfChi1("pdfChi1", "pdfChi1", varMass, varChi1Mass, varChi1Gamma, varSigma);
// 	listComp.add(pdfChi1);
// 	listNorm.add(varChi1Number);
// 	showParams.add(varChi1Number);
//	showParams.add(varChi1Mass);


	RooRealVar varChi2Number("N(#chi_{c2})", "varChi2Number", 660, 0, 1e3);
        RooRealVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", chi2Mass);
	RooRealVar varChi2Gamma("varChi2Gamma", "varChi2Gamma",chi2Gamma);
	RooVoigtian pdfChi2("pdfChi2", "pdfChi2", varMass, varChi2Mass, varChi2Gamma, varSigma);
	listComp.add(pdfChi2);
	listNorm.add(varChi2Number);
	showParams.add(varChi2Number);
// 	showParams.add(varChi2Mass);

	RooRealVar varEtac2Number("N(#eta_{c}(2S))", "varEtac2Number", 310, 0, 1e3);
	RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", etac2Gamma);
	RooRealVar varEtac2Mass("M(#eta_{c}(2S))", "varEtac2Mass", etac2Mass);	
	RooVoigtian pdfEtac2("pdfEtac2", "pdfEtac2", varMass, varEtac2Mass, varEtac2Gamma, varSigma);
	
	listComp.add(pdfEtac2);
	listNorm.add(varEtac2Number);
	showParams.add(varEtac2Number);
// 	showParams.add(varEtac2Mass);
// 	showParams.add(varEtac2Gamma);
	
	
	RooRealVar varC0("varC0","varC0",-1,1);
	RooRealVar varBgrNumber("varBgrNumber","varBgrNumber",0,1000);
 	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "(@0>@2)*exp(@0*@1)*sqrt(@0-@2)",
 			RooArgSet(varMass, varC0, var3PhiMass));


	listComp.add(pdfBgr);
	listNorm.add(varBgrNumber);

	RooAddPdf pdfModel("pdfModel", "pdfModel", listComp, listNorm);

	
	results = pdfModel.fitTo(*dset, Save(true), PrintLevel(0), NumCPU(4));


	RooPlot* frame3 =varMass.frame();
	dset->plotOn(frame3, Binning(binN, minMass, maxMass));
 	pdfModel.plotOn(frame3);
	pdfModel.paramOn(frame3, Layout(0.65, 0.95, 0.95), Parameters(showParams), ShowConstants(true));

	TCanvas* canvB = new TCanvas("canvB", "canvB", 1000, 600);
	frame3->Draw();

}



