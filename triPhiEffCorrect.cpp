
using namespace RooFit;


TH1D* EfficiencyRatios(Double_t range0, Double_t range1, Int_t nch){
	gROOT->Reset();
	gStyle->SetOptStat(000);
	
// 	Double_t range0=3e3;
// 	Double_t range1=23e3;
// 	Int_t nch=5;
	
	TChain * DecayTree3phi=new TChain("DecayTree");
	DecayTree3phi->Add("MC/AllMC_Reduced.root");
	Int_t NEn=DecayTree3phi->GetEntries();
	Double_t Branch_Value;
	DecayTree3phi->SetBranchAddress("B_PT",&Branch_Value);
	TH1D * m_3phi_hist = new TH1D("3#phi candidates","3#phi candidates" , nch, range0, range1);
	for (Int_t i=0; i<NEn; i++) 
	{
		DecayTree3phi->GetEntry(i);
		if(Branch_Value>range0)m_3phi_hist->Fill(Branch_Value);
	}
	m_3phi_hist->SetBinError(nch,sqrt(m_3phi_hist->GetBinContent(nch)));
	m_3phi_hist->Scale(1./NEn);
	
	
	
	TChain * DecayTree3phiTRUE=new TChain("MCDecayTreeTuple/MCDecayTree");
	DecayTree3phiTRUE->Add("/home/aus/CERN/Analysis/MC/GenSpectr/3phiGenSpectr/DVntuple.root");
	Int_t NEn2=DecayTree3phiTRUE->GetEntries();
	Double_t Branch_Value2;
	DecayTree3phiTRUE->SetBranchAddress("B_s0_TRUEPT",&Branch_Value2);
	TH1D * m_3phi_histTRUE = new TH1D("3#phi TRUE","3#phi TRUE" , nch, range0, range1);
	for (Int_t i=0; i<NEn2; i++) 
	{
		DecayTree3phiTRUE->GetEntry(i);
		if(Branch_Value>range0)m_3phi_histTRUE->Fill(Branch_Value2);
	}
	m_3phi_histTRUE->SetBinError(nch,sqrt(m_3phi_histTRUE->GetBinContent(nch)));
	m_3phi_histTRUE->Scale(1./NEn2);


	
	
	TChain * DecayTree2phi=new TChain("DecayTree");
	DecayTree2phi->Add("/home/aus/CERN/Analysis/MC/CAL_0/Bs13/AllMC_Reduced.root");
	Int_t NEn3=DecayTree->GetEntries();
	Double_t Branch_Value3;
	DecayTree2phi->SetBranchAddress("B_PT",&Branch_Value3);
	TH1D * m_2phi_hist = new TH1D("2#phi candidates","2#phi candidates" , nch, range0, range1);
	//m_2phi_hist->SetBinErrorOption(TH1::kPoisson);
	for (Int_t i=0; i<NEn3; i++) 
	{
		DecayTree2phi->GetEntry(i);
		if(Branch_Value3>range0)m_2phi_hist->Fill(Branch_Value3);
	}
	m_2phi_hist->SetBinError(nch,sqrt(m_2phi_hist->GetBinContent(nch)));
	m_2phi_hist->Scale(1./NEn3);
	
	
	TChain * DecayTree2phiTRUE=new TChain("MCDecayTreeTuple/MCDecayTree");
	DecayTree2phiTRUE->Add("/home/aus/CERN/Analysis/MC/GenSpectr/2phiGenSpectr/DVntuple.root");
	Int_t NEn4=DecayTree->GetEntries();
	Double_t Branch_Value4;
	DecayTree2phiTRUE->SetBranchAddress("B_s0_TRUEPT",&Branch_Value4);
	TH1D * m_2phi_histTRUE = new TH1D("2#phi TRUE","2#phi TRUE" , nch, range0, range1);
//	m_2phi_histTRUE->SetBinErrorOption(TH1::kPoisson);
	for (Int_t i=0; i<NEn4; i++) 
	{
		DecayTree2phiTRUE->GetEntry(i);
		if(Branch_Value3>range0)m_2phi_histTRUE->Fill(Branch_Value4);
	}
	m_2phi_histTRUE->SetBinError(nch,sqrt(m_2phi_histTRUE->GetBinContent(nch)));
	m_2phi_histTRUE->Scale(1./NEn4);


	
	TCanvas *canv1 = new TCanvas("canv1","B_PT",5,85,800,600);
	
// 	m_3phi_hist->SetXTitle("B_PT");
// 	m_3phi_hist->SetLineColor(2);
// 	m_3phi_hist->SetLineWidth(2);
// 	m_3phi_hist->SetMinimum(0);
// // 	m_3phi_hist->Draw("E1");
// 	m_3phi_hist->DrawCopy("E1");
// 
// 	m_2phi_hist->SetLineColor(1);
// 	m_2phi_hist->SetLineWidth(2);
// 	m_2phi_hist->DrawCopy("E1same");
// 	//m_2phi_hist->Draw("E1same");
// 	
// 	leg = new TLegend(0.65,0.65,0.85,0.85);
// 	leg->AddEntry(m_3phi_hist,"3phi MC");
// 	leg->AddEntry(m_2phi_hist,"2phi MC");
// 	leg->Draw();  
	
	
	
// 	TCanvas *canv0 = new TCanvas("canv0","B_PT",5,85,800,600);
// 	m_3phi_histTRUE->SetXTitle("B_PT");
// 	m_3phi_histTRUE->SetLineColor(2);
// 	m_3phi_histTRUE->SetLineWidth(2);
// 	m_3phi_histTRUE->SetMinimum(0);
// // 	m_3phi_histTRUE->Draw("E1");
// 	m_3phi_histTRUE->DrawCopy("E1");
// 
// 	m_2phi_histTRUE->SetLineColor(1);
// 	m_2phi_histTRUE->SetLineWidth(2);
// 	m_2phi_histTRUE->DrawCopy("E1same");
// 	//m_2phi_histTRUE->Draw("E1same");
// 	
// 	leg = new TLegend(0.65,0.65,0.85,0.85);
// 	leg->AddEntry(m_3phi_histTRUE,"3phi MC");
// 	leg->AddEntry(m_2phi_histTRUE,"2phi MC");
// 	leg->Draw();  
	

        Double_t GenEff2phi = 0.168/0.5033;
        Double_t GenEff3phi = 0.1901/0.5024;

	TH1D * ratio_hist = new TH1D("ratio_hist","ratio_hist" , nch, range0, range1);
	for(int i=1;i<=nch;i++)
	{
	  ratio_hist->SetBinContent(i,m_2phi_hist->GetBinContent(i)*m_3phi_histTRUE->GetBinContent(i)*GenEff2phi/GenEff3phi/m_3phi_hist->GetBinContent(i)/m_2phi_histTRUE->GetBinContent(i));
	}
	TCanvas *canv2 = new TCanvas("canv2","Ration (N2phi/N3phi)",5,85,800,600);
	ratio_hist->DrawClone();
	
	delete DecayTree3phi;
	delete DecayTree2phi;
	delete DecayTree3phiTRUE;
	delete DecayTree2phiTRUE;
	delete m_2phi_hist;
	delete m_3phi_hist;
	delete m_2phi_histTRUE;
	delete m_3phi_histTRUE;
	return ratio_hist;
}


void FillWithWeights(Double_t minMassBReg, Double_t maxMassBReg, TH1D* ratio_hist){
  	TChain* chain = new TChain("DecayTree");
	chain->Add("Data_NewTrig/AllStat3phi.root");
	char label[200];
	sprintf(label, "B_m_scaled>%e&&B_m_scaled<%e", minMassBReg, maxMassBReg);
        TCut BMassCut(label);
	TTree *tree = chain->CopyTree(BMassCut);

	Double_t Phi1_MM_Mix, Phi2_MM_Mix, Phi3_MM_Mix, B_PT, PT_Weight;
	tree->SetBranchAddress("Phi1_MM_Mix",&Phi1_MM_Mix);
	tree->SetBranchAddress("Phi2_MM_Mix",&Phi2_MM_Mix);
	tree->SetBranchAddress("Phi3_MM_Mix",&Phi3_MM_Mix);
	tree->SetBranchAddress("B_PT",&B_PT);
	
	TBranch *PT_Weight_Branch = tree->Branch("PT_Weight", &PT_Weight, "PT_Weight/D");
	const Int_t NEn=tree->GetEntries();
	
	for (Int_t i=0; i<NEn; i++) 
	{
		tree->GetEntry(i);
		Int_t EffBinNumber = ratio_hist->FindBin(B_PT);
 		PT_Weight = ratio_hist->GetBinContent(EffBinNumber);
		PT_Weight_Branch->Fill();
	}
	TFile *newfile = new TFile("Data_NewTrig/AllStat3phi_WithWeights.root","recreate");
	tree->Print();
	tree->Write();
	
	delete chain;
        delete newfile;
}


void triPhiPure(){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");
	
	Float_t minMassB = 3000;
	Float_t maxMassB = 11000;
	Float_t binWidthB = 10.;
	Int_t binNB = int((maxMassB-minMassB)/binWidthB);
	
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	
	Float_t minMassBReg = 5250;
	Float_t maxMassBReg = 5500;
	Int_t binNBReg = int((maxMassBReg-minMassBReg)/binWidthB);
	
	Float_t BsMass = 5366.77; // ±0.24 meV
	

	RooRealVar B_m_scaled("B_m_scaled", "B_m_scaled", minMassB, maxMassB, "MeV");
	RooRealVar Phi1_MM_Mix("Phi1_MM_Mix", "Phi1_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_MM_Mix("Phi2_MM_Mix", "Phi2_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi3_MM_Mix("Phi3_MM_Mix", "Phi3_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar PT_Weight("PT_Weight","PT_Weight",0,50);
	
	TH1F* histBTriPhi = new TH1F("histBTriPhi", "histBTriPhi", binNBReg, minMassBReg, maxMassBReg);

	TChain* chain = new TChain("DecayTree");
	chain->Add("Data_NewTrig/AllStat3phi_WithWeights.root");

	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(B_m_scaled, Phi1_MM_Mix, Phi2_MM_Mix, Phi3_MM_Mix,PT_Weight), "","PT_Weight");
	RooPlot* frame0 = B_m_scaled.frame(Title("B_m_scaled"));

	
	frame0->SetAxisRange(minMassBReg,maxMassBReg);
	Int_t binNBReg = int((maxMassBReg-minMassBReg)/binWidthB);
	dsetFull->plotOn(frame0, Binning(binNBReg, minMassBReg, maxMassBReg));
	TCanvas* canv0 = new TCanvas("canv0", "canv0", 800, 600);
	frame0->Draw();

	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);
	RooRealVar varSigma("varSigma", "varSigma", 1.272);
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	

	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi1_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	RooRealVar varA1("varA1", "varA1", 0.01,0,1);
	RooGenericPdf pdfRoot1("pdfRoot1", "pdfRoot1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM_Mix, var2KMass,varA1));	
//	RooGenericPdf pdfRoot1("pdfRoot1", "pdfRoot1", "sqrt(@0-@1)", RooArgList(Phi1_MM_Mix, var2KMass));	
	RooRealVar varK1("varK1", "varK1", 1e-4, 1);
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1", RooArgList(pdfPhi1, pdfRoot1), varK1);
	
	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi2_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
 	RooRealVar varA2("varA2", "varA2", 0.01,0,1);
 	RooGenericPdf pdfRoot2("pdfRoot2", "pdfRoot2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM_Mix, var2KMass,varA2));
//	RooGenericPdf pdfRoot2("pdfRoot2", "pdfRoot2", "sqrt(@0-@1)", RooArgList(Phi2_MM_Mix, var2KMass));
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2", RooArgList(pdfPhi2, pdfRoot2), varK1);
	
	RooGenericPdf pdfPhi3("pdfPhi3", "pdfPhi3", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi3_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	RooRealVar varA3("varA3", "varA3", 0.01,0,1);
	RooGenericPdf pdfRoot3("pdfRoot3", "pdfRoot3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi3_MM_Mix, var2KMass,varA3));
//	RooGenericPdf pdfRoot3("pdfRoot3", "pdfRoot3", "sqrt(@0-@1)", RooArgList(Phi3_MM_Mix, var2KMass));
	RooAddPdf pdfModel3("pdfModel3", "pdfModel3", RooArgList(pdfPhi3, pdfRoot3), varK1);
	
	
	RooProdPdf pdfModelNE("pdfModelNE", "pdfModelNE", RooArgSet(pdfModel1, pdfModel2, pdfModel3));
//	RooRealVar varEvN("varEvN", "varEvN", 0, 1e5);
	
	
	RooRealVar varNTriPhi("varNTriPhi", "varNTriPhi",5,0,100);
	RooFormulaVar varEvN("varEvN", "varEvN", "@0/@1/@1/@1", RooArgList(varNTriPhi, varK1));
	
	RooExtendPdf pdfModel("pdfModel", "pdfModel",  pdfModelNE, varEvN);
	

//	RooFormulaVar varNTriPhi("varNTriPhi", "varNTriPhi", "@0*@1*@1*@1", RooArgList(varEvN, varK1));
// 	RooFormulaVar varNTriK("varNTriK", "varNTriK", "@0*(1-@1)*(1-@1)*(1-@1)", RooArgList(varEvN, varK1));
// 	RooFormulaVar varNDiPhiK("varNDiPhiK", "varNDiPhiK", "3*@0*(@1*@1*(1-@1))", RooArgList(varEvN, varK1));
// 	RooFormulaVar varNPhiDiK("varNPhiDiK", "varNPhiDiK", "3*@0*(@1*(1-@1)*(1-@1))", RooArgList(varEvN, varK1));

	char label[200];
	Float_t massBLo, massBHi;
	Double_t ey1[1000], ey2[1000], xx[1000], yy[1000], ex1[1000], ex2[1000];
	
	
	RooRealVar varMass("varMass", "varMass", 5250, 5500);
	RooRealVar Signal("Signal", "Signal", 0);
	varMass.setMin(minMassBReg);
	varMass.setMax(maxMassBReg);
	Signal.setMin(0);
	Signal.setMax(50);
	
	
	
	for (Int_t i=0; i<binNBReg; i++){
		massBLo = minMassBReg + i*binWidthB;
		massBHi = minMassBReg + (i+1)*binWidthB;

		sprintf(label, "B_m_scaled>%i&&B_m_scaled<%i", massBLo, massBHi);
		RooDataSet* dset = dsetFull->reduce(Cut(label), Name("dset"), Title("dset"));
		if (dset->numEntries()>0)
		{
		    RooFitResult* res = pdfModel.fitTo(*dset, Minos(true), Strategy(2), Save(true), PrintLevel(0));
		    if(varNTriPhi.getErrorLo()==0)
		      res = pdfModel.fitTo(*dset, Minos(true), Strategy(2), Save(true), PrintLevel(0));
		    
		    Double_t EDM = res->edm();
		    if(EDM>1e-3)
		    {
		      varA3.setConstant(kTRUE);
		      varA3.setVal(0);
		      varA2.setConstant(kTRUE);
		      varA2.setVal(0);
		      varA1.setConstant(kTRUE);
		      varA1.setVal(0);
		      res = pdfModel.fitTo(*dset, Minos(true), Strategy(2), Save(true), PrintLevel(0));
		      varA3.setConstant(kFALSE);
		      varA2.setConstant(kFALSE);
		      varA1.setConstant(kFALSE);
		      EDM = res->edm();  
		    }
		    if(EDM>1e-3)return;
		    
		    
		    ey1[i]= -varNTriPhi.getErrorLo();
		    ey2[i]= varNTriPhi.getErrorHi();
		    if(ey1[i]==0) ey1[i]=varNTriPhi.getError();
		    if(ey1[i]>varNTriPhi.getVal()) ey1[i]=varNTriPhi.getVal();
		    
		    if(varNTriPhi.getVal()<1.1)
		    {
		      ey1[i]=varNTriPhi.getVal();
		      if(varNTriPhi.getVal()<0.1) ey2[i]=1.5;
		    }
		    
		    
		    xx[i]=(massBLo+massBHi)/2;
		    yy[i]=varNTriPhi.getVal();
		    ex1[i]=binWidthB/2;
		    ex2[i]=binWidthB/2;
		    
		    delete res;
		}
		else
		{
		    xx[i]=(massBLo+massBHi)/2;
		    yy[i]=0;
		    ex1[i]=binWidthB/2;
		    ex2[i]=binWidthB/2;
		    ey1[i]=0;
		    ey2[i]=1.4;
		}
		
		histBTriPhi->SetBinContent(i,yy[i]);
		delete dset;	
	}
	
	
	TCanvas* canvTest = new TCanvas("canvTest", "canvTest", 1200, 400);
	canvTest->Divide(3, 1);
	
		massBsLo = 5280;
		massBsHi = 5290;
 
		sprintf(label, "B_m_scaled>%i&&B_m_scaled<%i", massBsLo, massBsHi);
		RooDataSet* dset2 = dsetFull->reduce(Cut(label), Name("dset2"), Title("dset2"));
		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));

		RooPlot* frame1 = Phi1_MM_Mix.frame(Title("#phi_{1} mass"));
		dset2->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel1.plotOn(frame1);
		pdfModel1.plotOn(frame1, Components(pdfRoot1), LineStyle(kDashed));

		RooPlot* frame2 = Phi2_MM_Mix.frame(Title("#phi_{2} mass"));
		dset2->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel2.plotOn(frame2);
		pdfModel2.plotOn(frame2, Components(pdfRoot2), LineStyle(kDashed));
		
		RooPlot* frame3 = Phi3_MM_Mix.frame(Title("#phi_{3} mass"));
		dset2->plotOn(frame3, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel3.plotOn(frame3);
		pdfModel3.plotOn(frame3, Components(pdfRoot3), LineStyle(kDashed));

		canvTest->cd(1);
		frame1->DrawClone();
		canvTest->cd(2);
		frame2->DrawClone();
		canvTest->cd(3);
		frame3->DrawClone();

		delete frame1;
		delete frame2;
		delete frame3;

		delete dset2;
		delete res;

	
	

	FILE *fp = fopen("triPhiPureOut.txt", "w");
	for(int i=0;i<binNBReg;i++)
	    fprintf(fp,"%f %f %f %f %f %f \n",xx[i],yy[i],ex1[i],ex2[i],ey1[i],ey2[i]);
	fclose(fp);
	

	TGraphAsymmErrors* triPhiGr = new TGraphAsymmErrors(binNBReg, xx, yy, ex1, ex2, ey1, ey2);
	
	TCanvas* canvA = new TCanvas("canvA", "canvA", 800, 700);
	triPhiGr->Draw("AP");	 	
}

void main()
{
   TH1D* aa = EfficiencyRatios(3e3,34e3,1);
   aa->Draw();
//   FillWithWeights(5250,5500,EfficiencyRatios(3e3,35e3,1));
//   triPhiPure();
   cout<<endl<<aa->GetBinContent(1);
   
}
