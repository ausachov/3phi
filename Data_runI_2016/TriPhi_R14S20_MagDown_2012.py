DaVinciVersion = 'v33r4'
myJobName = 'TriPhi_R14S20_MagDown'
myApplication = DaVinci()
myApplication.version = DaVinciVersion
myApplication.user_release_area = '/afs/cern.ch/user/j/jhe/cmtuser'
myApplication.platform='x86_64-slc5-gcc46-opt'
myApplication.optsfile = ['DaVinci_TriPhi_R14S20_2012_DTF.py']

data = BKQuery(path="/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20/90000000/CHARM.MDST",
               dqflag=['OK']).getDataset()

mySplitter = SplitByFiles( filesPerJob = 20, maxFiles = -1, ignoremissing = True )

myBackend = Dirac()
j = Job (
    name         = myJobName,
    application  = myApplication,
    splitter     = mySplitter,
    outputfiles  = [ 'Tuple.root',
                     'DVHistos.root'
                     ],
    inputdata    = data,
    backend      = myBackend,
    do_auto_resubmit = True
    )
j.submit(keep_going=True, keep_on_fail=True)

