// $Id: $
{
  TChain *chain = new TChain("Bs2TriPhiTuple/DecayTree");
  gROOT->ProcessLine(".L Ccbar3Phi_MagUp_2011.C");
  gROOT->ProcessLine("Ccbar3Phi_MagUp_2011(chain)");
  
  TCut KaonIPCut("Kaon1_IPCHI2_OWNPV>4 && Kaon2_IPCHI2_OWNPV>4 && Kaon3_IPCHI2_OWNPV>4 && Kaon4_IPCHI2_OWNPV>4 && Kaon5_IPCHI2_OWNPV>4 && Kaon6_IPCHI2_OWNPV>4");
  TCut KaonTRCut("Kaon1_TRACK_CHI2NDOF<4 && Kaon2_TRACK_CHI2NDOF<4 && Kaon3_TRACK_CHI2NDOF<4 && Kaon4_TRACK_CHI2NDOF<4 && Kaon5_TRACK_CHI2NDOF<4 && Kaon6_TRACK_CHI2NDOF<4");
  TCut PIDCut("Kaon1_ProbNNk>0.1 && Kaon2_ProbNNk>0.1 && Kaon3_ProbNNk>0.1 && Kaon4_ProbNNk>0.1 && Kaon5_ProbNNk>0.1 && Kaon6_ProbNNk>0.1");
  TCut PhiMCut("abs(Phi1_MM-1020)<12 && abs(Phi2_MM-1020)<12 && abs(Phi3_MM-1020)<12");
  TCut PhiVerCut("Phi1_ENDVERTEX_CHI2<25 && Phi2_ENDVERTEX_CHI2<25  && Phi3_ENDVERTEX_CHI2<25");
  TCut BCut("B_ENDVERTEX_CHI2<81 && B_FDCHI2_OWNPV>100");// >49");
  TCut PTCut("Kaon1_PT>500 && Kaon2_PT>500 && Kaon3_PT>500 && Kaon4_PT>500 && Kaon5_PT>500 && Kaon6_PT>500");
//   TCut TriggerCut("BHlt1Global_TOS && BHlt2Global_TOS");
  TCut Trigger0Cut("B_L0HadronDecision_TOS || B_L0Global_TIS");
  TCut Trigger1Cut("B_Hlt1TrackAllL0Decision_TOS");
  TCut Trigger2Cut("B_Hlt2Topo2BodyBBDTDecision_TOS || B_Hlt2Topo3BodyBBDTDecision_TOS || B_Hlt2Topo4BodyBBDTDecision_TOS || B_Hlt2IncPhiDecision_TOS");
 
  TCut totCut = 
    KaonIPCut 
    && KaonTRCut
    && PIDCut 
    && PhiMCut
    && PhiVerCut
    && BCut
    && PTCut
    //&& TriggerCut;
    && Trigger0Cut
    && Trigger1Cut
    && Trigger2Cut;

  TFile *newfile = new TFile("3phi_MagUp2011.root","recreate");
  TTree *newtree = chain->CopyTree(totCut);
  
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
  
}