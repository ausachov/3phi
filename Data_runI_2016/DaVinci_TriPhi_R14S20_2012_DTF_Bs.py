def fillTuple( tuple, branches, myTriggerList ):
    
    from Configurables import DecayTreeTuple, TupleToolTISTOS, TupleToolDecay, TupleToolRecoStats, TupleToolEventInfo, TupleToolDecayTreeFitter

    # for MicroDST
    tuple.RootInTES  = "/Event/Charm/"
    
    tuple.Branches = branches
    
    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolPrimaries",
        "TupleToolRecoStats",
        "TupleToolTrackInfo"
        ]

    tuple.addTool(TupleToolDecay, name = 'Jpsi')
    tuple.addTool(TupleToolDecay, name = 'B')


    # TISTOS for Jpsi
    tuple.Jpsi.ToolList += [ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ] 
    tuple.Jpsi.addTool(TupleToolTISTOS("TupleToolTISTOSForJpsi") )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList

    tuple.B.ToolList += [ "TupleToolTISTOS/TupleToolTISTOSForB" ] 
    tuple.B.addTool(TupleToolTISTOS("TupleToolTISTOSForB") )
    tuple.B.TupleToolTISTOSForB.Verbose=True
    tuple.B.TupleToolTISTOSForB.TriggerList = myTriggerList


    # RecoStats for filling SpdMult, etc
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True
    
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled"             : "DTF_FUN ( M , False )" ,
        "m_pv"                 : "DTF_FUN ( M , True )" ,
        "c2dtf_1"              : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2"              : "DTF_CHI2NDOF( True  )"
        }
    tuple.addTool(TupleToolDecay, name="Jpsi")
    tuple.Jpsi.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Jpsi"]
    tuple.Jpsi.addTool(LoKi_Jpsi)
    

    tuple.addTool(TupleToolDecay, name = 'Phi1')
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Phi1=LoKi__Hybrid__TupleTool("LoKi_Phi1")
    LoKi_Phi1.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled"             : "DTF_FUN ( M , False )" ,
        "m_pv"                 : "DTF_FUN ( M , True )" ,
        "c2dtf_1"              : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2"              : "DTF_CHI2NDOF( True  )"
    }
    tuple.addTool(TupleToolDecay, name="Phi1")
    tuple.Phi1.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Phi1"]
    tuple.Phi1.addTool(LoKi_Phi1)

    tuple.addTool(TupleToolDecay, name = 'Phi2')
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Phi2=LoKi__Hybrid__TupleTool("LoKi_Phi2")
    LoKi_Phi2.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled"             : "DTF_FUN ( M , False )" ,
        "m_pv"                 : "DTF_FUN ( M , True )" ,
        "c2dtf_1"              : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2"              : "DTF_CHI2NDOF( True  )"
    }
    tuple.addTool(TupleToolDecay, name="Phi2")
    tuple.Phi2.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Phi2"]
    tuple.Phi2.addTool(LoKi_Phi2)
    
    tuple.addTool(TupleToolDecay, name = 'Phi3')
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Phi3=LoKi__Hybrid__TupleTool("LoKi_Phi3")
    LoKi_Phi3.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled"             : "DTF_FUN ( M , False )" ,
        "m_pv"                 : "DTF_FUN ( M , True )" ,
        "c2dtf_1"              : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2"              : "DTF_CHI2NDOF( True  )"
    }
    tuple.addTool(TupleToolDecay, name="Phi3")
    tuple.Phi3.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Phi3"]
    tuple.Phi3.addTool(LoKi_Phi3)


    #LoKi two
    LoKi_B=LoKi__Hybrid__TupleTool("LoKi_B")
    LoKi_B.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled"             : "DTF_FUN ( M , False )" ,
        "Etac1_m_scaled"       : "DTF_FUN ( M12 , False )" ,
        "Etac2_m_scaled"       : "DTF_FUN ( M13 , False )" ,
        "Etac3_m_scaled"       : "DTF_FUN ( M23 , False )" ,
        "m_pv"                 : "DTF_FUN ( M , True )" ,
        "c2dtf_1"              : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2"              : "DTF_CHI2NDOF( True  )"
        }    
    tuple.addTool(TupleToolDecay, name="B")
    tuple.B.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_B"]
    tuple.B.addTool(LoKi_B)

    tuple.B.addTool(TupleToolDecayTreeFitter())
    tuple.B.ToolList += ["TupleToolDecayTreeFitter/BsMassFit"]
    
    tuple.B.addTool(TupleToolDecayTreeFitter("BsMassFit"))
    tuple.B.BsMassFit.Verbose = True
    tuple.B.BsMassFit.constrainToOriginVertex = True
    tuple.B.BsMassFit.UpdateDaughters = True
    tuple.B.BsMassFit.daughtersToConstrain = ["B_s0"]



    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"
        #        ,"m_scaled"             : "DTF_FUN ( M , False )"
    }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)

#from os import environc
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
# DecayTreeTuple

from Configurables import DecayTreeTuple

myTriggerList = [
    # L0
    "L0ElectronDecision",
    "L0PhotonDecision",
    "L0HadronDecision",
    # L0 Muon
    "L0MuonDecision",
    "L0MuonHighDecision",
    "L0DiMuonDecision",
    
    # Hlt1 track 
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackMuonDecision",
    "Hlt1TrackPhotonDecision",

    # Hlt2
    "Hlt2IncPhiDecision",
    "Hlt2DiPhiDecision",
    
    # Hlt2 Topo lines
    "Hlt2Topo2BodyBBDTDecision",
    "Hlt2Topo3BodyBBDTDecision",
    "Hlt2Topo4BodyBBDTDecision"
    ]



#"""
#PhiKK
#"""
#Jpsi2PhiKKBranches={
#     "Kaon1" :  "J/psi(1S) -> ( phi(1020) ->^K+ K-)  K+ K-"
#    ,"Kaon2" :  "J/psi(1S) -> ( phi(1020) -> K+^K-)  K+ K-"
#    ,"Kaon3" :  "J/psi(1S) -> ( phi(1020) -> K+ K-) ^K+ K-"
#    ,"Kaon4" :  "J/psi(1S) -> ( phi(1020) -> K+ K-)  K+^K-"
#    ,"Phi1"  :  "J/psi(1S) -> (^phi(1020) -> K+ K-)  K+ K-"
#    ,"Jpsi"  :  "J/psi(1S): J/psi(1S) -> ( phi(1020) -> K+ K-) K+ K-"
#    }
#
#Jpsi2PhiKKTuple = DecayTreeTuple("Jpsi2PhiKKTuple")
#Jpsi2PhiKKTuple.Decay = "J/psi(1S) -> ( ^phi(1020) -> ^K+ ^K-) ^K+ ^K-"
#Jpsi2PhiKKTuple.Inputs = [ "Phys/Ccbar2PhiKKDetachedLine/Particles" ]
#fillTuple( Jpsi2PhiKKTuple, Jpsi2PhiKKBranches, myTriggerList )


#"""
#Bs2JpsiPhi
#"""
#Bs2JpsiPhiBranches={
#     "Kaon1" :  "B_s0 -> ( J/psi(1S) -> ( phi(1020) ->^K+ K-)  K+ K- ) ( phi(1020) -> K+ K- )"
#    ,"Kaon2" :  "B_s0 -> ( J/psi(1S) -> ( phi(1020) -> K+^K-)  K+ K- ) ( phi(1020) -> K+ K- )"
#    ,"Kaon3" :  "B_s0 -> ( J/psi(1S) -> ( phi(1020) -> K+ K-) ^K+ K- ) ( phi(1020) -> K+ K- )"
#    ,"Kaon4" :  "B_s0 -> ( J/psi(1S) -> ( phi(1020) -> K+ K-)  K+^K- ) ( phi(1020) -> K+ K- )"  
#    ,"Kaon5" :  "B_s0 -> ( J/psi(1S) -> ( phi(1020) -> K+ K-)  K+ K- ) ( phi(1020) ->^K+ K- )"
#    ,"Kaon6" :  "B_s0 -> ( J/psi(1S) -> ( phi(1020) -> K+ K-)  K+ K- ) ( phi(1020) -> K+^K- )"
#    ,"Phi1"  :  "B_s0 -> ( J/psi(1S) -> (^phi(1020) -> K+ K-)  K+ K- ) ( phi(1020) -> K+ K- )"
#    ,"Phi2"  :  "B_s0 -> ( J/psi(1S) -> ( phi(1020) -> K+ K-)  K+ K- ) (^phi(1020) -> K+ K- )" 
#    ,"Jpsi"  :  "B_s0 -> (^J/psi(1S) -> ( phi(1020) -> K+ K-)  K+ K- ) ( phi(1020) -> K+ K- )"
#    ,"B" : "B_s0: B_s0 -> ( J/psi(1S) ->  ( phi(1020) -> K+ K-)  K+ K- ) ( phi(1020) -> K+ K- )"
#    }
#
#Bs2JpsiPhiTuple = DecayTreeTuple("Bs2JpsiPhiTuple")
#Bs2JpsiPhiTuple.Decay = "B_s0 -> ( ^J/psi(1S) -> ( ^phi(1020) -> ^K+ ^K-) ^K+ ^K- ) ( ^phi(1020) -> ^K+ ^K- )"
#Bs2JpsiPhiTuple.Inputs = [ "Phys/Ccbar2PhiBs2JpsiPhiLine/Particles" ]
#fillTuple( Bs2JpsiPhiTuple, Bs2JpsiPhiBranches, myTriggerList )


"""
Bs2TriPhi
"""
Bs2TriPhiBranches={
    "Kaon1" :  "B_s0 -> ( phi(1020) ->^K+ K-) ( phi(1020) -> K+ K- ) ( phi(1020) -> K+ K- )"
    ,"Kaon2" :  "B_s0 -> ( phi(1020) -> K+^K-) ( phi(1020) -> K+ K- ) ( phi(1020) -> K+ K- )"
    ,"Kaon3" :  "B_s0 -> ( phi(1020) -> K+ K-) ( phi(1020) ->^K+ K- ) ( phi(1020) -> K+ K- )"
    ,"Kaon4" :  "B_s0 -> ( phi(1020) -> K+ K-) ( phi(1020) -> K+^K- ) ( phi(1020) -> K+ K- )"
    ,"Kaon5" :  "B_s0 -> ( phi(1020) -> K+ K-) ( phi(1020) -> K+ K- ) ( phi(1020) ->^K+ K- )"
    ,"Kaon6" :  "B_s0 -> ( phi(1020) -> K+ K-) ( phi(1020) -> K+ K- ) ( phi(1020) -> K+^K- )"
    ,"Phi1"  :  "B_s0 -> ^(phi(1020) -> K+ K-) ( phi(1020) -> K+ K- ) ( phi(1020) -> K+ K- )"
    ,"Phi2"  :  "B_s0 -> ( phi(1020) -> K+ K-) ^(phi(1020) -> K+ K- ) ( phi(1020) -> K+ K- )"
    ,"Phi3"  :  "B_s0 -> ( phi(1020) -> K+ K-) ( phi(1020) -> K+ K- ) ^(phi(1020) -> K+ K- )"
    ,"B"     : "^(B_s0 ->  ( phi(1020) -> K+ K-) ( phi(1020) -> K+ K- ) ( phi(1020) -> K+ K- ))"
}

Bs2TriPhiTuple = DecayTreeTuple("Bs2TriPhiTuple")
Bs2TriPhiTuple.Decay = "^(B_s0 -> ^(phi(1020) -> ^K+ ^K-) ^(phi(1020) -> ^K+ ^K- ) ^(phi(1020) -> ^K+ ^K- ))"
Bs2TriPhiTuple.Inputs = [ "Phys/Ccbar2PhiBs2TriPhiLine/Particles" ]
fillTuple( Bs2TriPhiTuple, Bs2TriPhiBranches, myTriggerList )






from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['/Event/DAQ',
                         '/Event/pRec']

"""
Event-level filters
"""
#from PhysConf.Filters import LoKi_Filters
#Jpsi2ppFilters = LoKi_Filters (
#    STRIP_Code = """
#    HLT_PASS('StrippingCcbar2PhiPhiLineDecision')
#    | HLT_PASS('StrippingCcbar2PhiPhiDetachedLineDecision')
#    | HLT_PASS('StrippingCcbar2PhiKKDetachedLineDecision')
#    | HLT_PASS('StrippingCcbar2PhiBs2JpsiPhiLineDecision')
#    | HLT_PASS('StrippingCcbar2PhiBs2TriPhiLineDecision')
#    | HLT_PASS('StrippingCcbar2PhiB2JpsiKLineDecision')
#    | HLT_PASS('StrippingCcbar2PhiPiPiDetachedLineDecision')
#    """
#    )
from PhysConf.Filters import LoKi_Filters
Jpsi2ppFilters = LoKi_Filters (
    STRIP_Code = """
    HLT_PASS('StrippingCcbar2PhiPhiLineDecision')
    | HLT_PASS('StrippingCcbar2PhiPhiDetachedLineDecision')
    | HLT_PASS('StrippingCcbar2PhiBs2TriPhiLineDecision')
    """
    )

"""
Scale momentum
"""
from Configurables import TrackScaleState
StateScale = TrackScaleState("StateScale", RootInTES = '/Event/Charm')
year = "2012"

from Configurables import DaVinci
DaVinci().EventPreFilters = Jpsi2ppFilters.filters ('Jpsi2ppFilters')
DaVinci().EvtMax = -1                          # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = year
DaVinci().Simulation    = False
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().InputType='MDST'
DaVinci().UserAlgorithms = [ eventNodeKiller,
                             StateScale, 
#                             Jpsi2DiPhiTuple,
#                             Jpsi2DiPhiDetachedTuple,
#                             Jpsi2PhiKKTuple,
#                             Bs2JpsiPhiTuple,
                             Bs2TriPhiTuple
#                             B2JpsiKTuple,
#                             Jpsi2PhiPiPiTuple
                             ]        # The algorithms

dv = DaVinci ( RootInTES  = '/Event/Charm' )

# Get Luminosity
DaVinci().Lumi = True

# database
from Configurables import CondDB
CondDB( LatestGlobalTagByDataType = year )

# database
# DaVinci().DDDBtag   = "dddb-20120831"
# DaVinci().CondDBtag = "cond-20121108"

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]



#from GaudiConf import IOHelper
## Use the local input data
#IOHelper().inputFiles([
#                       '00020198_00000017_1.charm.mdst'
#                       ], clear=True)
