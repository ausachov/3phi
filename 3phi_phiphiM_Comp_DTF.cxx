#include <TMath.h>







void phiphi_m_draw()
{
  gROOT->Reset();
  
  Double_t range0=2000;
  Double_t range1=4400;
  Int_t nch=120;
    gStyle->SetOptStat(111);
  
  TChain * DecayTree=new TChain("DecayTree");
  DecayTree->Add("3phi_BsMCut.root");
  const Int_t NEn=DecayTree->GetEntries();
  Double_t PX1, PY1, PZ1, M1, PE1;
  Double_t PX2, PY2, PZ2, M2, PE2;
  Double_t PX3, PY3, PZ3, M3, PE3;
  Double_t PX, PY, PZ, PE;
  
  Float_t k1PX1, k1PY1, k1PZ1, k1PE1;
  Float_t k1PX2, k1PY2, k1PZ2, k1PE2;
  Float_t k1PX3, k1PY3, k1PZ3, k1PE3;
  
  Float_t k2PX1, k2PY1, k2PZ1, k2PE1;
  Float_t k2PX2, k2PY2, k2PZ2, k2PE2;
  Float_t k2PX3, k2PY3, k2PZ3, k2PE3;
  

  
  Float_t k3PX1, k3PY1, k3PZ1, k3PE1;
  Float_t k3PX2, k3PY2, k3PZ2, k3PE2;
  Float_t k3PX3, k3PY3, k3PZ3, k3PE3;
  

  
  Double_t PX, PY, PZ, PE;
  
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PX",&k1PX1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PY",&k1PY1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PZ",&k1PZ1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PE",&k1PE1);

  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_0_PX",&k1PX2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_0_PY",&k1PY2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_0_PZ",&k1PZ2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_0_PE",&k1PE2);
  
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_PX",&k2PX1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_PY",&k2PY1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_PZ",&k2PZ1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_PE",&k2PE1);

  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_0_PX",&k2PX2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_0_PY",&k2PY2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_0_PZ",&k2PZ2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10200_Kplus_0_PE",&k2PE2);
  
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_PX",&k3PX1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_PY",&k3PY1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_PZ",&k3PZ1);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_PE",&k3PE1);

  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_0_PX",&k3PX2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_0_PY",&k3PY2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_0_PZ",&k3PZ2);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_10201_Kplus_0_PE",&k3PE2);
  
  
  

  TCanvas *canv1 = new TCanvas("canv1","MJPsi",5,85,800,600);
  TH1D * m_phiphi_hist = new TH1D("M(#phi#phi) from 3#phi","#phi#phi candidates" , nch, range0, range1);
    
    Double_t mass = 0;
    Double_t bestMass = 9999;
    Double_t bestMassDiff = 9999;
    Double_t etacMass = 2983.6;
  for (Int_t i=0; i<NEn; i++) 
  {
      bestMassDiff = 9999;
	  DecayTree->GetEntry(i);
	  PE = k1PE1 + k1PE2 + k2PE1 +k2PE2;
	  PZ = k1PZ1 + k1PZ2 + k2PZ1 +k2PZ2;
	  PX = k1PX1 + k1PX2 + k2PX1 +k2PX2;
	  PY = k1PY1 + k1PY2 + k2PY1 +k2PY2;
      mass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);

      
	  m_phiphi_hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
	  
	  PE = k1PE1 + k1PE2 + k3PE1 +k3PE2;
	  PZ = k1PZ1 + k1PZ2 + k3PZ1 +k3PZ2;
	  PX = k1PX1 + k1PX2 + k3PX1 +k3PX2;
	  PY = k1PY1 + k1PY2 + k3PY1 +k3PY2;
      mass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);

	  m_phiphi_hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
	  
	  PE = k3PE1 + k3PE2 + k2PE1 +k2PE2;
	  PZ = k3PZ1 + k3PZ2 + k2PZ1 +k2PZ2;
	  PX = k3PX1 + k3PX2 + k2PX1 +k2PX2;
	  PY = k3PY1 + k3PY2 + k2PY1 +k2PY2;
      mass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);

	  m_phiphi_hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
	  

  }
  m_phiphi_hist->Draw("E1");
  
  
  
  
  TChain * DecayTreeMC=new TChain("DecayTree");
  DecayTreeMC->Add("MC/AllMC_Reduced.root");
  const Int_t NEnMC=DecayTreeMC->GetEntries();

  
  DecayTreeMC->SetBranchAddress("Phi1_PX",&PX1);
  DecayTreeMC->SetBranchAddress("Phi1_PY",&PY1);
  DecayTreeMC->SetBranchAddress("Phi1_PZ",&PZ1);
  DecayTreeMC->SetBranchAddress("Phi1_MM",&M1);
  DecayTreeMC->SetBranchAddress("Phi1_PE",&PE1);

  DecayTreeMC->SetBranchAddress("Phi2_PX",&PX2);
  DecayTreeMC->SetBranchAddress("Phi2_PY",&PY2);
  DecayTreeMC->SetBranchAddress("Phi2_PZ",&PZ2);
  DecayTreeMC->SetBranchAddress("Phi2_MM",&M2);
  DecayTreeMC->SetBranchAddress("Phi2_PE",&PE2);
  
  DecayTreeMC->SetBranchAddress("Phi3_PX",&PX3);
  DecayTreeMC->SetBranchAddress("Phi3_PY",&PY3);
  DecayTreeMC->SetBranchAddress("Phi3_PZ",&PZ3);
  DecayTreeMC->SetBranchAddress("Phi3_MM",&M3);
  DecayTreeMC->SetBranchAddress("Phi3_PE",&PE3);

  TH1D * MCm_phiphi_hist = new TH1D("M(#phi#phi) from MC 3#phi","#phi#phi candidates" , nch, range0, range1);
  for (Int_t i=0; i<NEnMC; i++) 
  {
      bestMassDiff = 9999;
	  DecayTreeMC->GetEntry(i);
	  PE = PE1 + PE2;
	  PZ = PZ1 + PZ2;
	  PX = PX1 + PX2;
	  PY = PY1 + PY2;
	  MCm_phiphi_hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
      mass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);
      
	  PE = PE1 + PE3;
	  PZ = PZ1 + PZ3;
	  PX = PX1 + PX3;
	  PY = PY1 + PY3;
	  MCm_phiphi_hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
      mass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);

      
	  PE = PE3 + PE2;
	  PZ = PZ3 + PZ2;
	  PX = PX3 + PX2;
	  PY = PY3 + PY2;
	  MCm_phiphi_hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
      mass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);

  }
  MCm_phiphi_hist->SetLineColor(kRed);
  MCm_phiphi_hist->Scale(Double_t(NEn)/NEnMC);
  MCm_phiphi_hist->Draw("same");
}
