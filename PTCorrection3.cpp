
using namespace RooFit;


PTCorrection(Int_t nch){
  
  
	gROOT->Reset();
	gStyle->SetOptStat(000);
	
	
	
	Double_t range0=0;
	Double_t range1=20e3;
	Double_t binWidth = (range1-range0)/nch;
	
	TChain * DecayTree2phiDataAll=new TChain("DecayTree");
	DecayTree2phiDataAll->Add("/home/aus/LHCb/2phi/Data_07_15/All2phi.root");
	
	
	Double_t BsMass = 5366.77; // ±0.24 meV

	Double_t minMass = 5250;
	Double_t maxMass = 5500;
	Double_t BinWidth = 10.;	
	Int_t binN = int((maxMass-minMass)/BinWidth);
	RooRealVar varMass("M(#phi#phi)", "M(#phi#phi)", minMass, maxMass, "MeV");	
	
	RooRealVar varBsNumber("N(B_{s})", "varBsNumber", 2560, 0, 1e6);
	RooRealVar varBsSigma("#sigma", "#sigma", 13,5,40);
	RooRealVar varBsMass("M(Bs)", "M(Bs)", 5366.7);
	RooGaussian pdfBs("pdfBs", "pdfBs", varMass, varBsMass, varBsSigma);

	RooRealVar varBgrNBs("varBgrNBs", "varBgrNBs", 10,0, 1e4);
	RooRealVar varC4("varC4", "varC4", 0);
	RooExponential pdfBsBgr("pdfBsBgr", "pdfBsBgr", varMass, varC4);
//	RooGenericPdf pdfBsBgr("pdfBsBgr", "pdfBsBgr","1+(@0-5200)*@1", RooArgSet(varMass, varC4));

	RooAddPdf pdfModelBs("pdfModelBs", "pdfModelBs", RooArgList(pdfBs, pdfBsBgr), RooArgList(varBsNumber, varBgrNBs));
	
	TH1D *hist2phiData = new TH1D("2#phi Data","2#phi Data", nch, range0, range1);
	
	char filename[100], label[100];
	for(int ii=0;ii<nch;ii++)
	{
	  Double_t low = range0+ii*binWidth;
	  Double_t high = low+binWidth;

	  sprintf(label, "Jpsi_PT>%f&&Jpsi_PT<%f", low, high);
	  
	  TFile *newfile = new TFile("2phi_afterCut.root","recreate");
	  TTree* DecayTree2phiData = DecayTree2phiDataAll->CopyTree(label);
	
	  newfile->Write();
	  newfile->Close();

	
	  gROOT->ProcessLine(".L diPhiPure.cpp");
	  gROOT->ProcessLine("diPhiPure(4,1)");
	  
	  TFile* file = new TFile("bsPureAll.root");
	  TH1D *histJpsiDiPhi  = (TH1D*) file->Get("histJpsiDiPhi");
	  RooDataHist* dset = new RooDataHist("dset", "dset", varMass, histJpsiDiPhi);	
	  pdfModelBs.chi2FitTo(*dset, Extended(kTRUE), Save(true), PrintLevel(0), NumCPU(4));

	  hist2phiData->SetBinContent(ii+1,varBsNumber.getVal());
	  hist2phiData->SetBinError(ii+1,varBsNumber.getError());


	  delete dset;
	  delete histJpsiDiPhi;
	  
 	  file->Close();
	  delete file;
	  delete newfile;
	}

	Double_t Branch_Value;
	
	
	TChain * DecayTree2phiMCAll=new TChain("DecayTree");
	DecayTree2phiMCAll->Add("/home/aus/CERN/Analysis/MC/CAL_0/Bs13/AllMC_Reduced.root");
	TTree* DecayTree2phiMC = DecayTree2phiMCAll->CopyTree("B_PT>0 && B_PT<25000");
	 
	Int_t NEnMC=DecayTree2phiMC->GetEntries();
	DecayTree2phiMC->SetBranchAddress("B_PT",&Branch_Value);
	TH1D *hist2phiMC = new TH1D("2#phi MC","2#phi MC", nch, range0, range1);
	for (Int_t i=0; i<NEnMC; i++) 
	{
		DecayTree2phiMC->GetEntry(i);
		hist2phiMC->Fill(Branch_Value);
	}


	TCanvas* canvC = new TCanvas("canvC", "canv", 800, 700);



	TChain * DecayTree3phiGenAll=new TChain("MCDecayTreeTuple/MCDecayTree");
	DecayTree3phiGenAll->Add("/home/aus/CERN/Analysis/MC/GenSpectr/3phiGenSpectr/DVntuple.root");
	TTree* DecayTree3phiGen = DecayTree3phiGenAll->CopyTree("B_s0_TRUEPT>0 && B_s0_TRUEPT<25000");
	Int_t NEnGen=DecayTree3phiGen->GetEntries();
	Double_t Branch_Value1;
	DecayTree3phiGen->SetBranchAddress("B_s0_TRUEPT",&Branch_Value1);
	TH1D *hist3phiGen = new TH1D("3#phi Gen","3#phi Gen", nch, range0, range1);
	for (Int_t i=0; i<NEnGen; i++) 
	{
		DecayTree3phiGen->GetEntry(i);
		hist3phiGen->Fill(Branch_Value1);
	}
	




	
	TChain * DecayTree3phiMCAll=new TChain("DecayTree");
	DecayTree3phiMCAll->Add("/home/aus/CERN/Analysis/MC/CAL_0/3phi/AllMC_Reduced.root");
	TTree* DecayTree3phiMC = DecayTree3phiMCAll->CopyTree("B_PT>0 && B_PT<25000");
	 
	Int_t NEn3MC=DecayTree3phiMC->GetEntries();
	Double_t Branch_Value2;
	DecayTree3phiMC->SetBranchAddress("B_PT",&Branch_Value2);
	TH1D *hist3phiMC = new TH1D("3#phi MC","3#phi MC", nch, range0, range1);
	TH1D *hist3phiMCNew = new TH1D("3#phi MC New","3#phi MC New", nch, range0, range1);
	
	for (Int_t i=0; i<NEn3MC; i++) 
	{
		DecayTree3phiMC->GetEntry(i);
		hist3phiMC->Fill(Branch_Value2);
	}	
	
	
	TChain * DecayTree2phiGenAll=new TChain("MCDecayTreeTuple/MCDecayTree");
	DecayTree2phiGenAll->Add("/home/aus/CERN/Analysis/MC/GenSpectr/2phiGenSpectr/DVntuple*");
	TTree* DecayTree2phiGen = DecayTree2phiGenAll->CopyTree("B_s0_TRUEPT>0 && B_s0_TRUEPT<25000");
	Int_t NEnGen=DecayTree2phiGen->GetEntries();
	Double_t Branch_Value3;
	DecayTree2phiGen->SetBranchAddress("B_s0_TRUEPT",&Branch_Value3);
	TH1D *hist2phiGen = new TH1D("2#phi Gen","2#phi Gen", nch, range0, range1);
	for (Int_t i=0; i<NEnGen; i++) 
	{
		DecayTree2phiGen->GetEntry(i);
		hist2phiGen->Fill(Branch_Value3);
	}
	
	
    hist2phiMC->Scale(1./hist2phiMC->Integral());
	hist2phiData->Scale(1./hist2phiData->Integral());	
	hist3phiGen->Scale(1./hist3phiGen->Integral());
	hist2phiGen->Scale(1./hist2phiGen->Integral());
	hist3phiMC->Scale(1./hist3phiMC->Integral());
	
	hist2phiGen->SetLineColor(kRed);
	hist2phiGen->Draw();
	hist2phiData->Draw("same");	
	hist2phiMC->Draw("same");	
	
// 
 	Double_t PtCorrection = 0;
	Double_t dPtCorrection = 0;
// 	
// 	
// 	hist2phiData->Draw();
// 	
// 	
	for(int i=0;i<nch;i++)
	{
	  cout<<hist2phiData->GetBinContent(i+1)<<endl;
	  cout<<hist3phiGen->GetBinContent(i+1)<<endl;
	  cout<<hist3phiMC->GetBinContent(i+1)<<endl;
	  cout<<hist2phiMC->GetBinContent(i+1)<<endl;
	  
	  Double_t Data2phiBin = hist2phiData->GetBinContent(i+1);
	  Double_t dData2phiBin = hist2phiData->GetBinError(i+1);
	  Double_t Gen2phiBin  = hist2phiGen->GetBinContent(i+1);
	  Double_t MC2phiBin = hist2phiMC->GetBinContent(i+1);

	  
	  
	  PtCorrection+=(Gen2phiBin)*(Data2phiBin)/(MC2phiBin);
	  dPtCorrection+=(Gen2phiBin)*(dData2phiBin)/(MC2phiBin)*(Gen2phiBin)*(dData2phiBin)/(MC2phiBin);

	}
	dPtCorrection=sqrt(dPtCorrection);
	cout<<"PtCorrection="<<PtCorrection<<" +- "<<dPtCorrection<<endl;
}

