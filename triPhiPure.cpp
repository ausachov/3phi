
using namespace RooFit;


//0- FixSigma
//1- VariableSigma
//2- Sigma MC

//3 - Exp bg
//4 - Shift bg


void triPhiPure(int Type=0){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");


    gROOT->ProcessLine(".x lhcbStyle.C");

	
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	
	
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	
	Float_t minMassBReg = 5250;
	Float_t maxMassBReg = 5500;
    
//    Float_t minMassBReg = 5300;
//    Float_t maxMassBReg = 5430;
	
        if(Type==4)
	{
	  minMassBReg-=5;
	  maxMassBReg-=5;
	}	
	
	Float_t binWidthB = 10.;
	Int_t binNBReg = int((maxMassBReg-minMassBReg)/binWidthB);
	
	Float_t BsMass = 5366.77; // ±0.24 meV
	
	RooRealVar B_m_scaled("B_m_scaled", "B_m_scaled", minMassBReg, maxMassBReg, "MeV");
	RooRealVar Phi1_MM_Mix("Phi1_MM_Mix", "Phi1_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_MM_Mix("Phi2_MM_Mix", "Phi2_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi3_MM_Mix("Phi3_MM_Mix", "Phi3_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	
	TH1F* histBTriPhi = new TH1F("histBTriPhi", "histBTriPhi", binNBReg, minMassBReg, maxMassBReg);

	TChain* chain = new TChain("DecayTree");
	chain->Add("Data_NewTrig/AllStat3phi.root");

	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(B_m_scaled, Phi1_MM_Mix, Phi2_MM_Mix, Phi3_MM_Mix), "");
	RooPlot* frame0 = B_m_scaled.frame(Title("B_m_scaled"));

	
	frame0->SetAxisRange(minMassBReg,maxMassBReg);
	Int_t binNBReg = int((maxMassBReg-minMassBReg)/binWidthB);
	dsetFull->plotOn(frame0, Binning(binNBReg, minMassBReg, maxMassBReg));
	TCanvas* canvDirty = new TCanvas("canvDirty", "canvDirty", 800, 600);
	frame0->Draw();

	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46,1018,1020);
	
	
	switch(Type)
	{
	  case 1:
	    RooRealVar varSigma("varSigma", "varSigma", 1, 0.1, 4);
	    break;
	  case 2:
	    RooRealVar varSigma("varSigma", "varSigma", 1.15);
	    break;
	  default:
	    RooRealVar varSigma("varSigma", "varSigma", 1.20,1,3);
	    break;
	}
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	
	
	RooRealVar varA1("varA1","varA1",0);
	RooRealVar varA2("varA2","varA2",0);
	RooRealVar varA3("varA3","varA3",0);
	

	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
							
			      RooArgList(Phi1_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	RooGenericPdf pdfRoot1("pdfRoot1", "pdfRoot1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM_Mix, var2KMass,varA1));	
	
	
	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
						  RooArgList(Phi2_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
 	RooGenericPdf pdfRoot2("pdfRoot2", "pdfRoot2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM_Mix, var2KMass,varA2));
	
	
	RooGenericPdf pdfPhi3("pdfPhi3", "pdfPhi3", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
							  RooArgList(Phi3_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	RooGenericPdf pdfRoot3("pdfRoot3", "pdfRoot3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi3_MM_Mix, var2KMass,varA3));

	
	
	RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot1A3("pdfRoot1A3", "pdfRoot1A3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM_Mix, var2KMass,varA3));
	
	RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot2A3("pdfRoot2A3", "pdfRoot2A3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM_Mix, var2KMass,varA3));
	
	RooGenericPdf pdfRoot3A1("pdfRoot3A1", "pdfRoot3A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi3_MM_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot3A2("pdfRoot3A2", "pdfRoot3A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi3_MM_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot3A3("pdfRoot3A3", "pdfRoot3A3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi3_MM_Mix, var2KMass,varA3));
	
	
	RooProdPdf pdfSSS("pdfSSS", "pdfSSS", RooArgSet(pdfPhi1, pdfPhi2, pdfPhi3));
	
	RooProdPdf pdfS1S2B3("pdfS1S2B3", "pdfS1S2B3", RooArgSet(pdfPhi1, pdfPhi2, pdfRoot3A1));
	RooProdPdf pdfS1B2S3("pdfS1B2S3", "pdfS1B2S3", RooArgSet(pdfPhi1, pdfRoot2A1, pdfPhi3));
	RooProdPdf pdfB1S2S3("pdfB1S2S3", "pdfB1S2S3", RooArgSet(pdfRoot1A1, pdfPhi2, pdfPhi3));
	
	RooProdPdf pdfS1B2B3("pdfS1B2B3", "pdfS1B2B3", RooArgSet(pdfPhi1, pdfRoot2A2, pdfRoot3A2));
	RooProdPdf pdfB1S2B3("pdfB1S2B3", "pdfB1S2B3", RooArgSet(pdfRoot1A2, pdfPhi2, pdfRoot3A2));
	RooProdPdf pdfB1B2S3("pdfB1B2S3", "pdfB1B2S3", RooArgSet(pdfRoot1A2, pdfRoot2A2, pdfPhi3));
	
	RooProdPdf pdfBBB("pdfBBB", "pdfBBB", RooArgSet(pdfRoot1A3, pdfRoot2A3, pdfRoot3A3));
	
	RooRealVar varNTriPhi	("varNTriPhi" , "varNTriPhi" , 100,0, 1e7);
	RooRealVar varN2Phi2K	("varN2Phi2K", "varN2Phi2K", 10,0, 1e7);
	RooRealVar varNPhi4K	("varNPhi4K" , "varNPhi4K" , 0,0, 1e3);
	RooRealVar varN6K	("varN6K"    , "varN6K"    , 0,0, 1e3);
	
	
	
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1",  
			RooArgList(pdfPhi1   ,pdfPhi1   ,pdfPhi1   ,pdfRoot1A1,pdfPhi1   ,pdfRoot1A2,pdfRoot1A2,pdfRoot1A3), 
			RooArgList(varNTriPhi,varN2Phi2K,varN2Phi2K,varN2Phi2K,varNPhi4K ,varNPhi4K ,varNPhi4K ,varN6K));
	
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2",  
			RooArgList(pdfPhi2   ,pdfPhi2   ,pdfRoot2A1,pdfPhi2   ,pdfRoot2A2,pdfPhi2   ,pdfRoot2A2,pdfRoot2A3), 
			RooArgList(varNTriPhi,varN2Phi2K,varN2Phi2K,varN2Phi2K,varNPhi4K ,varNPhi4K ,varNPhi4K ,varN6K));
	
	RooAddPdf pdfModel3("pdfModel3", "pdfModel3",  
			RooArgList(pdfPhi3   ,pdfRoot3A1,pdfPhi3   ,pdfPhi3   ,pdfRoot3A2,pdfRoot3A2,pdfPhi3  ,pdfRoot3A3), 
			RooArgList(varNTriPhi,varN2Phi2K,varN2Phi2K,varN2Phi2K,varNPhi4K ,varNPhi4K ,varNPhi4K,varN6K));
	

	
	
	RooAddPdf pdfModel("pdfModel", "pdfModel",  
			   RooArgList(pdfSSS,     pdfS1S2B3 ,pdfS1B2S3 ,pdfB1S2S3 ,  pdfS1B2B3,pdfB1S2B3,pdfB1B2S3,  pdfBBB), 
			   RooArgList(varNTriPhi, varN2Phi2K,varN2Phi2K,varN2Phi2K,  varNPhi4K,varNPhi4K,varNPhi4K,  varN6K));
	
	


	char label[200];
	Float_t massBLo, massBHi;
	Double_t ey1[1000], ey2[1000], xx[1000], yy[1000], ex1[1000], ex2[1000], edm[1000];
	
	

    pdfModel.fitTo(*dsetFull ,Minos(true),Extended(true), Save(true), PrintLevel(0));
    varPhiMass.setConstant(kTRUE);
    varSigma.setConstant(kTRUE);
	
	
	for (Int_t i=0; i<binNBReg; i++){
        
		massBLo = minMassBReg + i*binWidthB;
		massBHi = minMassBReg + (i+1)*binWidthB;

		varN6K.setConstant(kFALSE);
		varN2Phi2K.setConstant(kFALSE);
		varNPhi4K.setConstant(kFALSE);
		

		
		sprintf(label, "B_m_scaled>%i&&B_m_scaled<%i", massBLo, massBHi);
		RooDataSet* dset = dsetFull->reduce(Cut(label), Name("dset"), Title("dset"));
		
		varNTriPhi.setVal(dset->sumEntries());
		varN6K.setVal(0);
		varN2Phi2K.setVal(0);
		varNPhi4K.setVal(0);
		
		RooFitResult* res = pdfModel.fitTo(*dset ,Minos(true),Extended(true), Save(true), PrintLevel(0));
		if(varNTriPhi.getErrorLo()==0)
		  res = pdfModel.fitTo(*dset ,Minos(true),Extended(true), Save(true), PrintLevel(0));
		if(varNTriPhi.getErrorHi()==0)
		  res = pdfModel.fitTo(*dset ,Minos(true),Extended(true), Save(true), PrintLevel(0));
		
		Double_t EDM = res->edm();
	
/*		if(EDM>2e-3)
		{
		  varN6K.setConstant(kTRUE);
		  varN6K.setVal(0);
		  res = pdfModel.fitTo(*dset ,Minos(true),Extended(true), Save(true), PrintLevel(0));
		  EDM = res->edm();
		}
		
		if(EDM>2e-3)
		{
		  varNPhi4K.setConstant(kTRUE);
		  varNPhi4K.setVal(0);
		  res = pdfModel.fitTo(*dset ,Minos(true),Extended(true), Save(true), PrintLevel(0));
		  EDM = res->edm();
		}
		
		if(EDM>2e-3)
		{
		  varN2Phi2K.setConstant(kTRUE);
		  varN2Phi2K.setVal(0);
		  res = pdfModel.fitTo(*dset ,Minos(true),Extended(true), Save(true), PrintLevel(0));
		  EDM = res->edm();
		}	*/	
		
		if(TMath::Abs(varN6K.getVal())<(varN6K.getError()/2))
		{
		  varN6K.setConstant(kTRUE);
//		  varN6K.setVal(0);
		  res = pdfModel.fitTo(*dset ,Minos(true),Extended(true), Save(true), PrintLevel(0));
		  EDM = res->edm();
		}
		
		if(TMath::Abs(varNPhi4K.getVal())<(varNPhi4K.getError()/2))
		{
		  varNPhi4K.setConstant(kTRUE);
//		  varNPhi4K.setVal(0);
		  res = pdfModel.fitTo(*dset ,Minos(true),Extended(true), Save(true), PrintLevel(0));
		  EDM = res->edm();
		}
		
		if(TMath::Abs(varN2Phi2K.getVal())<(varN2Phi2K.getError()/2))
		{
		  varN2Phi2K.setConstant(kTRUE);
//		  varN2Phi2K.setVal(0);
		  res = pdfModel.fitTo(*dset ,Minos(true),Extended(true), Save(true), PrintLevel(0));
		  EDM = res->edm();
		}
		
// 		if(TMath::Abs(varN2Phi2K.getVal())<(varN2Phi2K.getError()/10))
// 		{
// 		  varN2Phi2K.setConstant(kTRUE);
// 		  varN2Phi2K.setVal(0);
// 		  res = pdfModel.fitTo(*dset ,Minos(true),Extended(true), Save(true), PrintLevel(0));
// 		  EDM = res->edm();
// 		}
		
// 		if(EDM<2e-3 & TMath::Abs(varNPhi4K.getVal())<(varNPhi4K.getError()/2))
// 		{
// 		  varNPhi4K.setConstant(kTRUE);
// 		  varNPhi4K.setVal(0);
// 		  res = pdfModel.fitTo(*dset ,Minos(true),Extended(true), Save(true), PrintLevel(0));
// 		  EDM = res->edm();
// 		}

		if(EDM>1e-3)return;
		
		
		ey1[i]= -varNTriPhi.getErrorLo();
		ey2[i]= varNTriPhi.getErrorHi();
		if(ey1[i]==0) ey1[i]=varNTriPhi.getError();
		if(ey1[i]>varNTriPhi.getVal()) ey1[i]=varNTriPhi.getVal();
	
        
//!!!!!!
		if(varNTriPhi.getVal()<=1.2)
		{
		  ey1[i]=varNTriPhi.getVal();
		  if(varNTriPhi.getVal()<0.1) ey2[i]=1.5;
		}
		
		
		xx[i]=(massBLo+massBHi)/2;
		yy[i]=varNTriPhi.getVal();
        edm[i]=EDM;
		ex1[i]=binWidthB/2;
		ex2[i]=binWidthB/2;
		
		histBTriPhi->SetBinContent(i,varNTriPhi.getVal());
		
		varN6K.setConstant(kFALSE);
		varN2Phi2K.setConstant(kFALSE);
		varNPhi4K.setConstant(kFALSE);
		
        
        TCanvas* canvBin = new TCanvas("canvBin", "canvBin", 1200, 400);
        canvBin->Divide(3, 1);
        
        RooPlot* frame1bin = Phi1_MM_Mix.frame(Title("#phi_{1} mass"));
        dset->plotOn(frame1bin, Binning(binNPhi, minMassPhi, maxMassPhi));
        pdfModel.plotOn(frame1bin);
        pdfModel.plotOn(frame1bin, Components(pdfRoot1), LineStyle(kDashed));
        
        RooPlot* frame2bin = Phi2_MM_Mix.frame(Title("#phi_{2} mass"));
        dset->plotOn(frame2bin, Binning(binNPhi, minMassPhi, maxMassPhi));
        pdfModel.plotOn(frame2bin);
        pdfModel.plotOn(frame2bin, Components(pdfRoot2), LineStyle(kDashed));
        
        RooPlot* frame3bin = Phi3_MM_Mix.frame(Title("#phi_{3} mass"));
        dset->plotOn(frame3bin, Binning(binNPhi, minMassPhi, maxMassPhi));
        pdfModel.plotOn(frame3bin);
        pdfModel.plotOn(frame3bin, Components(pdfRoot3), LineStyle(kDashed));
        
        canvBin->cd(1);
        frame1bin->Draw();
        canvBin->cd(2);
        frame2bin->Draw();
        canvBin->cd(3);
        frame3bin->Draw();
        
		sprintf(label, "3Dpics/B_m_scaled>%i&&B_m_scaled<%i.pdf", massBLo, massBHi);
        canvBin->SaveAs(label);
        
		delete dset;
		delete res;
        delete canvBin;
	}
	
//DRAW
	TCanvas* canvTest = new TCanvas("canvTest", "canvTest", 1200, 400);
	canvTest->Divide(3, 1);
	

		massBsLo = minMassBReg;
		massBsHi = maxMassBReg;
 
		sprintf(label, "B_m_scaled>%i&&B_m_scaled<%i", massBsLo, massBsHi);
		RooDataSet* dset2 = dsetFull->reduce(Cut(label), Name("dset2"), Title("dset2"));
		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));

		RooPlot* frame1 = Phi1_MM_Mix.frame(Title("#phi_{1} mass"));
		dset2->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel1.plotOn(frame1);
        dset2->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
//		pdfModel1.plotOn(frame1, Components(pdfRoot1), LineStyle(kDashed));

		RooPlot* frame2 = Phi2_MM_Mix.frame(Title("#phi_{2} mass"));
		dset2->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel2.plotOn(frame2);
        dset2->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
//		pdfModel2.plotOn(frame2, Components(pdfRoot2), LineStyle(kDashed));
		
		RooPlot* frame3 = Phi3_MM_Mix.frame(Title("#phi_{3} mass"));
		dset2->plotOn(frame3, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel3.plotOn(frame3);
        dset2->plotOn(frame3, Binning(binNPhi, minMassPhi, maxMassPhi));
//		pdfModel3.plotOn(frame3, Components(pdfRoot3), LineStyle(kDashed));

    
        Double_t yMax = frame2->GetMaximum();
        frame1->SetMaximum(yMax);
        frame3->SetMaximum(yMax);
    
        canvTest->cd(1)->SetLeftMargin(0.06);
		canvTest->cd(1)->SetRightMargin(0.01);
		frame1->Draw();
		canvTest->cd(2)->SetRightMargin(0.035);
        canvTest->cd(2)->SetLeftMargin(0.035);
		frame2->Draw();
		canvTest->cd(3)->SetLeftMargin(0.01);
        canvTest->cd(3)->SetRightMargin(0.06);
		frame3->Draw();

//		delete frame1;
//		delete frame2;
//		delete frame3;
//
//		delete dset2;
//		delete res;
	

    canvTest->Draw();
    
	FILE *fp = fopen("triPhiPureOut.txt", "w");
	for(int i=0;i<binNBReg;i++)
	    fprintf(fp,"%f %f %f %f %f %f \n",xx[i],yy[i],ex1[i],ex2[i],ey1[i],ey2[i]);
	fclose(fp);
    
    
    FILE *fp = fopen("triPhiPureOut_Bins.txt", "w");
    for(int i=0;i<binNBReg;i++)
        fprintf(fp,"%f    %3.1f     %3.1f     %3.1f     %3.1f \n",edm[i],xx[i],yy[i],ey1[i],ey2[i]);
    fclose(fp);

	TGraphAsymmErrors* triPhiGr = new TGraphAsymmErrors(binNBReg, xx, yy, ex1, ex2, ey1, ey2);
	
	TCanvas* canvA = new TCanvas("canvA", "canvA", 800, 700);
	triPhiGr->Draw("AP");

}

void SysFramework()
{
  triPhiPure(0);
  triPhiPure(2);
  triPhiPure(4);
}


