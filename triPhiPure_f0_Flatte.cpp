
  

using namespace RooFit;


//0- FixSigma
//1- VariableSigma
//2- Sigma MC

//3 - Exp bg
//4 - Shift bg


double triPhiPure_f0_Flatte(Double_t Mass, Double_t g2g1Ratio){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");


	RooRealVar varg0("varg0","varg0",168);
	RooRealVar varg1("varg1","varg1",168*g2g1Ratio); //+-0.32
	RooRealVar varPiMass("varPiMass","varPiMass",139.57);
	RooRealVar varKMass("varKMass","varKMass",493.67);		

	RooRealVar varf0Mass("varf0Mass", "varf0Mass", Mass);
//	RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", Gamma);	
	
        int Type=0;
	
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	
	
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	
	Float_t minMassBReg = 5250;
	Float_t maxMassBReg = 5500;
	
        if(Type==4)
	{
	  minMassBReg-=5;
	  maxMassBReg-=5;
	}	
	
	Float_t binWidthB = 10.;
	Int_t binNBReg = int((maxMassBReg-minMassBReg)/binWidthB);
	
	Float_t BsMass = 5366.77; // ±0.24 meV
	
	RooRealVar B_m_scaled("B_m_scaled", "B_m_scaled", minMassBReg, maxMassBReg, "MeV");
	RooRealVar Phi1_MM_Mix("Phi1_MM_Mix", "Phi1_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_MM_Mix("Phi2_MM_Mix", "Phi2_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi3_MM_Mix("Phi3_MM_Mix", "Phi3_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	
	TH1F* histBTriPhi = new TH1F("histBTriPhi", "histBTriPhi", binNBReg, minMassBReg, maxMassBReg);

	TChain* chain = new TChain("DecayTree");
	chain->Add("Data_NewTrig/AllStat3phi.root");

	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(B_m_scaled, Phi1_MM_Mix, Phi2_MM_Mix, Phi3_MM_Mix), "");
	RooPlot* frame0 = B_m_scaled.frame(Title("B_m_scaled"));

	
	frame0->SetAxisRange(minMassBReg,maxMassBReg);
	Int_t binNBReg = int((maxMassBReg-minMassBReg)/binWidthB);
	dsetFull->plotOn(frame0, Binning(binNBReg, minMassBReg, maxMassBReg));
	TCanvas* canvDirty = new TCanvas("canvDirty", "canvDirty", 800, 600);
	frame0->Draw();

	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);
	
	
	switch(Type)
	{
	  case 1:
	    RooRealVar varSigma("varSigma", "varSigma", 1, 0.1, 4);
	    break;
	  case 2:
	    RooRealVar varSigma("varSigma", "varSigma", 1.15);
	    break;
	  default:
	    RooRealVar varSigma("varSigma", "varSigma", 1.20);
	    break;
	}
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	
	
	RooRealVar varA1("varA1","varA1",0);
	RooRealVar varA2("varA2","varA2",0);
	RooRealVar varA3("varA3","varA3",0);
	

	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
							
			      RooArgList(Phi1_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	RooGenericPdf pdfRoot1("pdfRoot1", "pdfRoot1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM_Mix, var2KMass,varA1));	
	
	
	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
						  RooArgList(Phi2_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
 	RooGenericPdf pdfRoot2("pdfRoot2", "pdfRoot2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM_Mix, var2KMass,varA2));
	
	
	RooGenericPdf pdfPhi3("pdfPhi3", "pdfPhi3", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
							  RooArgList(Phi3_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	RooGenericPdf pdfRoot3("pdfRoot3", "pdfRoot3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi3_MM_Mix, var2KMass,varA3));

	
	
	RooFlatte pdfFlatte1("pdfFlatte1","pdfFlatte1",Phi1_MM_Mix, varf0Mass, 
			   				  varg1, varKMass,  varKMass,
			   				  varg0, varPiMass, varPiMass);
	RooGenericPdf Thresh1("Thresh1","Thresh1","(@0>@1)*TMath::Sqrt(TMath::Abs(@0-@1))",RooArgList(Phi1_MM_Mix,var2KMass));
	RooProdPdf pdff01("f0BW1","f0BW1",RooArgSet(pdfFlatte1,Thresh1));		
	
	RooFlatte pdfFlatte2("pdfFlatte2","pdfFlatte2",Phi2_MM_Mix, varf0Mass, 
			   				  varg1, varKMass,  varKMass,
			   				  varg0, varPiMass, varPiMass);
	RooGenericPdf Thresh2("Thresh2","Thresh2","(@0>@1)*TMath::Sqrt(TMath::Abs(@0-@1))",RooArgList(Phi2_MM_Mix,var2KMass));
	RooProdPdf pdff02("f0BW2","f0BW2",RooArgSet(pdfFlatte2,Thresh2));		
	
	RooFlatte pdfFlatte3("pdfFlatte3","pdfFlatte3",Phi3_MM_Mix, varf0Mass, 
			   				  varg1, varKMass,  varKMass,
			   				  varg0, varPiMass, varPiMass);
	RooGenericPdf Thresh3("Thresh3","Thresh3","(@0>@1)*TMath::Sqrt(TMath::Abs(@0-@1))",RooArgList(Phi3_MM_Mix,var2KMass));
	RooProdPdf pdff03("f0BW3","f0BW3",RooArgSet(pdfFlatte3,Thresh3));		
		

	RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot1A3("pdfRoot1A3", "pdfRoot1A3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM_Mix, var2KMass,varA3));
	
	RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot2A3("pdfRoot2A3", "pdfRoot2A3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM_Mix, var2KMass,varA3));
	
	RooGenericPdf pdfRoot3A1("pdfRoot3A1", "pdfRoot3A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi3_MM_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot3A2("pdfRoot3A2", "pdfRoot3A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi3_MM_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot3A3("pdfRoot3A3", "pdfRoot3A3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi3_MM_Mix, var2KMass,varA3));
	
	
	RooProdPdf pdfSSS("pdfSSS", "pdfSSS", RooArgSet(pdfPhi1, pdfPhi2, pdfPhi3));
	
	RooProdPdf pdfS1S2B3("pdfS1S2B3", "pdfS1S2B3", RooArgSet(pdfPhi1, pdfPhi2, pdfRoot3A1));
	RooProdPdf pdfS1B2S3("pdfS1B2S3", "pdfS1B2S3", RooArgSet(pdfPhi1, pdfRoot2A1, pdfPhi3));
	RooProdPdf pdfB1S2S3("pdfB1S2S3", "pdfB1S2S3", RooArgSet(pdfRoot1A1, pdfPhi2, pdfPhi3));
	
	RooProdPdf pdfS1B2B3("pdfS1B2B3", "pdfS1B2B3", RooArgSet(pdfPhi1, pdfRoot2A2, pdfRoot3A2));
	RooProdPdf pdfB1S2B3("pdfB1S2B3", "pdfB1S2B3", RooArgSet(pdfRoot1A2, pdfPhi2, pdfRoot3A2));
	RooProdPdf pdfB1B2S3("pdfB1B2S3", "pdfB1B2S3", RooArgSet(pdfRoot1A2, pdfRoot2A2, pdfPhi3));
	
	
	
	RooProdPdf pdffff("pdffff", "pdffff", RooArgSet(pdff01, pdff02, pdff03));	
	
	RooProdPdf pdfS1S2f3("pdfS1S2f3", "pdfS1S2f3", RooArgSet(pdfPhi1, pdfPhi2, pdff03));
	RooProdPdf pdfS1f2S3("pdfS1f2S3", "pdfS1f2S3", RooArgSet(pdfPhi1, pdff02, pdfPhi3));
	RooProdPdf pdff1S2S3("pdff1S2S3", "pdff1S2S3", RooArgSet(pdff01, pdfPhi2, pdfPhi3));
	
	RooProdPdf pdfS1f2f3("pdfS1f2f3", "pdfS1f2f3", RooArgSet(pdfPhi1, pdff02, pdff03));
	RooProdPdf pdff1S2f3("pdff1S2B3", "pdff1S2f3", RooArgSet(pdff01, pdfPhi2, pdff03));
	RooProdPdf pdff1f2S3("pdff1f2S3", "pdff1f2S3", RooArgSet(pdff01, pdff02, pdfPhi3));	
	
	
	RooProdPdf pdff1f2B3("pdff1f2B3", "pdff1f2B3", RooArgSet(pdff01, pdff01, pdfRoot3A1));
	RooProdPdf pdff1B2f3("pdff1B2f3", "pdff1B2f3", RooArgSet(pdff01, pdfRoot2A1, pdff03));
	RooProdPdf pdfB1f2f3("pdfB1f2f3", "pdfB1f2f3", RooArgSet(pdfRoot1A1, pdff02, pdff03));
	
	RooProdPdf pdff1B2B3("pdff1B2B3", "pdff1B2B3", RooArgSet(pdff01, pdfRoot2A2, pdfRoot3A2));
	RooProdPdf pdfB1f2B3("pdfB1f2B3", "pdfB1f2B3", RooArgSet(pdfRoot1A2, pdff02, pdfRoot3A2));
	RooProdPdf pdfB1B2f3("pdfB1B2f3", "pdfB1B2f3", RooArgSet(pdfRoot1A2, pdfRoot2A2, pdff03));	
	
	
	
	
	
	
	RooProdPdf pdfBBB("pdfBBB", "pdfBBB", RooArgSet(pdfRoot1A3, pdfRoot2A3, pdfRoot3A3));
	
	RooRealVar varNTriPhi	("varNTriPhi" , "varNTriPhi" , 100,0, 1e7);
	RooRealVar varN2Phi2K	("varN2Phi2K", "varN2Phi2K", 10,0, 1e7);
	RooRealVar varNPhi4K	("varNPhi4K" , "varNPhi4K" , 0,0, 1e3);
	RooRealVar varN6K	("varN6K"    , "varN6K"    , 0,0, 1e3);
	
	RooRealVar kF0("kF0","kF0",0,100);
	
	RooFormulaVar varNTrif0	("varNTrif0" , "varNTrif0","@0*@1*@1*@1*0" , RooArgList(varNTriPhi,kF0));
	RooFormulaVar varN2f02K	("varN2f02K", "varN2f02K","@0*@1*@1*0",  RooArgList(varN2Phi2K,kF0));
	RooFormulaVar varNf04K	("varNf04K" , "varNf04K" , "@0*@1", RooArgList(varNPhi4K,kF0));	
	RooFormulaVar varN2f0Phi	("varN2f0Phi", "varN2f0Phi", "@0*@1*@*01", RooArgList(varNTriPhi,kF0));
	RooFormulaVar varNf02Phi	("varNf02Phi" , "varNf02Phi"  ,"@0*@1", RooArgList(varNTriPhi,kF0));
	
	
	
	
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1",  
			RooArgList(pdfPhi1   ,pdfPhi1   ,pdfPhi1   ,pdfRoot1A1,pdfPhi1   ,pdfRoot1A2,pdfRoot1A2,pdfRoot1A3), 
			RooArgList(varNTriPhi,varN2Phi2K,varN2Phi2K,varN2Phi2K,varNPhi4K ,varNPhi4K ,varNPhi4K ,varN6K));
	
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2",  
			RooArgList(pdfPhi2   ,pdfPhi2   ,pdfRoot2A1,pdfPhi2   ,pdfRoot2A2,pdfPhi2   ,pdfRoot2A2,pdfRoot2A3), 
			RooArgList(varNTriPhi,varN2Phi2K,varN2Phi2K,varN2Phi2K,varNPhi4K ,varNPhi4K ,varNPhi4K ,varN6K));
	
	RooAddPdf pdfModel3("pdfModel3", "pdfModel3",  
			RooArgList(pdfPhi3   ,pdfRoot3A1,pdfPhi3   ,pdfPhi3   ,pdfRoot3A2,pdfRoot3A2,pdfPhi3  ,pdfRoot3A3), 
			RooArgList(varNTriPhi,varN2Phi2K,varN2Phi2K,varN2Phi2K,varNPhi4K ,varNPhi4K ,varNPhi4K,varN6K));
	

	
	RooArgList pdfList;
	pdfList.add(pdfSSS);
	pdfList.add(pdfS1S2B3);
	pdfList.add(pdfS1B2S3);
	pdfList.add(pdfB1S2S3);
	pdfList.add(pdfS1B2B3);
	pdfList.add(pdfB1S2B3);
	pdfList.add(pdfB1B2S3);
	pdfList.add(pdfBBB);
	pdfList.add(pdffff);
	pdfList.add(pdff1f2B3);
	pdfList.add(pdff1B2f3);
	pdfList.add(pdfB1f2f3);
	pdfList.add(pdff1B2B3);
	pdfList.add(pdfB1f2B3);
	pdfList.add(pdfB1B2f3);
	pdfList.add(pdfS1S2f3);
	pdfList.add(pdfS1f2S3);
	pdfList.add(pdff1S2S3);
	pdfList.add(pdfS1f2f3);
	pdfList.add(pdff1S2f3);
	pdfList.add(pdff1f2S3);
	
	RooArgList pdfNorms;
	pdfNorms.add(varNTriPhi);
	pdfNorms.add(varN2Phi2K);
	pdfNorms.add(varN2Phi2K);
	pdfNorms.add(varN2Phi2K);
	pdfNorms.add(varNPhi4K);
	pdfNorms.add(varNPhi4K);
	pdfNorms.add(varNPhi4K);
	pdfNorms.add(varN6K);
	pdfNorms.add(varNTrif0);
	pdfNorms.add(varN2f02K);
	pdfNorms.add(varN2f02K);
	pdfNorms.add(varN2f02K);
	pdfNorms.add(varNf04K);
	pdfNorms.add(varNf04K);
	pdfNorms.add(varNf04K);
	pdfNorms.add(varNf02Phi);
	pdfNorms.add(varNf02Phi);
	pdfNorms.add(varNf02Phi);
	pdfNorms.add(varN2f0Phi);
	pdfNorms.add(varN2f0Phi);
	pdfNorms.add(varN2f0Phi);
	
	
	RooAddPdf pdfModel("pdfModel", "pdfModel",  pdfList,pdfNorms);

	char label[200];
	Float_t massBLo, massBHi;
	Double_t ey1[1000], ey2[1000], xx[1000], yy[1000], ex1[1000], ex2[1000];

	
	TCanvas* canvTest = new TCanvas("canvTest", "canvTest", 1200, 400);
	canvTest->Divide(3, 1);
	
	for (Int_t i=0; i<1; i++){
		massBsLo = minMassBReg;
		massBsHi = maxMassBReg;
 
		sprintf(label, "B_m_scaled>%i&&B_m_scaled<%i", massBsLo, massBsHi);
		RooDataSet* dset2 = dsetFull->reduce(Cut(label), Name("dset2"), Title("dset2"));
		
	kF0.setConstant(kTRUE);	
// 	varN2f02K.setConstant(kTRUE);	
// 	varNf04K.setConstant(kTRUE);	
// 	varN2f0Phi.setConstant(kTRUE);	
// 	varNf02Phi.setConstant(kTRUE);	
	
		
	kF0.setVal(0);	
//kF0	varN2f02K.setVal(0);	
/*	varNf04K.setVal(0);	
	varN2f0Phi.setVal(0);	
	varNf02Phi.setVal(0);*/		
		
		
		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
		
	Double_t zeroNTriPhi=varNTriPhi.getVal();
	
	
	varN2Phi2K.setConstant(kTRUE);	
	varNPhi4K.setConstant(kTRUE);	
	varN6K.setConstant(kTRUE);	

	kF0.setConstant(kFALSE);	
// 	varN2f02K.setConstant(kFALSE);	
// 	varNf04K.setConstant(kFALSE);	
// 	varN2f0Phi.setConstant(kFALSE);	
// 	varNf02Phi.setConstant(kFALSE);	
	
	RooFitResult* res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
	
		RooPlot* frame1 = Phi1_MM_Mix.frame(Title("#phi_{1} mass"));
		dset2->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel1.plotOn(frame1);
		pdfModel1.plotOn(frame1, Components(pdfRoot1), LineStyle(kDashed));

		RooPlot* frame2 = Phi2_MM_Mix.frame(Title("#phi_{2} mass"));
		dset2->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel2.plotOn(frame2);
		pdfModel2.plotOn(frame2, Components(pdfRoot2), LineStyle(kDashed));
		
		RooPlot* frame3 = Phi3_MM_Mix.frame(Title("#phi_{3} mass"));
		dset2->plotOn(frame3, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel3.plotOn(frame3);
		pdfModel3.plotOn(frame3, Components(pdfRoot3), LineStyle(kDashed));

		canvTest->cd(1);
		frame1->DrawClone();
		canvTest->cd(2);
		frame2->DrawClone();
		canvTest->cd(3);
		frame3->DrawClone();

		delete frame1;
		delete frame2;
		delete frame3;

		delete dset2;
		delete res;
	}
	


	
	cout<<varf0Mass.getVal()<<endl;
//	cout<<varf0Gamma.getVal()<<endl;
	
	
	
	cout<<zeroNTriPhi<<endl;
	
	return varNTriPhi.getVal();
}

void Framework()
{
  Double_t results[5];
  int RangeNumber = 5;
  int Type = 0;
  
  results[0]=triPhiPure_f0_Flatte(990,4.12 );
  results[1]=triPhiPure_f0_Flatte(990,4.12-0.32 );
  results[2]=triPhiPure_f0_Flatte(990,4.12+0.32 );
  results[3]=triPhiPure_f0_Flatte(970,4.12 );
  results[4]=triPhiPure_f0_Flatte(1010,4.12 );
  
  cout<<"990 4.12 NDiPhi = "<<results[0]<<endl;
  cout<<"990 4.12-0.32 NDiPhi = "<<results[1]<<endl;
  cout<<"990 4.12+0.32 NDiPhi = "<<results[2]<<endl;
  cout<<"970 4.12 NDiPhi = "<<results[3]<<endl;
  cout<<"1010 4.12 NDiPhi = "<<results[4]<<endl;
}
