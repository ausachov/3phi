#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooExtendPdf.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"


using namespace RooFit;

double bgr(double *x, double *par)  {
        double xx = x[0];
 	return par[0]*(TMath::Exp(par[1]*xx));
}



double FitFunc0(double *x, double *par) 
{
        double val = 0;
	val+=par[0]*TMath::Gaus(x[0],par[1],par[2], kTRUE);	
	val += bgr(x,&par[3]);
	return val;
}

double FitFunc1(double *x, double *par) 
{
        double val = 0;
	val+=par[0]*(par[4]*TMath::Gaus(x[0],par[1],par[2], kTRUE)+(1-par[4])*TMath::Gaus(x[0],par[1],par[2]*par[3], kTRUE));	
	val += bgr(x,&par[5]);
	return val;
}



//0 - 2 Gaus
//1 - 1 Gaus
//2 - blind

void bs(int Type){
  
  
    gROOT->ProcessLine(".x lhcbStyle.C");
    
	Float_t BsMass = 5366.77; // ±0.24 meV

	Float_t minMass = 5250;
	Float_t maxMass = 5500;
	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar varMass("B_m_scaled", "B_m_scaled", minMass, maxMass, "MeV");
	RooRealVar Counts("Counts","Counts",0,100);
	RooDataSet DataSet("DataSet","DataSet",RooArgSet(varMass,Counts),StoreAsymError(RooArgSet(varMass,Counts)));
  
	Float_t xx[1000], yy[1000], ey1[1000], ey2[1000], xx[1000], yy[1000], ex1[1000], ex2[1000];
   	FILE *fp = fopen("triPhiPureOut.txt", "r");
	for(int i=0;i<binN;i++)
	{
	    fscanf(fp,"%f",&xx[i]);
	    fscanf(fp,"%f",&yy[i]);
	    fscanf(fp,"%f",&ex1[i]);
	    fscanf(fp,"%f",&ex2[i]);
	    fscanf(fp,"%f",&ey1[i]);
	    fscanf(fp,"%f",&ey2[i]);
        
//        ex1[i]=0;
//        ex2[i]=0;
        
        if(Type==2)
        {
            if(xx[i]<5330 || xx[i]>5390)
            {
                varMass = xx[i];
                Counts = yy[i];
                Counts.setAsymError(-ey1[i],ey2[i]);
                DataSet.add(RooArgSet(varMass,Counts));
            }
        }
        else
        {
            varMass = xx[i];
            Counts = yy[i];
            Counts.setAsymError(-ey1[i],ey2[i]);
            DataSet.add(RooArgSet(varMass,Counts));
        }
    
	}
	fclose(fp);


	RooPlot* frame = varMass.frame(Title("3#phi pure")) ;


	
	RooRealVar varBsNumber("N(B_{s})", "varBsNumber", 30, 0, 1000);
 	RooRealVar varBsSigma("#sigma", "varBsSigma", 13, 4, 40);
//	RooRealVar varBsSigma("#sigma", "varBsSigma", 9.84);
	RooRealVar varBsMass("M(B_{s})", "varBsMass", BsMass, BsMass-10, BsMass+10);
	RooGaussian pdfBsN("pdfBsN", "pdfBsN", varMass, varBsMass, varBsSigma);
	
	
	
	RooRealVar FractionN("FractionN","FractionN",0.853);
	RooRealVar SigmaRatio("SigmaRatio","SigmaRatio",2.2);
	
	RooFormulaVar varBsSigmaW("varBsSigmaW","varBsSigmaW","@0*@1",RooArgSet(varBsSigma,SigmaRatio));
	RooGaussian pdfBsW("pdfBsW", "pdfBsW", varMass, varBsMass, varBsSigmaW);
	
	RooAddPdf pdfBs("pdfBs","pdfBs",RooArgList(pdfBsN,pdfBsW),RooArgSet(FractionN));

	RooRealVar varNBgr("varNBgr", "varNBgr", 50, 0, 1e3);
	RooRealVar varE("varE", "varE", 0);
	RooExponential pdfBgr("pdfBgr", "pdfBgr", varMass, varE);
	

	
if(Type==1)
	RooAddPdf pdfModel("pdfModel", "pdfModel", RooArgList(pdfBsN, pdfBgr), RooArgList(varBsNumber, varNBgr));
if(Type==0 || Type==2)
	RooAddPdf pdfModel("pdfModel", "pdfModel", RooArgList(pdfBs, pdfBgr), RooArgList(varBsNumber, varNBgr));
if(Type==3)
{     
	RooRealVar varBsFraction("varBsFraction","varBsFraction",0,1);
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1", RooArgList(pdfBs, pdfBgr), RooArgList(varBsFraction));
	RooExtendPdf pdfModel("pdfModel", "pdfModel", pdfModel1, varNBgr);
}
    
    
    varMass.setRange("LeftSB",5250,5330);
    varMass.setRange("RightSB",5390,5500);

    
    if(Type==2)
    {
        varBsNumber.setConstant(kTRUE);
        varBsSigma.setConstant(kTRUE);
        varBsMass.setConstant(kTRUE);
        varBsNumber.setVal(0.);
        pdfModel.chi2FitTo(DataSet,YVar(Counts),PrintLevel(0),Minos(kTRUE));
        
        DataSet.plotOnXY(frame,YVar(Counts));
        pdfModel.plotOn(frame,Normalization(1.,RooAbsReal::RelativeExpected));
        
        frame->Draw();
    }

    else
        pdfModel.chi2FitTo(DataSet,YVar(Counts),Minos(kTRUE),PrintLevel(0));
    
    

	
if(Type!=2)
{
	TCanvas* canvB = new TCanvas("canvB", "canvB", 1000, 600);
	
	TF1* BgFunction = new TF1("BgFunction",bgr,minMass,maxMass,2);
	BgFunction->SetParameters(1,varE.getVal());
	Double_t BgNorm = BgFunction->Integral(minMass,maxMass);
	cout<<BgNorm<<endl;
	
	
	TGraphAsymmErrors* triPhiGr = new TGraphAsymmErrors(binN, xx, yy, ex1, ex2, ey1, ey2);
    triPhiGr->GetXaxis()->SetLimits(minMass,maxMass);
	triPhiGr->Draw("AP");
    if(Type==0)
    {
        TF1 *FitFunction = new TF1("FitFunction",FitFunc0,minMass,maxMass,5);
        FitFunction->SetParNames("Amp","Centre","Sigma","BgAmp","Exp");
        FitFunction->SetParameters(varBsNumber.getVal(),varBsMass.getVal(),varBsSigma.getVal(),varNBgr.getVal()/BgNorm,varE.getVal());
        //FitFunction->FixParameter(4,0);
    }

    if(Type==1)
    {
        TF1 *FitFunction = new TF1("FitFunction",FitFunc1,minMass,maxMass,7);
        FitFunction->SetParameters(varBsNumber.getVal(),varBsMass.getVal(),varBsSigma.getVal(),
                       SigmaRatio.getVal(), FractionN.getVal(), 
                       varNBgr.getVal()/BgNorm,varE.getVal());
        FitFunction->FixParameter(4,0);
    }
    
	FitFunction->SetNpx(2000);
	FitFunction->SetLineColor(kBlue);
	FitFunction->Draw("same");
    triPhiGr->Draw("P");
    
    triPhiGr->Fit("FitFunction","NMQ","",minMass,maxMass);
    triPhiGr->Fit("FitFunction","NM","",minMass,maxMass);
    triPhiGr->Fit("FitFunction","NM","",minMass,maxMass);
    //triPhiGr->Fit("FitFunction","NME","",minMass,maxMass);
    cout<<"NDOF = "<<FitFunction->GetNDF()<<endl;
    
    //FitFunction->FixParameter(3,2*FitFunction->GetParameter(3));
    
//    cout<<"integral Bg in Signal region = "
//    <<FitFunction->Integral(varBsMass.getVal()-3*varBsSigma.getVal(),varBsMass.getVal()+3*varBsSigma.getVal())
//    <<" +-  "
//    <<FitFunction->IntegralError(varBsMass.getVal()-3*varBsSigma.getVal(),varBsMass.getVal()+3*varBsSigma.getVal(),FitFunction->GetParameters())
//    <<endl;
}
    

}



