// $Id: $
{
  TChain *chain = new TChain("DecayTree");
  chain->Add("Bs/AllBsMC_Reduced.root");
  
  
  TCut Cut("");

  TFile *newfile = new TFile("3phi_afterCut.root","recreate");
  TTree *newtree = chain->CopyTree(Cut);
 
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
  
}

