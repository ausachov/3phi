#include <TMath.h>



using namespace RooFit;


void MCResolution()
{
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");

	Double_t minResid = -40;
	Double_t maxResid = 40;
	Int_t nch=30;
	Double_t binWidth = (maxResid-minResid)/nch;
	
	
	TChain * DecayTree=new TChain("DecayTree");
	DecayTree->Add("../../MC/CAL_0/3phi/AllMC_Reduced.root");
	const Int_t NEn=DecayTree->GetEntries();
	Double_t JPsi_Mass;
	Double_t True_PE;
	Double_t True_PT;
	Double_t True_PZ;
  
	Double_t True_M;
	
	DecayTree->SetBranchAddress("B_MM",&JPsi_Mass); 
	DecayTree->SetBranchAddress("B_TRUEP_E",&True_PE);
	DecayTree->SetBranchAddress("B_TRUEPT",&True_PT);
	DecayTree->SetBranchAddress("B_TRUEP_Z",&True_PZ);

	TH1D * Resolution_hist = new TH1D("MJPsi-TrueMB","MJPsi-TrueMB" , nch, minResid, maxResid);
	for (Int_t i=0; i<NEn; i++) 
	{
		DecayTree->GetEntry(i);
		True_M = TMath::Sqrt(True_PE*True_PE-True_PT*True_PT-True_PZ*True_PZ);	
		Resolution_hist->Fill(JPsi_Mass-True_M);
	}
	
	
	
	RooRealVar varMass("M(#phi#phi)", "M(#phi#phi)", minResid, maxResid, "MeV");
	RooDataHist* DataSet = new RooDataHist("DataSet", "DataSet", varMass, Resolution_hist);

	RooPlot* frame = varMass.frame(Title("Cc Candidates"));

	RooArgList listNorm, listComp;
	RooArgSet showParams;
	
	
	RooRealVar varCcNumber("CcNumber", "CcNumber", 5000, 0, 1e5);
        RooRealVar varCentre("varCentre", "varCentre", 0, minResid, maxResid);
	RooRealVar varFraction1("varFraction1","varFraction1",0.9,0,1);
	
	RooRealVar varSigma1("#sigma1", "#sigma1", 6, 4, 12);
	RooGaussian pdfSignal1("pdfSignal1", "pdfSignal1", varMass, varCentre, varSigma1);

	RooRealVar varSigma2("#sigma2", "#sigma2", 15, 8, 60);
	RooGaussian pdfSignal2("pdfSignal2", "pdfSignal2", varMass, varCentre, varSigma2);
	
	RooAddPdf pdfSignalSum("pdfSignalSum", "pdfSignalSum", RooArgList(pdfSignal1, pdfSignal2), varFraction1);
	
	listNorm.add(varCcNumber);
	listComp.add(pdfSignalSum);
	
	RooRealVar varBgrN("varBgrN", "varBgrN", 0, 0, 1e9);
	RooRealVar varC("varC", "varC", -1e-4, -1, 0);
	RooExponential pdfBgr("pdfBgr", "pdfBgr", varMass, varC);
// 	listNorm.add(varBgrN);
// 	listComp.add(pdfBgr);
	

	RooAddPdf pdfModel("pdfModel", "pdfModel", listComp, listNorm);
	
	
	RooFitResult* results = pdfModel.fitTo(*DataSet,Save(true), Strategy(1), PrintLevel(0), NumCPU(4));
	RooPlot* frame =varMass.frame();

	DataSet->plotOn(frame, Binning(nch, minResid, maxResid));
	pdfModel.plotOn(frame);

	showParams.add(varCcNumber);
	showParams.add(varBgrN);
	showParams.add(varSigma1);
	showParams.add(varSigma2);
	showParams.add(varFraction1);
	showParams.add(varCentre);
	pdfModel.paramOn(frame, Layout(0.65, 0.95, 0.95), Parameters(showParams), ShowConstants(true));

	TCanvas* canvB = new TCanvas("canvB", "canvB", 1000, 600);
	frame->Draw();
	
	showParams.writeToFile("params.txt");
}
