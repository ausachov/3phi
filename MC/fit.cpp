
using namespace RooFit;

void fit(){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");

	Float_t etacMass =	2981.0; // ±1.1 MeV
	Float_t jpsiMass =	3096.92;
	Float_t chi0Mass =	3414.75; // ±0.31 MeV
	Float_t chi1Mass =	3510.66; // ±0.07 MeV <-- refer point
	Float_t hcMass   = 	3525.41; // ±0.16 MeV
	Float_t chi2Mass =	3556.20; // ±0.09 MeV
	Float_t etac2Mass =	3638.9; // ±1.3 MeV
	Float_t psi2sMass =	3686.11;
	Float_t psi3770Mass =	3773.15; // ±0.33 MeV
	Float_t x3872Mass = 	3871.69; // ±0.17 MeV
	Float_t x3915Mass = 	3918.4; // ±2.7 MeV

	Float_t minMass = 3300;
	Float_t maxMass = 3800;
	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar varMass("M(#phi#phi)", "M(#phi#phi)", minMass, maxMass, "MeV");



// 	histogramms:
	TFile* file = new TFile("diPhiPureAll.root");
	TH1D *histJpsiDiPhi  = (TH1D*) file->Get("histJpsiDiPhi");
	RooDataHist* DataSet = new RooDataHist("DataSet", "DataSet", varMass, histJpsiDiPhi);

	RooPlot* frame = varMass.frame(Title("M(#phi#phi)"));

	RooArgList listComp, listNorm;
	RooArgSet showParams;
	

//========== fitting model components:
// 	RooRealVar varEtacNumber("N(#eta_{c}(1S))", "varEtacNumber", 6900, 0, 1e4);
// 	RooRealVar varEtacMass("varEtacMass", "varEtacMass", 2985, 2975, 2989);
// 	RooRealVar varEtacGamma("varEtacGamma", "varEtacGamma", 32);
// 	RooRealVar varEtacSigma("varEtacSigma", "varEtacSigma", 6.23, 3 ,10);
// 	RooVoigtian pdfEtac("pdfEtac", "pdfEtac", varMass, varEtacMass, varEtacGamma, varEtacSigma);
// 	listComp.add(pdfEtac);
// 	listNorm.add(varEtacNumber);
// 	showParams.add(varEtacNumber);
// 	showParams.add(varEtacMass);
// 	showParams.add(varEtacGamma);
// 	showParams.add(varEtacSigma);
	
	RooRealVar varChi2Number("N(#chi_{c2})", "varChi2Number", 660, 0, 1e5);
        RooRealVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", chi2Mass-5, chi2Mass+5);
	RooRealVar varChi2Gamma("varChi2Gamma", "varChi2Gamma", 1.97);
	RooRealVar varChi2Sigma("varChi2Sigma", "varChi2Sigma", 6,2,15); 
	RooVoigtian pdfChi2("pdfChi2", "pdfChi2", varMass, varChi2Mass, varChi2Gamma, varChi2Sigma);
	listComp.add(pdfChi2);
	listNorm.add(varChi2Number);
	showParams.add(varChi2Number);




	RooRealVar varBgrNumber("N_{bgr}", "number of background events", 200, 0, 1e5);
	char label[200];
	RooRealVar varA0("varA0", "varA0", -1e-1, +1e-1);
	RooRealVar varC0("varC0", "varC0", -1e-1, 0);
	RooRealVar varD0("varD0", "varD0", -1, 1);
	//RooRealVar varF0("varF0", "varF0", -1, 1);
	RooRealVar var2PhiMass("var2PhiMass", "var2PhiMass", 2038.9);
//  	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "exp(@0*@1)*(1+(@0-@3)*@2+(@0-@3)*(@0-@3)*@4)*sqrt(@0-@3)*(@0>@3)",
//  			RooArgSet(varMass, varC0, varA0, var2PhiMass, varD0));
	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "exp(@0*@1)*sqrt(@0-@2)*(@0>@2)",
			RooArgSet(varMass, varC0, var2PhiMass));					//linear
	sprintf(label, "e^{-c_{0} M}#times(1+c_{1} M)#times #sqrt{M-2M_{#phi}}");

// 	listComp.add(pdfBgr);
// 	listNorm.add(varBgrNumber);

	RooAddPdf pdfModel("pdfModel", "pdfModel", listComp, listNorm);

	results = pdfModel.fitTo(*DataSet,Save(true), Strategy(1), PrintLevel(0), NumCPU(4));
	
	DataSet->plotOn(frame, Binning(binN, minMass, maxMass));
	pdfModel.plotOn(frame);

	pdfModel.paramOn(frame, Layout(0.75, 0.95, 0.95), Parameters(showParams), ShowConstants(true), Format("NELU",AutoPrecision(1)));
	frame->getAttText()->SetTextSize(0.03) ;

	TCanvas* canvA = new TCanvas("canvA", "canvA", 1000, 600);
	frame->Draw();
	
        showParams.writeToFile("params.txt");

}


