
using namespace RooFit;


double bgr(double *x, double *par)  {
        double xx = x[0]-5250;
	return par[0]*10*(TMath::Exp(-par[1]*xx));
}



double FitFunc(double *x, double *par) 
{
        double val = 0;
	val+=10*par[0]*TMath::Gaus(x[0],par[1],par[2], kTRUE);	
	val += bgr(x,&par[3]);
	return val;
}


void TriPhiPure(){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");

	Float_t minMassB = 3000;
	Float_t maxMassB = 11000;
	Float_t binWidthB = 10.;
	Int_t binNB = int((maxMassB-minMassB)/binWidthB);
	
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	
	Float_t minMassBReg = 5250;
	Float_t maxMassBReg = 5500;
	Int_t binNBReg = int((maxMassBReg-minMassBReg)/binWidthB);
	
	Float_t BsMass = 5366.77; // ±0.24 meV
	
	RooRealVar B_MM("B_MM", "B_MM", minMassB, maxMassB, "MeV");
	RooRealVar Phi1_MM("Phi1_MM", "Phi1_MM", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_MM("Phi2_MM", "Phi2_MM", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi3_MM("Phi3_MM", "Phi3_MM", minMassPhi, maxMassPhi, "MeV");
	
	TH1F* histBTriPhi = new TH1F("histBTriPhi", "histBTriPhi", binNBReg, minMassBReg, maxMassBReg);

	TChain* chain = new TChain("DecayTree");
	chain->Add("3phi_afterCut.root");

	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(B_MM, Phi1_MM, Phi2_MM, Phi3_MM), "");
	RooPlot* frame0 = B_MM.frame(Title("B_MM"));

	
	frame0->SetAxisRange(minMassBReg,maxMassBReg);
	Int_t binNBReg = int((maxMassBReg-minMassBReg)/binWidthB);
	dsetFull->plotOn(frame0, Binning(binNBReg, minMassBReg, maxMassBReg));
	TCanvas* canv0 = new TCanvas("canv0", "canv0", 800, 600);
	frame0->Draw();

	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);
	RooRealVar varSigma("varSigma", "varSigma", 1.184);
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	

	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi1_MM, var2KMass, varPhiMass,varSigma,varPhiGamma));
	RooRealVar varA1("varA1", "varA1", 0/*,10*/);
	RooGenericPdf pdfRoot1("pdfRoot1", "pdfRoot1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM, var2KMass,varA1));	
//	RooGenericPdf pdfRoot1("pdfRoot1", "pdfRoot1", "sqrt(@0-@1)", RooArgList(Phi1_MM, var2KMass));	
	RooRealVar varK1("varK1", "varK1", 1e-5, 1);
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1", RooArgList(pdfPhi1, pdfRoot1), varK1);
	
	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi2_MM, var2KMass, varPhiMass,varSigma,varPhiGamma));
 	RooRealVar varA2("varA2", "varA2", 0/*,10*/);
 	RooGenericPdf pdfRoot2("pdfRoot2", "pdfRoot2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM, var2KMass,varA2));
//	RooGenericPdf pdfRoot2("pdfRoot2", "pdfRoot2", "sqrt(@0-@1)", RooArgList(Phi2_MM, var2KMass));
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2", RooArgList(pdfPhi2, pdfRoot2), varK1);
	
	RooGenericPdf pdfPhi3("pdfPhi3", "pdfPhi3", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi3_MM, var2KMass, varPhiMass,varSigma,varPhiGamma));
	RooRealVar varA3("varA3", "varA3", 0/*,10*/);
	RooGenericPdf pdfRoot3("pdfRoot3", "pdfRoot3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi3_MM, var2KMass,varA3));
//	RooGenericPdf pdfRoot3("pdfRoot3", "pdfRoot3", "sqrt(@0-@1)", RooArgList(Phi3_MM, var2KMass));
	RooAddPdf pdfModel3("pdfModel3", "pdfModel3", RooArgList(pdfPhi3, pdfRoot3), varK1);
	
	
	RooProdPdf pdfModelNE("pdfModelNE", "pdfModelNE", RooArgSet(pdfModel1, pdfModel2, pdfModel3));
	//RooRealVar varEvN("varEvN", "varEvN", 0, 1e5);
	RooRealVar varNTriPhi("varNTriPhi", "varNTriPhi",5,0,10000);
	RooFormulaVar varEvN("varEvN", "varEvN", "@0/@1/@1/@1", RooArgList(varNTriPhi, varK1));
	
	RooExtendPdf pdfModel("pdfModel", "pdfModel",  pdfModelNE, varEvN);
	

//	RooFormulaVar varNTriPhi("varNTriPhi", "varNTriPhi", "@0*@1*@1*@1", RooArgList(varEvN, varK1));
// 	RooFormulaVar varNTriK("varNTriK", "varNTriK", "@0*(1-@1)*(1-@1)*(1-@1)", RooArgList(varEvN, varK1));
// 	RooFormulaVar varNDiPhiK("varNDiPhiK", "varNDiPhiK", "3*@0*(@1*@1*(1-@1))", RooArgList(varEvN, varK1));
// 	RooFormulaVar varNPhiDiK("varNPhiDiK", "varNPhiDiK", "3*@0*(@1*(1-@1)*(1-@1))", RooArgList(varEvN, varK1));

	char label[200];
	Float_t massBLo, massBHi;
	Double_t ey1[1000], ey2[1000], xx[1000], yy[1000], ex1[1000], ex2[1000];
	
	
	RooRealVar varMass("varMass", "varMass", 5250, 5500);
	RooRealVar Signal("Signal", "Signal", 0);
	varMass.setMin(minMassBReg);
	varMass.setMax(maxMassBReg);
	Signal.setMin(0);
	Signal.setMax(50);
	
// 	RooDataSet* dsetPure = new RooDataSet("dsetPure", "dsetPure", RooArgSet(varMass, Signal), 
// 					      StoreError(RooArgSet(varMass)),StoreAsymError(RooArgSet(Signal)));
	
	
	for (Int_t i=0; i<binNBReg; i++){
		massBLo = minMassBReg + i*binWidthB;
		massBHi = minMassBReg + (i+1)*binWidthB;

		sprintf(label, "B_MM>%i&&B_MM<%i", massBLo, massBHi);
		RooDataSet* dset = dsetFull->reduce(Cut(label), Name("dset"), Title("dset"));
		RooFitResult* res = pdfModel.fitTo(*dset, Minos(true), Strategy(2), Save(true), PrintLevel(0));
		if(varNTriPhi.getErrorLo()==0)
		  RooFitResult* res = pdfModel.fitTo(*dset, Minos(true), Strategy(2), Save(true), PrintLevel(0));
		
		ey1[i]= -varNTriPhi.getErrorLo();
		ey2[i]= varNTriPhi.getErrorHi();
		if(ey1[i]==0) ey1[i]=varNTriPhi.getError();
		if(ey1[i]>varNTriPhi.getVal()) ey1[i]=varNTriPhi.getVal();
		
		if(varNTriPhi.getVal()<0.1)
		{
		  ey1[i]=varNTriPhi.getVal();
		  ey2[i]=1.5;
		}
		xx[i]=(massBLo+massBHi)/2;
		yy[i]=varNTriPhi.getVal();
		ex1[i]=binWidthB/2;
		ex2[i]=binWidthB/2;
		
		histBTriPhi->SetBinContent(i,varNTriPhi.getVal());
		
// 	        varMass.setVal((massBLo+massBHi)/2);
// 		Signal.setVal(varNTriPhi.getVal());
// 		Signal.setAsymError(-ey1,ey2);
// 		varMass.setError(binWidthB/2);
// 		
// 		dsetPure->add(RooArgSet(varMass, Signal));
		delete dset;
		delete res;	
	}
	
	
	TCanvas* canvTest = new TCanvas("canvTest", "canvTest", 1200, 400);
	canvTest->Divide(3, 1);
	
	for (Int_t i=0; i<1; i++){
		massBsLo = 3000;
		massBsHi = 11000;
 
		sprintf(label, "B_MM>%i&&B_MM<%i", massBsLo, massBsHi);
		RooDataSet* dset2 = dsetFull->reduce(Cut(label), Name("dset2"), Title("dset2"));
		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));

		RooPlot* frame1 = Phi1_MM.frame(Title("#phi_{1} mass"));
		dset2->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel1.plotOn(frame1);
		pdfModel1.plotOn(frame1, Components(pdfRoot1), LineStyle(kDashed));

		RooPlot* frame2 = Phi2_MM.frame(Title("#phi_{2} mass"));
		dset2->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel2.plotOn(frame2);
		pdfModel2.plotOn(frame2, Components(pdfRoot2), LineStyle(kDashed));
		
		RooPlot* frame3 = Phi3_MM.frame(Title("#phi_{3} mass"));
		dset2->plotOn(frame3, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel3.plotOn(frame3);
		pdfModel3.plotOn(frame3, Components(pdfRoot3), LineStyle(kDashed));

		canvTest->cd(1);
		frame1->DrawClone();
		canvTest->cd(2);
		frame2->DrawClone();
		canvTest->cd(3);
		frame3->DrawClone();

		delete frame1;
		delete frame2;
		delete frame3;

		delete dset2;
		delete res;
	}
	
	RooDataHist* DataSetPure = new RooDataHist("DataSetPure", "DataSetPure", varMass, histBTriPhi);
	
	RooRealVar varBsNumber("varBsNumber", "varBsNumber", 65, 0, 1e7);
	RooRealVar varBsSigma("#sigma", "varBsSigma", 13, 3, 30);
	RooRealVar varBsMass("M(B_{s})", "varBsMass", BsMass, BsMass-10, BsMass+10);
	RooGaussian pdfBs("pdfBs", "pdfBs", varMass, varBsMass, varBsSigma);

	RooRealVar varBgrNBs("varBgrNBs", "varBgrNBs", 10, 0, 1e3);
	RooRealVar varC4("varC4", "varC4", 0, -1, 1);
	RooExponential pdfBsBgr("pdfBsBgr", "pdfBsBgr", varMass, varC4);

	RooAddPdf pdfModelBs("pdfModelBs", "pdfModelBs", RooArgList(pdfBs, pdfBsBgr), RooArgList(varBsNumber, varBgrNBs));
	RooFitResult* results2 = pdfModelBs.fitTo(*DataSetPure, Save(true));
	
	RooPlot* frame4 = varMass.frame(Title("B_Mass"));
	DataSetPure->plotOn(frame4);
	pdfModelBs.plotOn(frame4);
	TCanvas* canvRes= new TCanvas("canvRes", "canvRes", 1200, 400);
	frame4->Draw();
}


