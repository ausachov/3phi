#include <TMath.h>




void CosThetaM2phi_Comp(int Type=1)
{
    
    
    
  gROOT->Reset();
   gStyle->SetOptStat(001);
    
  gROOT->ProcessLine(".x lhcbStyle.C");
  
  Double_t range0=-1;
  Double_t range1=1;
  Int_t nch=20;
  Double_t binWidth = (range1-range0)/nch;
  
  
  Double_t M2range0=2000;
  Double_t M2range1=4400;
  Int_t M2nch=15;
  
  Double_t Mrange0=5250;
  Double_t Mrange1=5500;
  Int_t Mnch=25;
  
  Double_t PX1, PY1, PZ1, M1, PE1;
  Double_t PX2, PY2, PZ2, M2, PE2;
  Double_t PX3, PY3, PZ3, M3, PE3;
  Double_t PX, PY, PZ, PE;
  
  Double_t BMass;
  
  TChain * DecayTree=new TChain("DecayTree");
  DecayTree->Add("3phi_BsMCut.root");
  const Int_t NEn=DecayTree->GetEntries();
  Double_t Cos1, Cos2, Cos3;
  DecayTree->SetBranchAddress("Phi1_CosTheta",&Cos1);
  DecayTree->SetBranchAddress("Phi2_CosTheta",&Cos2);
  DecayTree->SetBranchAddress("Phi3_CosTheta",&Cos3);
  
  DecayTree->SetBranchAddress("Phi1_PX",&PX1);
  DecayTree->SetBranchAddress("Phi1_PY",&PY1);
  DecayTree->SetBranchAddress("Phi1_PZ",&PZ1);
  DecayTree->SetBranchAddress("Phi1_MM",&M1);
  DecayTree->SetBranchAddress("Phi1_PE",&PE1);

  DecayTree->SetBranchAddress("Phi2_PX",&PX2);
  DecayTree->SetBranchAddress("Phi2_PY",&PY2);
  DecayTree->SetBranchAddress("Phi2_PZ",&PZ2);
  DecayTree->SetBranchAddress("Phi2_MM",&M2);
  DecayTree->SetBranchAddress("Phi2_PE",&PE2);
  
  DecayTree->SetBranchAddress("Phi3_PX",&PX3);
  DecayTree->SetBranchAddress("Phi3_PY",&PY3);
  DecayTree->SetBranchAddress("Phi3_PZ",&PZ3);
  DecayTree->SetBranchAddress("Phi3_MM",&M3);
  DecayTree->SetBranchAddress("Phi3_PE",&PE3);


  TH1D * data_hist = new TH1D("#phi cos(#theta)","#phi cos(#theta)" , nch, range0, range1);
  TH1D * data_M2hist = new TH1D("data_M2hist","data_M2hist" , M2nch, M2range0, M2range1);
  for (Int_t i=0; i<NEn; i++) 
  {
	  DecayTree->GetEntry(i);
	  if((Cos1>range0)&(Cos1<range1))data_hist->Fill(Cos1);
	  if((Cos2>range0)&(Cos2<range1))data_hist->Fill(Cos2);
	  if((Cos3>range0)&(Cos3<range1))data_hist->Fill(Cos3);
	  
	  PE = PE1 + PE2;
	  PZ = PZ1 + PZ2;
	  PX = PX1 + PX2;
	  PY = PY1 + PY2;
	  data_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
	  
	  PE = PE1 + PE3;
	  PZ = PZ1 + PZ3;
	  PX = PX1 + PX3;
	  PY = PY1 + PY3;
	  data_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
	  
	  PE = PE3 + PE2;
	  PZ = PZ3 + PZ2;
	  PX = PX3 + PX2;
	  PY = PY3 + PY2;
	  data_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
  }
  
  
  TChain * DecayTreeMC=new TChain("DecayTree");
  DecayTreeMC->Add("MC/AllMC_Reduced.root");
  const Int_t NEnMC=DecayTreeMC->GetEntries();
  
  DecayTreeMC->SetBranchAddress("B_m_scaled",&BMass);
  
  DecayTreeMC->SetBranchAddress("Phi1_CosTheta",&Cos1);
  DecayTreeMC->SetBranchAddress("Phi2_CosTheta",&Cos2);
  DecayTreeMC->SetBranchAddress("Phi3_CosTheta",&Cos3);
  
  
  DecayTreeMC->SetBranchAddress("Phi1_PX",&PX1);
  DecayTreeMC->SetBranchAddress("Phi1_PY",&PY1);
  DecayTreeMC->SetBranchAddress("Phi1_PZ",&PZ1);
  DecayTreeMC->SetBranchAddress("Phi1_MM",&M1);
  DecayTreeMC->SetBranchAddress("Phi1_PE",&PE1);

  DecayTreeMC->SetBranchAddress("Phi2_PX",&PX2);
  DecayTreeMC->SetBranchAddress("Phi2_PY",&PY2);
  DecayTreeMC->SetBranchAddress("Phi2_PZ",&PZ2);
  DecayTreeMC->SetBranchAddress("Phi2_MM",&M2);
  DecayTreeMC->SetBranchAddress("Phi2_PE",&PE2);
  
  DecayTreeMC->SetBranchAddress("Phi3_PX",&PX3);
  DecayTreeMC->SetBranchAddress("Phi3_PY",&PY3);
  DecayTreeMC->SetBranchAddress("Phi3_PZ",&PZ3);
  DecayTreeMC->SetBranchAddress("Phi3_MM",&M3);
  DecayTreeMC->SetBranchAddress("Phi3_PE",&PE3);
  
  
  
  
  TH1D * mcPHSP_hist = new TH1D("mcPHSP_hist","mcPHSP_hist" , nch, range0, range1);
  TH1D * mc1_hist = new TH1D("","" , nch, range0, range1);
  TH1D * mc2_hist = new TH1D("","" , nch, range0, range1);
  
  TH1D * mcPHSP_M2hist = new TH1D("mcPHSP_M2hist","mcPHSP_M2hist" , M2nch, M2range0, M2range1);
  
  TH1D * mcPHSP_BMasshist = new TH1D("mcPHSP_BMasshist","mcPHSP_BMasshist" , Mnch, Mrange0, Mrange1);
  TH1D * mc1_BMasshist = new TH1D("mc1_BMasshist","mc1_BMasshist" , Mnch, Mrange0, Mrange1);
  TH1D * mc2_BMasshist = new TH1D("mc2_BMasshist","mc2_BMasshist" , Mnch, Mrange0, Mrange1);
  
  for (Int_t i=0; i<NEnMC; i++) 
  {
	  DecayTreeMC->GetEntry(i);
	  mcPHSP_hist->Fill(Cos1);
	  mcPHSP_hist->Fill(Cos2);
	  mcPHSP_hist->Fill(Cos3);
	  
	  mc1_hist->Fill(Cos1,3*Cos1*Cos1/2);
	  mc1_hist->Fill(Cos2,3*Cos2*Cos2/2);
	  mc1_hist->Fill(Cos3,3*Cos3*Cos3/2);
	  
	  mc2_hist->Fill(Cos1,3*(1-Cos1*Cos1)/4);
	  mc2_hist->Fill(Cos2,3*(1-Cos2*Cos2)/4);
	  mc2_hist->Fill(Cos3,3*(1-Cos3*Cos3)/4);

	  
	  mc1_BMasshist->Fill(BMass,27.*Cos1*Cos1*Cos2*Cos2*Cos3*Cos3/8.);
	  mc2_BMasshist->Fill(BMass,27.*(1.-Cos1*Cos1)*(1.-Cos2*Cos2)*(1.-Cos3*Cos3)/64.);
	  mcPHSP_BMasshist->Fill(BMass);
	  
	  PE = PE1 + PE2;
	  PZ = PZ1 + PZ2;
	  PX = PX1 + PX2;
	  PY = PY1 + PY2;
	  mcPHSP_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
	  
	  PE = PE1 + PE3;
	  PZ = PZ1 + PZ3;
	  PX = PX1 + PX3;
	  PY = PY1 + PY3;
	  mcPHSP_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
	  
	  PE = PE3 + PE2;
	  PZ = PZ3 + PZ2;
	  PX = PX3 + PX2;
	  PY = PY3 + PY2;
	  mcPHSP_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));	  	  
  }
  
  
  
  TCanvas *canv1 = new TCanvas("canv1","B_CosTheta",5,85,800,600);
  
  data_hist->SetMarkerStyle(8);
  
  data_hist->SetLineColor(kBlack);
  data_hist->SetLineWidth(2);
//  data_hist->SetMarkerColor(kBlue);
  data_hist->SetMarkerSize(0.8);
  data_hist->SetXTitle("B_CosTheta");
  data_hist->DrawCopy("E1");
  
  cout<<"mcPHSP_hist Integral = "<<mcPHSP_hist->Integral()/3<<endl;
  mcPHSP_hist->Scale(Double_t(NEn)/NEnMC);
  mcPHSP_hist->SetLineColor(kRed);
  mcPHSP_hist->SetLineWidth(2);
  mcPHSP_hist->DrawCopy("same");
  cout<<"mcPHSP_hist Integral = "<<mcPHSP_hist->Integral()<<endl;
  
  cout<<"mc1_hist Integral = "<<2*mc1_hist->Integral()/3<<endl;
  mc1_hist->Scale(3*Double_t(NEn)/mc1_hist->Integral());
  mc1_hist->SetLineColor(kBlue);
  mc1_hist->SetLineWidth(2);
  mc1_hist->DrawCopy("same");
  cout<<"mc1_hist Integral = "<<2*mc1_hist->Integral()<<endl;
  
  cout<<"mc2_hist Integral = "<<2*mc2_hist->Integral()/3<<endl;
  mc2_hist->Scale(3*Double_t(NEn)/mc2_hist->Integral());
  mc2_hist->SetLineColor(kGreen+2);
  mc2_hist->SetLineWidth(2);
  mc2_hist->DrawCopy("same");
  cout<<"mc2_hist Integral = "<<2*mc2_hist->Integral()<<endl;
  
  
  
  TCanvas *canv2 = new TCanvas("canv2","B_Mphiphi",5,85,800,600);
  data_M2hist->SetMarkerStyle(8);
  
  data_M2hist->SetLineColor(kBlack);
  data_M2hist->SetLineWidth(2);
//  data_M2hist->SetMarkerColor(kBlue);
  data_M2hist->SetMarkerSize(0.8);
  data_M2hist->SetXTitle("B_Mphiphi");
  data_M2hist->DrawCopy("E1");
  
  
  mcPHSP_M2hist->Scale(Double_t(NEn)/NEnMC);
  mcPHSP_M2hist->SetLineColor(kRed);
  mcPHSP_M2hist->SetLineWidth(2);
  mcPHSP_M2hist->DrawCopy("same");
  
//  TCanvas *canv3 = new TCanvas("canv3","B_m_scaled",5,85,800,600);
//  mc2_BMasshist->Draw();
//  mc1_BMasshist->SetLineColor(kBlack);
//  mc1_BMasshist->Draw("same");
//  cout<<"mc1_BMasshist = "<<8*mc1_BMasshist->Integral()<<endl;
//  cout<<"mc2_BMasshist = "<<8*mc2_BMasshist->Integral()<<endl;
//  cout<<"mcPHSP_hist = "<<mcPHSP_BMasshist->Integral()<<endl;
//  
//  
   
if(Type==0)
{
  Int_t NSteps = 200;
  Double_t stepSize = 1.0/NSteps;
  
  Double_t A1, A2, A3, chi2, minChi2, BestA1, BestA2, BestA3;

  TH2D *Chi2hist = new TH2D("#chi^{2}","#chi^{2}",NSteps,0.0,1.0,NSteps,0.0,1.0);
  
  minChi2=100;
  for(int i=0;i<NSteps;i++)
    for(int j=0;j<(NSteps-i);j++)
    {
      A1=i*stepSize;
      A2=j*stepSize;
      A3=1-A1-A2;
      TH1D* sumHist = new TH1D("","" , nch, range0, range1);
      if (sumHist->Add(mc1_hist, mc2_hist,A1,A2)==kFALSE)return;
      if (sumHist->Add(mcPHSP_hist,A3)==kFALSE)return;  
      chi2=0;
      chi2 = data_hist->Chi2Test(sumHist,"CHI2/NDF");
      if(chi2<minChi2)
      {
	BestA1=A1;
	BestA2=A2;
	BestA3=A3;
	minChi2=chi2;
      }
      sumHist->Clear();
    }
    
  for(int i=0;i<NSteps;i++)
    for(int j=0;j<(NSteps-i);j++)
    {
      A1=i*stepSize;
      A2=j*stepSize;
      A3=1-A1-A2;
      TH1D* sumHist = new TH1D("","" , nch, range0, range1);
      if (sumHist->Add(mc1_hist, mc2_hist,A1,A2)==kFALSE)return;
      if (sumHist->Add(mcPHSP_hist,A3)==kFALSE)return;  
      chi2=0;
      chi2 = data_hist->Chi2Test(sumHist,"CHI2/NDF");
      if((chi2-minChi2)<1)Chi2hist->SetBinContent(i,j,chi2);

      sumHist->Clear();
      
    }
    
   
  cout<<"BestA1 = "<<BestA1<<endl;
  cout<<"BestA2 = "<<BestA2<<endl;
  cout<<"BestA3 = "<<BestA3<<endl;
  
  TCanvas *canvChi2 = new TCanvas("canvChi2","canvChi2",5,85,800,600);
  Chi2hist->Draw("COLZ");
  cout<<stepSize<<endl;
    
    
    cout<<"entries =    "<<data_hist->GetEntries()<<endl;
  
}
}
