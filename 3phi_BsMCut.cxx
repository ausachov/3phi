// $Id: $
{
  TChain *chain = new TChain("DecayTree");
  chain->Add("DTF_Data/AllStat3phi.root");
  
  
//  TCut BsCut("B_m_scaled>5325 && B_m_scaled<5415");
//  TFile *newfile = new TFile("3phi_BsMCut.root","recreate");
    
  TCut BsCut("B_m_scaled>5250 && B_m_scaled<5500");
  TFile *newfile = new TFile("3phi_BsMWideCut.root","recreate");
    
  TTree *newtree = chain->CopyTree(BsCut);
 
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
  
}

