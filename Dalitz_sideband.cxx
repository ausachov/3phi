#include <TMath.h>




void Dalitz()
{
  gROOT->Reset();
  
  Double_t range0=4e6;
  Double_t range1=2e7;
  
  Double_t PhiMass = 1019.455;
  Double_t BsMass = 5366.77;
  
  Int_t nch=150;
    gStyle->SetOptStat(111);
  
  TChain * DecayTree=new TChain("DecayTree");
  DecayTree->Add("3phi_BsSidebandCut.root");
  const Int_t NEn=DecayTree->GetEntries();
  Double_t PX1, PY1, PZ1, M1, PE1;
  Double_t PX2, PY2, PZ2, M2, PE2;
  Double_t PX3, PY3, PZ3, M3, PE3;
  Double_t PX12, PY12, PZ12, PE12, M12;
  Double_t PX13, PY13, PZ13, PE13, M13;
  Double_t PX23, PY23, PZ23, PE23, M23;
  
  DecayTree->SetBranchAddress("Phi1_PX",&PX1);
  DecayTree->SetBranchAddress("Phi1_PY",&PY1);
  DecayTree->SetBranchAddress("Phi1_PZ",&PZ1);
  DecayTree->SetBranchAddress("Phi1_MM",&M1);
  DecayTree->SetBranchAddress("Phi1_PE",&PE1);

  DecayTree->SetBranchAddress("Phi2_PX",&PX2);
  DecayTree->SetBranchAddress("Phi2_PY",&PY2);
  DecayTree->SetBranchAddress("Phi2_PZ",&PZ2);
  DecayTree->SetBranchAddress("Phi2_MM",&M2);
  DecayTree->SetBranchAddress("Phi2_PE",&PE2);
  
  DecayTree->SetBranchAddress("Phi3_PX",&PX3);
  DecayTree->SetBranchAddress("Phi3_PY",&PY3);
  DecayTree->SetBranchAddress("Phi3_PZ",&PZ3);
  DecayTree->SetBranchAddress("Phi3_MM",&M3);
  DecayTree->SetBranchAddress("Phi3_PE",&PE3);
  
    
  ULong64_t evNumber;  
  DecayTree->SetBranchAddress("eventNumber",&evNumber);
  TCanvas *canv1 = new TCanvas("canv1","Dalitz",5,85,600,600);
  TH2D * Dalitz_hist = new TH2D("Dalitz 3#phi","Dalitz plot" , nch, range0, range1,nch, range0, range1);
  
  
  Double_t DalX [20000];
  Double_t DalY[20000];
  
  
  Double_t Tav = BsMass/3 - PhiMass;

  
  for (Int_t i=0; i<NEn; i++) 
  {
	  DecayTree->GetEntry(i);
	  PE12 = PE1 + PE2;
	  PZ12 = PZ1 + PZ2;
	  PX12 = PX1 + PX2;
	  PY12 = PY1 + PY2;
	  M12 = (PE12*PE12-PX12*PX12-PZ12*PZ12-PY12*PY12);
	  
	  PE13 = PE1 + PE3;
	  PZ13 = PZ1 + PZ3;
	  PX13 = PX1 + PX3;
	  PY13 = PY1 + PY3;
	  M13 = (PE13*PE13-PX13*PX13-PZ13*PZ13-PY13*PY13); 
	  
	  PE23 = PE2 + PE3;
	  PZ23 = PZ2 + PZ3;
	  PX23 = PX2 + PX3;
	  PY23 = PY2 + PY3;
	  M23 = (PE23*PE23-PX23*PX23-PZ23*PZ23-PY23*PY23); 
	  
	  Dalitz_hist->Fill(M12,M13);
	  Dalitz_hist->Fill(M23,M12);
	  Dalitz_hist->Fill(M13,M23);
	  Dalitz_hist->Fill(M13,M12);
	  Dalitz_hist->Fill(M12,M23);
	  Dalitz_hist->Fill(M23,M13);
	  
	  
	  Double_t Tcm1, Tcm2, Tcm3;
	  
	  Tcm1 = (-M23+BsMass*BsMass+PhiMass*PhiMass)/(2*BsMass) - PhiMass;
	  Tcm2 = (-M13+BsMass*BsMass+PhiMass*PhiMass)/(2*BsMass) - PhiMass;
	  Tcm3 = (-M12+BsMass*BsMass+PhiMass*PhiMass)/(2*BsMass) - PhiMass;
	  
	  
	  DalX[3*i] = (Tcm1-Tcm2)/TMath::Sqrt(3.)/Tav;   DalY[3*i] = (Tcm3)/Tav - 1;
 	  DalX[3*i+1] = (Tcm2-Tcm3)/TMath::Sqrt(3.)/Tav; DalY[3*i+1] = (Tcm1)/Tav - 1;
 	  DalX[3*i+2] = (Tcm3-Tcm1)/TMath::Sqrt(3.)/Tav; DalY[3*i+2] = (Tcm2)/Tav - 1;
// 	  DalX[6*i+3] = (Tcm2-Tcm1)/TMath::Sqrt(3.)/Tav; DalY[6*i+3] = (Tcm3)/Tav - 1;
//  	  DalX[6*i+4] = (Tcm3-Tcm2)/TMath::Sqrt(3.)/Tav; DalY[6*i+4] = (Tcm1)/Tav - 1;
//  	  DalX[6*i+5] = (Tcm1-Tcm3)/TMath::Sqrt(3.)/Tav; DalY[6*i+5] = (Tcm2)/Tav - 1;
  }
  
  TGraph* Dalitz_gr = new TGraph(3*NEn, DalX, DalY);
  Dalitz_gr->SetTitle("Dalitz Plot of B_{s} Sideband");
  Dalitz_gr->GetXaxis()->SetTitle("X_{D} = #sqrt{3} (T1-T2)/Q");
  Dalitz_gr->GetYaxis()->SetTitle("Y_{D} = 3 T3/Q - 1");
  Dalitz_gr->GetYaxis()->SetLabelSize(0.03);
  Dalitz_gr->GetXaxis()->SetLabelSize(0.03);
  Dalitz_gr->GetXaxis()->SetLimits(-1.2,1.2);
  Dalitz_gr->SetMinimum(-1.5);
  Dalitz_gr->SetMaximum(1.);
  Dalitz_gr->Draw("AP");
  
  
  cout<<NEn<<endl;
  
  //Dalitz_hist->Draw("COLZ");
  
}
