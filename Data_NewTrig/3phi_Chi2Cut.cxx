// $Id: $
{
  TChain *chain = new TChain("DecayTree");
  chain->Add("AllStat3phi.root");
  
  
  TCut BsCut("B_ENDVERTEX_CHI2<81");
  
  TFile *newfile = new TFile("3phi_Bs3.root","recreate");
  TTree *newtree = chain->CopyTree(BsCut);
 
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
  
}

