 void MixPhi_AllStat()
{
      
  TChain *chain = new TChain("DecayTree");
  chain->Add("AllStat3phi.root");
  
  TFile *newfile = new TFile("AllStat3phi_Mix.root","recreate");
  TTree *newtree = chain->CopyTree("");
  


  Double_t Phi1_MM_Mix;
  Double_t Phi2_MM_Mix;
  Double_t Phi3_MM_Mix;
  Double_t Phi1_MM;
  Double_t Phi2_MM;
  Double_t Phi3_MM;

   
  chain->SetBranchAddress("Phi1_MM",&Phi1_MM);
  chain->SetBranchAddress("Phi2_MM",&Phi2_MM);
  chain->SetBranchAddress("Phi3_MM",&Phi3_MM);
   
  TBranch *Phi1_MM_Mix_Branch = newtree->Branch("Phi1_MM_Mix", &Phi1_MM_Mix, "Phi1_MM_Mix/D");
  TBranch *Phi2_MM_Mix_Branch = newtree->Branch("Phi2_MM_Mix", &Phi2_MM_Mix, "Phi2_MM_Mix/D");
  TBranch *Phi3_MM_Mix_Branch = newtree->Branch("Phi3_MM_Mix", &Phi3_MM_Mix, "Phi3_MM_Mix/D");

  Long64_t NEntries = newtree->GetEntries();

  for (Long64_t i = 0; i < NEntries; i++)
  {
     chain->GetEntry(i);
     
     switch (i%6) 
     {
	case 0:
	  Phi1_MM_Mix = Phi1_MM;
	  Phi2_MM_Mix = Phi2_MM;
	  Phi3_MM_Mix = Phi3_MM;
	  break;
	case 1:
	  Phi1_MM_Mix = Phi1_MM;
	  Phi2_MM_Mix = Phi3_MM;
	  Phi3_MM_Mix = Phi2_MM;
	  break;
	case 2:
	  Phi1_MM_Mix = Phi2_MM;
	  Phi2_MM_Mix = Phi1_MM;
	  Phi3_MM_Mix = Phi3_MM;
	  break;
	case 3:
	  Phi1_MM_Mix = Phi2_MM;
	  Phi2_MM_Mix = Phi3_MM;
	  Phi3_MM_Mix = Phi1_MM;
	  break;
	case 4:
	  Phi1_MM_Mix = Phi3_MM;
	  Phi2_MM_Mix = Phi1_MM;
	  Phi3_MM_Mix = Phi2_MM;
	  break;
	case 5:
	  Phi1_MM_Mix = Phi3_MM;
	  Phi2_MM_Mix = Phi2_MM;
	  Phi3_MM_Mix = Phi1_MM;
	  break;
     }

     Phi1_MM_Mix_Branch->Fill();
     Phi2_MM_Mix_Branch->Fill();
     Phi3_MM_Mix_Branch->Fill();
  }

  
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
}