#include <TMath.h>


using namespace RooFit;

void main()
{
  gROOT->Reset();
  
  Double_t minMass=2040.;
  Double_t maxMass=4400.;
  Float_t binWidth = 40;
  Int_t binN = int((maxMass-minMass)/binWidth);
    gStyle->SetOptStat(111);
  
    
  TChain * DecayTree = new TChain("NewTree");
  DecayTree->Add("M2phi.root");
    
  RooRealVar M2phi("M2phi", "M2phi", minMass, maxMass, "MeV");
  RooDataSet* DataSet = new RooDataSet("DataSet", "DataSet", DecayTree, RooArgSet(M2phi), "");
  
  
 
 

  RooArgList listComp, listNorm;
  RooArgSet showParams;
  
  
  	RooRealVar var2PhiMass("var2PhiMass", "var2PhiMass", 2038.9);
	RooRealVar varPhif0Mass("varPhif0Mass", "varPhif0Mass", 2000);
	RooRealVar varQ("varQ","varQ", 5366.77-2038.9/2);
	
	RooPlot* frame = M2phi.frame(Title("M(#phi#phi)"));
		
	
	RooRealVar varf2_1950Number("N(f_{2}(1950))", "varf2(1950)Number", 0, 1e3);
	RooRealVar varf2_1950MassPDG("varf2(1950)MassPDG", "varf2(1950)MassPDG", 1944.);
	RooRealVar varf2_1950GammaPDG("varf2(1950)Gamma", "varf2(1950)Gamma", 472.);
	RooGenericPdf pdff2_1950("pdff2(1950)", "pdff2(1950)", "sqrt(@0-@1)*TMath::BreitWigner(@0, @2, @3)",
				RooArgSet(M2phi, var2PhiMass, varf2_1950MassPDG, varf2_1950GammaPDG));
	listComp.add(pdff2_1950);
	listNorm.add(varf2_1950Number);
	showParams.add(varf2_1950Number);
	
	
// 	RooRealVar varf2_2010Number("N(f_{2}(2010))", "varf2(2010)Number", 0, 1e3);
// 	RooRealVar varf2_2010MassPDG("varf2(2010)MassPDG", "varf2(2010)MassPDG", 2011.);
// 	RooRealVar varf2_2010GammaPDG("varf2(2010)Gamma", "varf2(2010)Gamma", 202.);
// 	RooGenericPdf pdff2_2010("pdff2(2010)", "pdff2(2010)", "sqrt(@0-@1)*TMath::BreitWigner(@0, @2, @3)",
// 				RooArgSet(M2phi, var2PhiMass, varf2_2010MassPDG, varf2_2010GammaPDG));
// 	listComp.add(pdff2_2010);
// 	listNorm.add(varf2_2010Number);
// 	showParams.add(varf2_2010Number);
	
	
/*	RooRealVar vara4Number("N(a_{4}(1996))", "vara4Number", 0, 1e3);
	RooRealVar vara4MassPDG("vara4MassPDG", "vara4MassPDG", 1996.);
	RooRealVar vara4GammaPDG("vara4GammaPDG", "vara4GammaPDG", 255.);
	RooGenericPdf pdfa4("pdfa4", "pdfa4", "sqrt(@0-@1)*TMath::BreitWigner(@0, @2, @3)",
				RooArgSet(M2phi, var2PhiMass, vara4MassPDG, vara4GammaPDG));
	listComp.add(pdfa4);
	listNorm.add(vara4Number);
	showParams.add(vara4Number)*/;
	
	
	RooRealVar varf4Number("N(f_{4}(2018))", "varf4Number", 0, 0, 1e3);
	RooRealVar varf4MassPDG("varf4MassPDG", "varf4MassPDG", 2018.);
	RooRealVar varf4GammaPDG("varf4GammaPDG", "varf4GammaPDG", 237.);
	RooGenericPdf pdff4("pdff4", "pdff4", "sqrt(@0-@1)*sqrt(@4-@0)*TMath::BreitWigner(@0, @2, @3)",
				RooArgSet(M2phi, var2PhiMass, varf4MassPDG, varf4GammaPDG, varQ));
	listComp.add(pdff4);
	listNorm.add(varf4Number);
	showParams.add(varf4Number);
	
	
	RooRealVar varphi_2170Number("N(#phi(2170))", "varphi_2170Number", 0, 0, 1e3);
	RooRealVar varphi_2170MassPDG("varphi_2170MassPDG", "varphi_2170MassPDG", 2175.);
	RooRealVar varphi_2170GammaPDG("varphi_2170GammaPDG", "varphi_2170GammaPDG", 65.);
	RooGenericPdf pdfphi_2170("pdfphi_2170", "pdfphi_2170", "sqrt(@0-@1)*sqrt(@4-@0)*TMath::BreitWigner(@0, @2, @3)",
				RooArgSet(M2phi, var2PhiMass, varphi_2170MassPDG, varphi_2170GammaPDG, varQ));
	listComp.add(pdfphi_2170);
	listNorm.add(varphi_2170Number);
	showParams.add(varphi_2170Number);
	
	
	RooRealVar varf2_2300Number("N(f_{2}(2300))", "varf2_2300Number", 0, 0, 1e2);
	RooRealVar varf2_2300MassPDG("varf2_2300MassPDG", "varf2_2300MassPDG", 2297.);
	RooRealVar varf2_2300GammaPDG("varf2_2300GammaPDG", "varf2_2300GammaPDG", 149.);
	RooGenericPdf pdff2_2300("pdff2_2300", "pdff2_2300", "sqrt(@0-@1)*sqrt(@4-@0)*TMath::BreitWigner(@0, @2, @3)",
				RooArgSet(M2phi, var2PhiMass, varf2_2300MassPDG, varf2_2300GammaPDG, varQ));
	listComp.add(pdff2_2300);
	listNorm.add(varf2_2300Number);
	showParams.add(varf2_2300Number);
	
	
	RooRealVar varf2_2340Number("N(f_{2}(2340))", "varf2_2340Number", 0, 0, 1e3);
	RooRealVar varf2_2340MassPDG("varf2_2340MassPDG", "varf2_2340MassPDG", 2339.);
	RooRealVar varf2_2340GammaPDG("varf2_2340GammaPDG", "varf2_2340GammaPDG", 319.);
	RooGenericPdf pdff2_2340("pdff2_2340", "pdff2_2340", "sqrt(@0-@1)*sqrt(@4-@0)*TMath::BreitWigner(@0, @2, @3)",
				RooArgSet(M2phi, var2PhiMass, varf2_2340MassPDG, varf2_2340GammaPDG, varQ));
	listComp.add(pdff2_2340);
	listNorm.add(varf2_2340Number);
	showParams.add(varf2_2340Number);
	
	
	RooRealVar varEtacNumber("N(#eta_{c})", "varEtacNumber", 0, 0, 2e1);
	RooRealVar varEtacMassPDG("varEtacMassPDG", "varEtacMassPDG", 2983.);
	RooRealVar varEtacGammaPDG("varEtacGammaPDG", "varEtacGammaPDG", 32.);
	RooGenericPdf pdfEtac("pdfEtac", "pdfEtac", "sqrt(@0-@1)*sqrt(@4-@0)*TMath::BreitWigner(@0, @2, @3)",
				RooArgSet(M2phi, var2PhiMass, varEtacMassPDG, varEtacGammaPDG, varQ));
	listComp.add(pdfEtac);
	listNorm.add(varEtacNumber);
	showParams.add(varEtacNumber);



	RooRealVar varBgrNumber("varBgrNumber", "varBgrNumber", 0, 0, 500.);
	RooRealVar varA0("varA0", "varA0", -1e-2, 1e-2);
	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*sqrt(@2-@0)",
			RooArgSet(M2phi, var2PhiMass, varQ));
	listComp.add(pdfBgr);
	listNorm.add(varBgrNumber);
	//showParams.add(varBgrNumber);

	RooAddPdf pdfModel("pdfModel", "pdfModel", listComp, listNorm);
        
	//showParams.add(varA0);


	
	results = pdfModel.fitTo(*DataSet,Save(true), Minos(true), Strategy(2),PrintLevel(0), NumCPU(4));

	
	cout<<results->minNll();
	
	DataSet->plotOn(frame, Binning(binN, minMass, maxMass));
	pdfModel.plotOn(frame);

	pdfModel.paramOn(frame, Layout(0.65, 0.95, 0.95), Parameters(showParams), ShowConstants(true), Format("NLAEU",AutoPrecision(1)));
	frame->getAttText()->SetTextSize(0.03) ;
	
	frame->Draw();
}
