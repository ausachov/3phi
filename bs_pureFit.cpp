/*
Only fow low statistics*/

#include <stdio.h>

double bgr(double *x, double *par)  {
        double xx = x[0]-5250;
// 	return par[0]*10*(TMath::Exp(-par[1]*xx));
	return par[0]*10*(1-par[1]*xx)	;
}



double FitFunc(double *x, double *par) 
{
        double val = 0;
	val+=10*par[0]*TMath::Gaus(x[0],par[1],par[2], kTRUE);	
	val += bgr(x,&par[3]);
	return val;
}


void bs(){
  
	Float_t xx[1000], yy[1000], ey1[1000], ey2[1000], xx[1000], yy[1000], ex1[1000], ex2[1000];
	Float_t BsMass = 5366.77; // ±0.24 meV
  	Float_t minMassBReg = 5250;
	Float_t maxMassBReg = 5500;
	Float_t binWidthB = 10;
	Int_t binNBReg = int((maxMassBReg-minMassBReg)/binWidthB);
	
	
  	FILE *fp = fopen("triPhiPureOut.txt", "r");
	for(int i=0;i<binNBReg;i++)
	{
	    fscanf(fp,"%f",&xx[i]);
	    fscanf(fp,"%f",&yy[i]);
	    fscanf(fp,"%f",&ex1[i]);
	    fscanf(fp,"%f",&ex2[i]);
	    fscanf(fp,"%f",&ey1[i]);
	    fscanf(fp,"%f",&ey2[i]);
	}
	fclose(fp);

	TGraphAsymmErrors* triPhiGr = new TGraphAsymmErrors(binNBReg, xx, yy, ex1, ex2, ey1, ey2);
	
	TCanvas* canvA = new TCanvas("canvA", "canvA", 800, 700);
	triPhiGr->Draw("AP");
	
	ROOT::Math::MinimizerOptions::SetDefaultStrategy(2);
	ROOT::Math::MinimizerOptions::SetDefaultMaxFunctionCalls(10000);
	
	TF1 *FitFunction = new TF1("FitFunction",FitFunc,minMassBReg,maxMassBReg,5);
	 
	FitFunction->SetParNames("Amp","Centre","Sigma","BgAmp","Exp");
	FitFunction->SetParameters(35,BsMass,12,0,0);
	 

	FitFunction->FixParameter(3, FitFunction->GetParameter(3));
	FitFunction->FixParameter(4, FitFunction->GetParameter(4));
	triPhiGr->Fit("FitFunction","NMQ","",minMassBReg,maxMassBReg);
	 
	FitFunction->ReleaseParameter(3);
//	FitFunction->ReleaseParameter(4);
	
	
// 	FitFunction->FixParameter(0, 0);
// 	FitFunction->FixParameter(1, BsMass);
// 	FitFunction->FixParameter(2, 12);
	
	triPhiGr->Fit("FitFunction","NMQ","",minMassBReg,maxMassBReg);
	triPhiGr->Fit("FitFunction","NMQ","",minMassBReg,maxMassBReg);
	TFitResultPtr res = triPhiGr->Fit("FitFunction","SEM","",minMassBReg,maxMassBReg);
	 
	triPhiGr->GetXaxis()->SetLimits(5250,5500);
	
	 
	FitFunction->SetLineColor(1);
	FitFunction->Draw("same");
	 
	FILE *fout = fopen("Bs_triPhiParams.txt", "w");
	for(int i=0; i<5; i++)
	{
	    double low = res->LowerError(i);
	    double high = res->UpperError(i);
	    fprintf(fout,"%s = %e  %e + %e\n",FitFunction->GetParName(i),FitFunction->GetParameter(i),low, high);
	}
	fclose(fout);
	 	 
	 
	
	 
	cout<<"chi2 = "<<res->Chi2()/FitFunction->GetNDF()<<endl;

}



