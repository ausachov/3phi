#include <TMath.h>


// 0 - CosTheta hist addition

void CosThetaM2phi_Comp(int Type=1)
{
  gROOT->Reset();
  gStyle->SetOptStat(001);
    
  gROOT->ProcessLine(".x lhcbStyle.C");

  
  Double_t range0=-1;
  Double_t range1=1;
  Int_t nch=20;
  Double_t binWidth = (range1-range0)/nch;
  
  
  Double_t M2range0=2000;
  Double_t M2range1=4500;
  Double_t binWidth = 20;
  Int_t M2nch=int((M2range1-M2range0)/binWidth);
  

  Double_t PX, PY, PZ, M, PE;
  Double_t PX11, PY11, PZ11, M11, PE11;
  Double_t PX12, PY12, PZ12, M12, PE12;
  Double_t PX21, PY21, PZ21, M21, PE21;
  Double_t PX22, PY22, PZ22, M22, PE22;
  Double_t PX31, PY31, PZ31, M31, PE31;
  Double_t PX32, PY32, PZ32, M32, PE32;
    
    
    Double_t PX1, PY1, PZ1, M1, PE1;
    Double_t PX2, PY2, PZ2, M2, PE2;
    Double_t PX3, PY3, PZ3, M3, PE3;
    

    
    TH1D * data_hist = new TH1D("#phi cos(#theta)","#phi cos(#theta)" , nch, range0, range1);
    TH1D * data_M2hist = new TH1D("data_M2hist","data_M2hist" , M2nch, M2range0, M2range1);
  
//  TChain * DecayTree=new TChain("MCDecayTreeTuple/MCDecayTree");
 
//    DecayTree->Add("../MC/GenSpectr/3phi/DVntuple.root");
//  const Int_t NEn=DecayTree->GetEntries();
//
//  
//  DecayTree->SetBranchAddress("Kplus_TRUEP_X",&PX11);
//  DecayTree->SetBranchAddress("Kplus_TRUEP_Y",&PY11);
//  DecayTree->SetBranchAddress("Kplus_TRUEP_Z",&PZ11);
//  DecayTree->SetBranchAddress("Kplus_TRUEP_E",&PE11);
//
//  DecayTree->SetBranchAddress("Kminus_TRUEP_X",&PX12);
//  DecayTree->SetBranchAddress("Kminus_TRUEP_Y",&PY12);
//  DecayTree->SetBranchAddress("Kminus_TRUEP_Z",&PZ12);
//  DecayTree->SetBranchAddress("Kminus_TRUEP_E",&PE12);
//
//
//    DecayTree->SetBranchAddress("Kplus0_TRUEP_X",&PX21);
//    DecayTree->SetBranchAddress("Kplus0_TRUEP_Y",&PY21);
//    DecayTree->SetBranchAddress("Kplus0_TRUEP_Z",&PZ21);
//    DecayTree->SetBranchAddress("Kplus0_TRUEP_E",&PE21);
//    
//    DecayTree->SetBranchAddress("Kminus0_TRUEP_X",&PX22);
//    DecayTree->SetBranchAddress("Kminus0_TRUEP_Y",&PY22);
//    DecayTree->SetBranchAddress("Kminus0_TRUEP_Z",&PZ22);
//    DecayTree->SetBranchAddress("Kminus0_TRUEP_E",&PE22);
//    
//    
//    DecayTree->SetBranchAddress("Kplus1_TRUEP_X",&PX31);
//    DecayTree->SetBranchAddress("Kplus1_TRUEP_Y",&PY31);
//    DecayTree->SetBranchAddress("Kplus1_TRUEP_Z",&PZ31);
//    DecayTree->SetBranchAddress("Kplus1_TRUEP_E",&PE31);
//    
//    DecayTree->SetBranchAddress("Kminus1_TRUEP_X",&PX32);
//    DecayTree->SetBranchAddress("Kminus1_TRUEP_Y",&PY32);
//    DecayTree->SetBranchAddress("Kminus1_TRUEP_Z",&PZ32);
//    DecayTree->SetBranchAddress("Kminus1_TRUEP_E",&PE32);
    
//  for (Int_t i=0; i<NEn; i++) 
//  {
//	  DecayTree->GetEntry(i);
//
//	  
//      PE = PE11+PE12+PE21+PE22;
//	  PZ = PZ11+PZ12+PZ21+PZ22;
//	  PX = PX11+PX12+PX21+PX22;
//	  PY = PY11+PY12+PY21+PY22;
//	  data_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
//	  
//      PE = PE11+PE12+PE31+PE32;
//      PZ = PZ11+PZ12+PZ31+PZ32;
//      PX = PX11+PX12+PX31+PX32;
//      PY = PY11+PY12+PY31+PY32;
//	  data_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
//	  
//      PE = PE21+PE22+PE31+PE32;
//      PZ = PZ21+PZ22+PZ31+PZ32;
//      PX = PX21+PX22+PX31+PX32;
//      PY = PY21+PY22+PY31+PY32;
//	  data_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
//  }
  
    
    TChain * DecayTreeMC=new TChain("DecayTree");
    DecayTreeMC->Add("../MC/CAL_0/3phi/AllMC_Reduced.root");
    const Int_t NEn=DecayTreeMC->GetEntries();
    
    
    DecayTreeMC->SetBranchAddress("Phi1_PX",&PX1);
    DecayTreeMC->SetBranchAddress("Phi1_PY",&PY1);
    DecayTreeMC->SetBranchAddress("Phi1_PZ",&PZ1);
    DecayTreeMC->SetBranchAddress("Phi1_MM",&M1);
    DecayTreeMC->SetBranchAddress("Phi1_PE",&PE1);
    
    DecayTreeMC->SetBranchAddress("Phi2_PX",&PX2);
    DecayTreeMC->SetBranchAddress("Phi2_PY",&PY2);
    DecayTreeMC->SetBranchAddress("Phi2_PZ",&PZ2);
    DecayTreeMC->SetBranchAddress("Phi2_MM",&M2);
    DecayTreeMC->SetBranchAddress("Phi2_PE",&PE2);
    
    DecayTreeMC->SetBranchAddress("Phi3_PX",&PX3);
    DecayTreeMC->SetBranchAddress("Phi3_PY",&PY3);
    DecayTreeMC->SetBranchAddress("Phi3_PZ",&PZ3);
    DecayTreeMC->SetBranchAddress("Phi3_MM",&M3);
    DecayTreeMC->SetBranchAddress("Phi3_PE",&PE3);
    
    
    
    
    
    for (Int_t i=0; i<NEn; i++)
    {
        DecayTreeMC->GetEntry(i);

        PE = PE1 + PE2;
        PZ = PZ1 + PZ2;
        PX = PX1 + PX2;
        PY = PY1 + PY2;
        data_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
        
        PE = PE1 + PE3;
        PZ = PZ1 + PZ3;
        PX = PX1 + PX3;
        PY = PY1 + PY3;
        data_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
        
        PE = PE3 + PE2;
        PZ = PZ3 + PZ2;
        PX = PX3 + PX2;
        PY = PY3 + PY2;
        data_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
    }
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    cout<<"Int Full = "<<data_M2hist->Integral()<<endl;
    
    int bin1 = data_M2hist->FindBin(2820.);
    int bin2 = data_M2hist->FindBin(3170.);
    cout<<"Int Cut = "<<data_M2hist->Integral(bin1, bin2)<<"  "<<bin1<<"  "<<bin2<<endl;
    cout<<"fraction = "<<data_M2hist->Integral(bin1, bin2)/data_M2hist->Integral()<<endl;

  

  
  TCanvas *canv2 = new TCanvas("canv2","B_Mphiphi",5,85,800,600);
  data_M2hist->SetMarkerStyle(8);
  
  data_M2hist->SetLineColor(kBlack);
  data_M2hist->SetLineWidth(2);
//  data_M2hist->SetMarkerColor(kBlue);
  data_M2hist->SetMarkerSize(0.8);
  data_M2hist->SetXTitle("B_Mphiphi");
  data_M2hist->DrawCopy("E1");
  
}
