
using namespace RooFit;

void triPhiPure(){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");

	Float_t minMassB = 3000;
	Float_t maxMassB = 11000;
	Float_t binWidthB = 10.;
	Int_t binNB = int((maxMassB-minMassB)/binWidthB);
	
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	
	Float_t minMassBReg = 5250;
	Float_t maxMassBReg = 5500;
	Int_t binNBReg = int((maxMassBReg-minMassBReg)/binWidthB);
	
	Float_t BsMass = 5366.77; // ±0.24 meV
	
	RooRealVar B_MM("B_MM", "B_MM", minMassB, maxMassB, "MeV");
	RooRealVar Phi1_MM_Mix("Phi1_MM_Mix", "Phi1_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_MM_Mix("Phi2_MM_Mix", "Phi2_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi3_MM_Mix("Phi3_MM_Mix", "Phi3_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	
	TH1F* histBTriPhi = new TH1F("histBTriPhi", "histBTriPhi", binNBReg, minMassBReg, maxMassBReg);

	TChain* chain = new TChain("DecayTree");
	chain->Add("Data_wtrig/3phi_Bs3.root");

	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(B_MM, Phi1_MM_Mix, Phi2_MM_Mix, Phi3_MM_Mix), "");
	RooPlot* frame0 = B_MM.frame(Title("B_MM"));

	
	frame0->SetAxisRange(minMassBReg,maxMassBReg);
	Int_t binNBReg = int((maxMassBReg-minMassBReg)/binWidthB);
	dsetFull->plotOn(frame0, Binning(binNBReg, minMassBReg, maxMassBReg));
	TCanvas* canv0 = new TCanvas("canv0", "canv0", 800, 600);
	frame0->Draw();

	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);
	RooRealVar varSigma("varSigma", "varSigma", 1.184);
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	

	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi1_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	RooRealVar varA1("varA1", "varA1", 0/*,10*/);
	RooGenericPdf pdfRoot1("pdfRoot1", "pdfRoot1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM_Mix, var2KMass,varA1));	
//	RooGenericPdf pdfRoot1("pdfRoot1", "pdfRoot1", "sqrt(@0-@1)", RooArgList(Phi1_MM_Mix, var2KMass));	
	RooRealVar varK1("varK1", "varK1", 0.001, 1);
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1", RooArgList(pdfPhi1, pdfRoot1), varK1);
	
	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi2_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
 	RooRealVar varA2("varA2", "varA2", 0/*,10*/);
 	RooGenericPdf pdfRoot2("pdfRoot2", "pdfRoot2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM_Mix, var2KMass,varA2));
//	RooGenericPdf pdfRoot2("pdfRoot2", "pdfRoot2", "sqrt(@0-@1)", RooArgList(Phi2_MM_Mix, var2KMass));
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2", RooArgList(pdfPhi2, pdfRoot2), varK1);
	
	RooGenericPdf pdfPhi3("pdfPhi3", "pdfPhi3", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi3_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	RooRealVar varA3("varA3", "varA3", 0/*,10*/);
	RooGenericPdf pdfRoot3("pdfRoot3", "pdfRoot3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi3_MM_Mix, var2KMass,varA3));
//	RooGenericPdf pdfRoot3("pdfRoot3", "pdfRoot3", "sqrt(@0-@1)", RooArgList(Phi3_MM_Mix, var2KMass));
	RooAddPdf pdfModel3("pdfModel3", "pdfModel3", RooArgList(pdfPhi3, pdfRoot3), varK1);
	
	
	RooProdPdf pdfModelNE("pdfModelNE", "pdfModelNE", RooArgSet(pdfModel1, pdfModel2, pdfModel3));
	//RooRealVar varEvN("varEvN", "varEvN", 0, 1e5);
	RooRealVar varNTriPhi("varNTriPhi", "varNTriPhi",5,0,1000);
	RooFormulaVar varEvN("varEvN", "varEvN", "@0/@1/@1/@1", RooArgList(varNTriPhi, varK1));
	
	RooExtendPdf pdfModel("pdfModel", "pdfModel",  pdfModelNE, varEvN);
	
	char label[200];
	Float_t massBLo = 5320;
	Float_t massBHi = 5330;

	Float_t EvNVal0 = 0.4;
	Float_t stepEvN = 0.01;
	Int_t NPoints = 350;
	
	Double_t x[1000], y[1000];
	
	
	varNTriPhi.setConstant(kTRUE);
	for (Int_t i = 0; i<NPoints; i++){

	        varNTriPhi.setVal(EvNVal0 + i*stepEvN);
		
		sprintf(label, "B_MM>%i&&B_MM<%i", massBLo, massBHi);
		RooDataSet* dset = dsetFull->reduce(Cut(label), Name("dset"), Title("dset"));
		RooFitResult* res = pdfModel.fitTo(*dset, Minos(true), Strategy(2), Save(true), PrintLevel(0));
		
	        x[i] = EvNVal0 + i*stepEvN;
		y[i] = res->minNll();
		
		delete dset;
		delete res;
		
	}
	
	TGraph* NllGr = new TGraph(NPoints, x, y);
	TCanvas* canvGr = new TCanvas("canvGr", "canvGr", 900, 700);
	NllGr->SetLineColor(2);
	NllGr->SetLineWidth(2);
	NllGr->Draw();

        varNTriPhi.setConstant(kFALSE);
	varNTriPhi.setVal(1);
	
		
		sprintf(label, "B_MM>%i&&B_MM<%i", massBLo, massBHi);
		RooDataSet* dset3 = dsetFull->reduce(Cut(label), Name("dset3"), Title("dset3"));
 		RooFitResult* res = pdfModel.fitTo(*dset3, Save(true), Strategy(2), PrintLevel(0));
		res->Print("v");
		Double_t Dll = res->minNll();
		TLine* horiz = new TLine(EvNVal0, Dll+0.5, EvNVal0 + NPoints*stepEvN,Dll+0.5);
		horiz->SetLineColor(1);
	        horiz->SetLineWidth(2);
		horiz->Draw("same");
		
		
// 		TCanvas* canvTest = new TCanvas("canvTest", "canvTest", 1200, 400);
// 		canvTest->Divide(3, 1);
// 		RooPlot* frame1 = Phi1_MM_Mix.frame(Title("#phi_{1} mass"));
// 		dset3->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
// 		pdfModel1.plotOn(frame1);
// 		pdfModel1.plotOn(frame1, Components(pdfRoot1), LineStyle(kDashed));
// 
// 		RooPlot* frame2 = Phi2_MM_Mix.frame(Title("#phi_{2} mass"));
// 		dset3->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
// 		pdfModel2.plotOn(frame2);
// 		pdfModel2.plotOn(frame2, Components(pdfRoot2), LineStyle(kDashed));
// 		
// 		RooPlot* frame3 = Phi3_MM_Mix.frame(Title("#phi_{3} mass"));
// 		dset3->plotOn(frame3, Binning(binNPhi, minMassPhi, maxMassPhi));
// 		pdfModel3.plotOn(frame3);
// 		pdfModel3.plotOn(frame3, Components(pdfRoot3), LineStyle(kDashed));
// 
// 		canvTest->cd(1);
// 		frame1->DrawClone();
// 		canvTest->cd(2);
// 		frame2->DrawClone();
// 		canvTest->cd(3);
// 		frame3->DrawClone();
// 		
// 		
// 		delete dset3;
// 		delete res;
	
}


