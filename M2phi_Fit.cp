#include <TMath.h>


using namespace RooFit;
using namespace RooStats;

void M2phi_Fit()
{
  gROOT->Reset();
  gStyle->SetOptStat(001);
    
  //gROOT->ProcessLine(".x lhcbStyle.C");

    
    
    
    
    
  Double_t M2range0=2000;
  Double_t M2range1=4400;
  Int_t M2nch=60;


    

  
  Double_t PX1, PY1, PZ1, M1, PE1;
  Double_t PX2, PY2, PZ2, M2, PE2;
  Double_t PX3, PY3, PZ3, M3, PE3;
  Double_t PX, PY, PZ, PE, BMass;

  
  TChain * DecayTree=new TChain("DecayTree");
  DecayTree->Add("3phi_BsMWideCut.root");
//  DecayTree->Add("3phi_BsMCut.root");
  const Int_t NEn=DecayTree->GetEntries();

    
  DecayTree->SetBranchAddress("B_m_scaled",&BMass);
  
  DecayTree->SetBranchAddress("Phi1_PX",&PX1);
  DecayTree->SetBranchAddress("Phi1_PY",&PY1);
  DecayTree->SetBranchAddress("Phi1_PZ",&PZ1);
  DecayTree->SetBranchAddress("Phi1_MM",&M1);
  DecayTree->SetBranchAddress("Phi1_PE",&PE1);

  DecayTree->SetBranchAddress("Phi2_PX",&PX2);
  DecayTree->SetBranchAddress("Phi2_PY",&PY2);
  DecayTree->SetBranchAddress("Phi2_PZ",&PZ2);
  DecayTree->SetBranchAddress("Phi2_MM",&M2);
  DecayTree->SetBranchAddress("Phi2_PE",&PE2);
  
  DecayTree->SetBranchAddress("Phi3_PX",&PX3);
  DecayTree->SetBranchAddress("Phi3_PY",&PY3);
  DecayTree->SetBranchAddress("Phi3_PZ",&PZ3);
  DecayTree->SetBranchAddress("Phi3_MM",&M3);
  DecayTree->SetBranchAddress("Phi3_PE",&PE3);


    
    
    
    
  RooRealVar PhiPhiMass("PhiPhiMass","PhiPhiMass",M2range0,M2range1);
  RooRealVar BsMass("BsMass","BsMass",5250,5500);
  RooDataSet DataSet("DataSet","DataSet",RooArgSet(PhiPhiMass,BsMass));
    
    
  TH1D * data_BMhist = new TH1D("One of M(phiphi)<2400","One of M(phiphi)<2400" , 25, 5250, 5500);
  TH1D * data_BMHighhist = new TH1D("All M(phiphi)>2400","All M(phiphi)>2400" , 25, 5250, 5500);
  TH1D * data_M2hist = new TH1D("data_M2hist","data_M2hist" , M2nch, M2range0, M2range1);

  Double_t phiphimasVal1 = 0,phiphimasVal2 = 0,phiphimasVal3 = 0;
  for (Int_t i=0; i<NEn; i++) 
  {
	  DecayTree->GetEntry(i);

	  
	  PE = PE1 + PE2;
	  PZ = PZ1 + PZ2;
	  PX = PX1 + PX2;
	  PY = PY1 + PY2;
	  data_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
      
      phiphimasVal1 = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);
      PhiPhiMass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);
      BsMass = BMass;
      DataSet.add(RooArgSet(PhiPhiMass,BsMass));
      

	  
	  PE = PE1 + PE3;
	  PZ = PZ1 + PZ3;
	  PX = PX1 + PX3;
	  PY = PY1 + PY3;
	  data_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
      phiphimasVal2 = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);
      PhiPhiMass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);
      DataSet.add(RooArgSet(PhiPhiMass,BsMass));


	  
	  PE = PE3 + PE2;
	  PZ = PZ3 + PZ2;
	  PX = PX3 + PX2;
	  PY = PY3 + PY2;
	  data_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
      phiphimasVal3 = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);
      PhiPhiMass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);
      DataSet.add(RooArgSet(PhiPhiMass,BsMass));
      
      
      if(phiphimasVal1<2400 || phiphimasVal2<2400 || phiphimasVal3<2400)data_BMhist->Fill(BMass);
      else data_BMHighhist->Fill(BMass);


  }
  
  
  TChain * DecayTreeMC=new TChain("DecayTree");
  DecayTreeMC->Add("MC/AllMC_Reduced.root");
  const Int_t NEnMC=DecayTreeMC->GetEntries();

  
  
  DecayTreeMC->SetBranchAddress("Phi1_PX",&PX1);
  DecayTreeMC->SetBranchAddress("Phi1_PY",&PY1);
  DecayTreeMC->SetBranchAddress("Phi1_PZ",&PZ1);
  DecayTreeMC->SetBranchAddress("Phi1_MM",&M1);
  DecayTreeMC->SetBranchAddress("Phi1_PE",&PE1);

  DecayTreeMC->SetBranchAddress("Phi2_PX",&PX2);
  DecayTreeMC->SetBranchAddress("Phi2_PY",&PY2);
  DecayTreeMC->SetBranchAddress("Phi2_PZ",&PZ2);
  DecayTreeMC->SetBranchAddress("Phi2_MM",&M2);
  DecayTreeMC->SetBranchAddress("Phi2_PE",&PE2);
  
  DecayTreeMC->SetBranchAddress("Phi3_PX",&PX3);
  DecayTreeMC->SetBranchAddress("Phi3_PY",&PY3);
  DecayTreeMC->SetBranchAddress("Phi3_PZ",&PZ3);
  DecayTreeMC->SetBranchAddress("Phi3_MM",&M3);
  DecayTreeMC->SetBranchAddress("Phi3_PE",&PE3);
  
  

  
  TH1D * mcPHSP_M2hist = new TH1D("mcPHSP_M2hist","mcPHSP_M2hist" , M2nch, M2range0, M2range1);
  for (Int_t i=0; i<NEnMC; i++) 
  {
	  DecayTreeMC->GetEntry(i);

	  
	  PE = PE1 + PE2;
	  PZ = PZ1 + PZ2;
	  PX = PX1 + PX2;
	  PY = PY1 + PY2;
	  mcPHSP_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
	  
	  PE = PE1 + PE3;
	  PZ = PZ1 + PZ3;
	  PX = PX1 + PX3;
	  PY = PY1 + PY3;
	  mcPHSP_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
	  
	  PE = PE3 + PE2;
	  PZ = PZ3 + PZ2;
	  PX = PX3 + PX2;
	  PY = PY3 + PY2;
	  mcPHSP_M2hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));	  	  
  }
  
  
    
  
  RooDataHist PHSPhist("PHSPhist","PHSPhist",RooArgSet(PhiPhiMass),mcPHSP_M2hist);
  
  
    
  RooRealVar var2PhiMass("var2PhiMass","var2PhiMass",1029.46*2);
    
    
  RooHistPdf PHSPModel("PHSPModel", "PHSPModel",RooArgSet(PhiPhiMass),PHSPhist);
  RooRealVar PHSPNumber("PHSPNumber", "PHSPNumber",0,1e4);

    
    
    
  RooRealVar varf2_2300Number("N(f_{2}(2300))", "varf2_2300Number", 1, 0, 1e6);
  RooRealVar varf2_2300Mass("M(f_{2}(2300)", "varf2_2300MassPDG", 2297.,2100,2500);
  RooRealVar varf2_2300GammaPDG("varf2_2300GammaPDG", "varf2_2300GammaPDG", 149.);
  RooGenericPdf pdff2_2300("pdff2_2300", "pdff2_2300", "sqrt(@0-@1)*TMath::BreitWigner(@0, @2, @3)",
                             RooArgSet(PhiPhiMass, var2PhiMass, varf2_2300Mass, varf2_2300GammaPDG));
    
    
    
  RooRealVar varEtacNumber("N(#eta_{c})", "varEtacNumber", 0, 0, 2e3);
  RooRealVar varEtacMassPDG("varEtacMassPDG", "varEtacMassPDG", 2983.4);
  RooRealVar varEtacGammaPDG("varEtacGammaPDG", "varEtacGammaPDG", 31.8);
  RooGenericPdf pdfEtac("pdfEtac", "pdfEtac", "sqrt(@0-@1)*TMath::BreitWigner(@0, @2, @3)",
                          RooArgSet(PhiPhiMass, var2PhiMass, varEtacMassPDG, varEtacGammaPDG));

    
    
    
  RooArgList listComp,listNorm;
    RooArgSet showParams;
  

    
  listComp.add(PHSPModel);
  listComp.add(pdff2_2300);
  listComp.add(pdfEtac);
    
  listNorm.add(PHSPNumber);
  listNorm.add(varf2_2300Number);
  listNorm.add(varEtacNumber);



  showParams.add(PHSPNumber);
  showParams.add(varf2_2300Number);
  showParams.add(varEtacNumber);
  showParams.add(varf2_2300Mass);
    
    
  RooAddPdf pdfModel("pdfModel", "pdfModel", listComp, listNorm);
    
    TCanvas* c1 = new TCanvas("c1","c1");
  pdfModel.fitTo(DataSet,Save(true),Minos(true));
    
    
  RooPlot* frame = PhiPhiMass.frame();
  DataSet.plotOn(frame,Binning(M2nch,M2range0,M2range1));
    
  pdfModel.plotOn(frame);
  pdfModel.plotOn(frame,Components(pdff2_2300),LineColor(kRed));
  pdfModel.paramOn(frame, Layout(0.65, 0.95, 0.95), Parameters(showParams), Format("NLAEU",AutoPrecision(1)));
 
    
  SPlot* sData1 = new SPlot("sData1", "sData1", DataSet, &pdfModel, RooArgList(PHSPNumber, varf2_2300Number));
    
  RooDataSet * dsetPHSP = new RooDataSet(DataSet.GetName(),DataSet.GetTitle(),&DataSet,*DataSet.get(),"","PHSPNumber_sw") ;
  RooDataSet * dsetF2 = new RooDataSet(DataSet.GetName(),DataSet.GetTitle(),&DataSet,*DataSet.get(),"","N(f_{2}(2300))_sw") ;

    
    
  frame->Draw();
    
    
  
  TCanvas* c2 = new TCanvas("c2","c2");
  c2->Divide(2,1);
  c2->cd(1);
  data_BMhist->Draw("E");
    
  c2->cd(2);
  data_BMHighhist->Draw("E");
    
    
  RooPlot* frame2 = BsMass.frame(Title("Bs->phi f2 (2300)"));
    dsetF2->plotOn(frame2,Binning(25,5250,5500));
  RooPlot* frame3 = BsMass.frame(Title("Bs->3phi non resonant (3 entries per event)"));
    dsetPHSP->plotOn(frame3,Binning(25,5250,5500));

    TCanvas* c3 = new TCanvas("c3","c3");
    c3->Divide(2,1);
    c3->cd(1);
    frame2->Draw();
    
    c3->cd(2);
    frame3->Draw();

}
