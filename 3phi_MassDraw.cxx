#include <TMath.h>


using namespace RooFit;

void main()
{
  gROOT->Reset();
  //gStyle->SetOptStat(kFALSE);
  
  
  
  TChain * DecayTree=new TChain("DecayTree");
  DecayTree->Add("Data_NewTrig/AllStat3phi.root");
  
  RooRealVar B_m_scaled("B_m_scaled", "B_m_scaled", 5250, 5500, "MeV");
   RooDataSet* dset = new RooDataSet("dset", "dset", DecayTree, RooArgSet(B_m_scaled));
  
   RooPlot* frameMass = B_m_scaled.frame();
  dset->plotOn(frameMass,Binning(25,5250,5500));
  TCanvas* canvMass = new TCanvas("canvMass", "canvMass", 800, 600);
  frameMass->Draw(); 


//
//  Double_t range0=0;
//  Double_t range1=3.5e9;
//  Int_t nch=1e6;
//  
//  TH1D * nev_hist = new TH1D("nev_hist","nev_hist" , nch, range0, range1);
//  ULong64_t evNumber;
//  TTree* newTree = DecayTree->CopyTree(TCut("B_m_scaled>3526.5 && B_m_scaled<3527"));
//  newTree->SetBranchAddress("eventNumber",&evNumber);
//  Int_t NEn = newTree->GetEntries();
//  for (Int_t i=0; i<NEn; i++) 
//  {
//	  newTree->GetEntry(i);
//	  nev_hist->Fill(evNumber);
//  }
//  TCanvas* canvEv = new TCanvas("canvEv", "canvEv", 800, 600);
//  nev_hist->Draw();  
  
  

  
}
