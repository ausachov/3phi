#include <TMath.h>







void phiphi_m_draw()
{
  gROOT->Reset();
  
  Double_t range0=2000;
  Double_t range1=4400;
  Int_t nch=120;
    gStyle->SetOptStat(111);
  
  TChain * DecayTree=new TChain("DecayTree");
  DecayTree->Add("3phi_BsMCut.root");
  const Int_t NEn=DecayTree->GetEntries();
  Double_t PX1, PY1, PZ1, M1, PE1;
  Double_t PX2, PY2, PZ2, M2, PE2;
  Double_t PX3, PY3, PZ3, M3, PE3;
  Double_t PX, PY, PZ, PE;
  
  Float_t Etac1Mass, Etac2Mass, Etac3Mass;

  

  

  
  Double_t PX, PY, PZ, PE;
  
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PX",&Etac1Mass);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PY",&Etac2Mass);
  DecayTree->SetBranchAddress("B_BsMassFit_phi_1020_Kplus_PZ",&Etac3Mass);


  

  TCanvas *canv1 = new TCanvas("canv1","MJPsi",5,85,800,600);
  TH1D * m_phiphi_hist = new TH1D("M(#phi#phi) from 3#phi","#phi#phi candidates" , nch, range0, range1);
    
 

  for (Int_t i=0; i<NEn; i++) 
  {

	  DecayTree->GetEntry(i);
	  m_phiphi_hist->Fill(Etac1Mass));
	  m_phiphi_hist->Fill(Etac2Mass);
	  m_phiphi_hist->Fill(Etac3Mass);
	  

  }
  m_phiphi_hist->Draw("E1");
  
  
  
  Double_t mass = 0;
    
  TChain * DecayTreeMC=new TChain("DecayTree");
  DecayTreeMC->Add("MC/AllMC_Reduced.root");
  const Int_t NEnMC=DecayTreeMC->GetEntries();

  
  DecayTreeMC->SetBranchAddress("Phi1_PX",&PX1);
  DecayTreeMC->SetBranchAddress("Phi1_PY",&PY1);
  DecayTreeMC->SetBranchAddress("Phi1_PZ",&PZ1);
  DecayTreeMC->SetBranchAddress("Phi1_MM",&M1);
  DecayTreeMC->SetBranchAddress("Phi1_PE",&PE1);

  DecayTreeMC->SetBranchAddress("Phi2_PX",&PX2);
  DecayTreeMC->SetBranchAddress("Phi2_PY",&PY2);
  DecayTreeMC->SetBranchAddress("Phi2_PZ",&PZ2);
  DecayTreeMC->SetBranchAddress("Phi2_MM",&M2);
  DecayTreeMC->SetBranchAddress("Phi2_PE",&PE2);
  
  DecayTreeMC->SetBranchAddress("Phi3_PX",&PX3);
  DecayTreeMC->SetBranchAddress("Phi3_PY",&PY3);
  DecayTreeMC->SetBranchAddress("Phi3_PZ",&PZ3);
  DecayTreeMC->SetBranchAddress("Phi3_MM",&M3);
  DecayTreeMC->SetBranchAddress("Phi3_PE",&PE3);

  TH1D * MCm_phiphi_hist = new TH1D("M(#phi#phi) from MC 3#phi","#phi#phi candidates" , nch, range0, range1);
  for (Int_t i=0; i<NEnMC; i++) 
  {
      bestMassDiff = 9999;
	  DecayTreeMC->GetEntry(i);
	  PE = PE1 + PE2;
	  PZ = PZ1 + PZ2;
	  PX = PX1 + PX2;
	  PY = PY1 + PY2;
	  MCm_phiphi_hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
      mass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);

      
	  PE = PE1 + PE3;
	  PZ = PZ1 + PZ3;
	  PX = PX1 + PX3;
	  PY = PY1 + PY3;
	  MCm_phiphi_hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
      mass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);

      
	  PE = PE3 + PE2;
	  PZ = PZ3 + PZ2;
	  PX = PX3 + PX2;
	  PY = PY3 + PY2;
	  MCm_phiphi_hist->Fill(TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY));
      mass = TMath::Sqrt(PE*PE-PX*PX-PZ*PZ-PY*PY);


  }
  MCm_phiphi_hist->SetLineColor(kRed);
  MCm_phiphi_hist->Scale(Double_t(NEn)/NEnMC);
  MCm_phiphi_hist->Draw("same");
}
