/*
 * diKMass.cpp
 *
 *  Created on: Apr 16, 2013
 *      Author: maksym
 */

//0- FixSigma
//1- VariableSigma
//2- Sigma MC

//3 - Exp bg
//4 - Shift bg

using namespace RooFit;

void diPhiPure(int Type=0,int RangeNumber=2){
//	gROOT->Reset();
	gROOT->SetStyle("Plain");
	gStyle->SetOptStat(000);
	TProof::Open("");

switch(RangeNumber)
{
  case 0: 
	Float_t minMassJpsi = 2700;
	Float_t maxMassJpsi = 4000;
	break;  
  case 1: 
	Float_t minMassJpsi = 5250;
	Float_t maxMassJpsi = 5500;
	break;  
  case 2: 
	Float_t minMassJpsi = 2700;
	Float_t maxMassJpsi = 6000;
	break;
}

//         if(Type==4)
// 	{
// 	  minMassJpsi-=5;
// 	  maxMassJpsi-=5;
// 	}
	Float_t binWidthJpsi = 10.;
	Int_t binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi);
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	
	
	Float_t PhiMass = 1019.46;
	
 	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMassJpsi, maxMassJpsi, "MeV");
	RooRealVar Phi1_m_scaled_Mix("Phi1_m_scaled_Mix", "Phi1_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_m_scaled_Mix("Phi2_m_scaled_Mix", "Phi2_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	TH1F* histJpsiDiPhi = new TH1F("histJpsiDiPhi", "histJpsiDiPhi", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiDiK = new TH1F("histJpsiDiK", "histJpsiDiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiPhiK = new TH1F("histJpsiPhiK", "histJpsiPhiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histSigma = new TH1F("histSigma", "histSigma", binNJpsi, minMassJpsi-4*493.677, maxMassJpsi-4*493.677);

	TChain* chain = new TChain("DecayTree");
	chain->Add("2phi_afterCut.root");

	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_m_scaled, Phi1_m_scaled_Mix, Phi2_m_scaled_Mix), "");

	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46, 1019.46-1, 1019.46+1);
//	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);
	if(Type==0 || Type==3 || Type==4)
	  RooRealVar varSigma("varSigma", "varSigma", 1.20);
	if(Type==2)
	  RooRealVar varSigma("varSigma", "varSigma", 1.15);
	if(Type==1)
	  RooRealVar varSigma("varSigma", "varSigma", 1, 0.1, 4);
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	

	

	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi1_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));	
	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi2_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	
if(Type==3)
{
	RooRealVar varA1("varA1", "varA1", 0,-1,0);
	RooRealVar varA2("varA2", "varA2", 0,-1,0);

	RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*TMath::Exp(-(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*TMath::Exp(-(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*TMath::Exp(-(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*TMath::Exp(-(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA2));  
}
else
  if(Type==4)
  {
	RooRealVar varA1("varA1", "varA1", 0);
	RooRealVar varA2("varA2", "varA2", 0);

	RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA2));	

  }
  else
  {
	  RooRealVar varA1("varA1", "varA1", 0,-0.1,1);
	  RooRealVar varA2("varA2", "varA2", 0,-0.1,1);

	  RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA1));
	  RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA2));
	  RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA1));
	  RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA2));
  }
	
	
	RooProdPdf pdfSS("pdfSS", "pdfSS", RooArgSet(pdfPhi1, pdfPhi2));
	RooProdPdf pdfSB("pdfSB", "pdfSB", RooArgSet(pdfPhi1, pdfRoot2A1));
	RooProdPdf pdfBS("pdfBS", "pdfBS", RooArgSet(pdfRoot1A1, pdfPhi2));
	RooProdPdf pdfBB("pdfBB", "pdfBB", RooArgSet(pdfRoot1A2, pdfRoot2A2));
	
	
	
	RooRealVar varNDiPhi("varNDiPhi", "varNDiPhi", 0, 1e7);	
	RooRealVar varNPhiK("varNPhiK", "varNPhiK", 0, 1e7);
	RooRealVar varNDiK("varNDiK", "varNDiK", 0, 1e7);
	
	

	
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1",  RooArgList(pdfPhi1,pdfPhi1,pdfRoot1A1,pdfRoot1A2), 
							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2",  RooArgList(pdfPhi2,pdfRoot2A1,pdfPhi2,pdfRoot2A2), 
							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));
	


		
	

	
	RooAddPdf pdfModel("pdfModel", "pdfModel",  RooArgList(pdfSS,pdfSB,pdfBS,pdfBB), RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));


	char label[200];
	Float_t massJpsiLo, massJpsiHi;

	for (Int_t i=0; i<binNJpsi; i++){
		massJpsiLo = minMassJpsi + i*binWidthJpsi;
		massJpsiHi = minMassJpsi + (i+1)*binWidthJpsi;

		if(Type!=4)
		{
		  varA1.setConstant(kFALSE);
		  varA2.setConstant(kFALSE);
		}
		varPhiMass.setConstant(kFALSE);
		varNDiK.setConstant(kFALSE);

		
		sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
		RooDataSet* dset = dsetFull->reduce(Cut(label), Name("dset"), Title("dset"));
		
		varNDiPhi.setVal(dset->numEntries()/1.5);
		varNDiK.setVal(dset->numEntries()/3);
		varNPhiK.setVal(dset->numEntries()/4);

		varNDiPhi.setMax(Double_t(dset->numEntries()));	
		varNDiK.setMax(Double_t(dset->numEntries()));	
		varNPhiK.setMax(Double_t(dset->numEntries()));	
		
		
		RooFitResult* res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		
		Double_t EDM = res->edm();
		
		if(EDM>2e-3)
		{
		  varPhiMass.setConstant(kTRUE);
		  varPhiMass.setVal(PhiMass);
		  res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		  res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		  EDM = res->edm();  
		}		
		

		
		if(EDM>2e-3)
		{
		    if(TMath::Abs(varA2.getVal(0))<(varA2.getError()/2))
		    {
		      varA2.setConstant(kTRUE);
		      varA2.setVal(0);
		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		      EDM = res->edm();
		    }
				    
	    
		    if(TMath::Abs(varA1.getVal(0))<(varA1.getError()/2))
		    {
		      varA1.setConstant(kTRUE);
		      varA1.setVal(0);
		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		      EDM = res->edm();
		    }
		    
		    if(EDM>2e-3)
		    {
		      varPhiMass.setConstant(kTRUE);
		      varPhiMass.setVal(PhiMass);
		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		      EDM = res->edm();  
		    }
		    
		    if(EDM>2e-3)
		    {
		      varNDiK.setConstant(kTRUE);
		      varNDiK.setVal(0);
		      varA2.setConstant(kTRUE);
		      varA2.setVal(0);
		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		      EDM = res->edm();  
		    }
		
// 		    if(EDM>2e-3)
// 		    {
// 		      varNPhiK.setVal(0);
// 		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
// 		      EDM = res->edm(); 
// 		    }
		     
		}
		
		

		if(Type==3)
		    if(varA1.getVal()>-0.1)
		    {
		      varA1.setConstant(kTRUE);
		      varA1.setVal(0);
		    }
		    if(varA2.getVal()>-0.1)
		    {
		      varA2.setConstant(kTRUE);
		      varA2.setVal(0);
		    }
// 		res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
// 		EDM = res->edm(); 
//AAAAAAA		
/*		if(TMath::Abs(varNDiK.getVal())<(varNDiK.getError()/2.))
		{
		  varNDiK.setConstant(kTRUE);
		  res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		  EDM = res->edm(); 
		}*/		
		
		histJpsiDiPhi->SetBinContent(i+1, varNDiPhi.getVal());
		histJpsiDiPhi->SetBinError(i+1, varNDiPhi.getError());

		histJpsiDiK->SetBinContent(i+1, varNDiK.getVal());
		histJpsiDiK->SetBinError(i+1, varNDiK.getError());

		histJpsiPhiK->SetBinContent(i+1, varNPhiK.getVal());
		histJpsiPhiK->SetBinError(i+1, varNPhiK.getError());
		
		histSigma->SetBinContent(i+1, varSigma.getVal());
		histSigma->SetBinError(i+1, varSigma.getError());
		

		if(EDM>2e-3)return;
		


		delete dset;
		delete res;
	}

	
	
	TCanvas* canvTest = new TCanvas("canvTest", "canvTest", 1200, 400);
 	canvTest->Divide(3,1);

	Double_t minMassJpsiTest = minMassJpsi;
	Double_t maxMassJpsiTest = maxMassJpsi;
		

	
		massJpsiLo = minMassJpsiTest;
		massJpsiHi = maxMassJpsiTest;
		
		if(Type!=4)
		{
		  varA1.setConstant(kFALSE);
		  varA2.setConstant(kFALSE);
		}
		varPhiMass.setConstant(kFALSE);
		varNDiK.setConstant(kFALSE);		
 
		sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
		RooDataSet* dset2 = dsetFull->reduce(Cut(label), Name("dset2"), Title("dset2"));
		
		varNDiPhi.setVal(dset2->numEntries()/1.5);
		varNDiK.setVal(dset2->numEntries()/3);
		varNPhiK.setVal(dset2->numEntries()/4);

		varNDiPhi.setMax(Double_t(dset2->numEntries()));	
		varNDiK.setMax(Double_t(dset2->numEntries()));	
		varNPhiK.setMax(Double_t(dset2->numEntries()));			
		
		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
		res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));

		TH2* hPdf = pdfModel.createHistogram("Phi2_m_scaled_Mix,Phi1_m_scaled_Mix");

		RooPlot* frame1 = Phi1_m_scaled_Mix.frame(Title("#phi_{1} mass"));
		dset2->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel1.plotOn(frame1);
//		pdfModel1.plotOn(frame1, Components(pdfRoot1), LineStyle(kDashed));

		RooPlot* frame2 = Phi2_m_scaled_Mix.frame(Title("#phi_{2} mass"));
		dset2->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel2.plotOn(frame2);
//		pdfModel2.plotOn(frame2, Components(pdfRoot2), LineStyle(kDashed));

		canvTest->cd(1);
		frame1->DrawClone();
		canvTest->cd(2);
		frame2->DrawClone();

		canvTest->cd(3);
		hPdf->DrawClone("surf");
		delete frame1;
		delete frame2;
		delete hPdf;

		delete dset2;
		delete res;

	
	
	TCanvas* canvB = new TCanvas("canvB", "canvB", 800, 700);
	histSigma->DrawClone();

	if(RangeNumber==0)
	  if(Type==2)
	    TFile file("diPhiPureAll_MC.root","recreate");
	  else
	    if(Type==3)
	      TFile file("diPhiPureAllExp.root","recreate");
	    else
	      if(Type==4)
		TFile file("diPhiPureShift.root","recreate");
	      else
	      TFile file("diPhiPureAll.root","recreate");
	  
	if(RangeNumber==1)
	  if(Type==2)
	    TFile file("bsPureAll_MC.root","recreate");
	  else
	    if(Type==3)
	      TFile file("bsPureAllExp.root","recreate");
	    else
	      TFile file("bsPureAll.root","recreate");
	    
	TCanvas* canvA = new TCanvas("canvA", "canvA", 800, 700);
	canvA->Divide(1, 3);
	canvA->cd(1);
	histJpsiDiPhi->DrawClone();
	canvA->cd(2);
	histJpsiPhiK->DrawClone();
	canvA->cd(3);
	histJpsiDiK->DrawClone();		  
	    
	    
	histJpsiDiPhi->Write();
	histJpsiDiK->Write();
	histJpsiPhiK->Write();
	histSigma->Write();
	file.Close();
}

void runALL()
{
  diPhiPure(0,0);
  diPhiPure(2,0);
  diPhiPure(3,0);
  diPhiPure(4,0);
  
  diPhiPure(0,1);
  diPhiPure(2,1);
  diPhiPure(3,1);
  diPhiPure(4,1);
}

