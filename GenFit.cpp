using namespace RooFit;
void GenFit(int seedNumber=0)
{
	
    Double_t sigmaExternal = 13.64*9.49/11.66;
    
    Double_t fitMass = 5366.13;
	Double_t fitSigma = sigmaExternal;
  
//    Double_t NBsCentre = 41;
//	Double_t NBsError = 10;
	
//	Double_t NBgCentre = 8.7;
//	Double_t NBgError = 8.0;
    
    
    Double_t NBsCentre = 0;
    Double_t NBsError = 0;
    
    Double_t NBgCentre = 11.07;
    Double_t NBgError = 7.72;
	
    
    
    
	Double_t NEvCentre = 48;
	Double_t NEvError = 12;
	
	
	Float_t minMass = 5250;
	Float_t maxMass = 5500;
	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	
	Int_t nGenEvents = 50000;
	
	RooRealVar varNBsCentre("varNBsCentre","varNBsCentre",NBsCentre);
	RooRealVar varNBsError("varNBsError","varNBsError",NBsError);
        RooRealVar NBs("NBs","NBs",0,100);
	RooGaussian pdfNBs("pdfNBs","pdfNBs",NBs,varNBsCentre,varNBsError);
	
	RooRealVar varNBgCentre("varNBgCentre","varNBgCentre",NBgCentre);
	RooRealVar varNBgError("varNBgError","varNBgError",NBgError);
        RooRealVar NBg("NBg","NBg",0,100);
	RooGaussian pdfNBg("pdfNBg","pdfNBg",NBg,varNBgCentre,varNBgError);
	
	RooRealVar varNEvCentre("varNEvCentre","varNEvCentre",NEvCentre);
	RooRealVar varNEvError("varNEvError","varNEvError",NEvError);
        RooRealVar NEv("NEv","NEv",0,100);
	RooGaussian pdfNEv("pdfNEv","pdfNEv",NEv,varNEvCentre,varNEvError);
	
	
	RooProdPdf pdfNumbers("pdfNumbers", "pdfNumbers", RooArgSet(pdfNBs, pdfNBg, pdfNEv));
	
//	RooDataSet* AmplitudesSet = pdfNumbers.generate(RooArgSet(NBg,NEv),nGenEvents) ;
    
    RooRandom::randomGenerator()->SetSeed(seedNumber*nGenEvents);
    
    RooDataSet* AmplitudesSet = pdfNumbers.generate(RooArgSet(NBg),nGenEvents) ;
	
// 	RooPlot* frame1 = NBs.frame(Title("NBs"));
// 	RooPlot* frame2 = NBg.frame(Title("NBg"));
// 	RooPlot* frame3 = NEv.frame(Title("NEv"));
// 	
// 	AmplitudesSet->plotOn(frame1);
// 	AmplitudesSet->plotOn(frame2);
// 	AmplitudesSet->plotOn(frame3);
// 	
// 	TCanvas* canv1 = new TCanvas("canv1", "canv1", 800, 700);
// 	frame1->Draw();
// 	
// 	TCanvas* canv2 = new TCanvas("canv2", "canv2", 800, 700);
// 	frame2->Draw();
// 	
// 	TCanvas* canv3 = new TCanvas("canv3", "canv3", 800, 700);
// 	frame3->Draw();
  
	
	
	
	
	RooRealVar varMass("B_m_scaled", "B_m_scaled", minMass, maxMass, "MeV");
	varMass.setBins(binN);
	
	RooRealVar varBsNumber("N(B_{s})", "varBsNumber", 30, 0, 1000);
 	RooRealVar varBsSigma("#sigma", "varBsSigma", fitSigma);
	RooRealVar varBsMass("M(B_{s})", "varBsMass", fitMass);
	RooGaussian pdfBsN("pdfBsN", "pdfBsN", varMass, varBsMass, varBsSigma);
	RooRealVar FractionN("FractionN","FractionN",0.853);
	RooRealVar SigmaRatio("SigmaRatio","SigmaRatio",2.2);
	RooFormulaVar varBsSigmaW("varBsSigmaW","varBsSigmaW","@0*@1",RooArgSet(varBsSigma,SigmaRatio));
	RooGaussian pdfBsW("pdfBsW", "pdfBsW", varMass, varBsMass, varBsSigmaW);
	RooAddPdf pdfBs("pdfBs","pdfBs",RooArgList(pdfBsN,pdfBsW),RooArgSet(FractionN));

	RooRealVar varNBgr("varNBgr", "varNBgr", 50, 0, 1e3);
	RooRealVar varE("varE", "varE", 0);
	RooExponential pdfBgr("pdfBgr", "pdfBgr", varMass, varE);
	RooExtendPdf pdfBgrEx("pdfBgrEx", "pdfBgrEx", pdfBgr, varNBgr);
	
	
	RooAddPdf pdfModel("pdfModel", "pdfModel", RooArgList(pdfBs, pdfBgr), RooArgList(varBsNumber, varNBgr));
	
	
	TH1F* histBs_from_Bs = new TH1F("histBs_from_Bs", "histBs_from_Bs", 100, 0, 5);
	TH1F* histBs_from_Bg = new TH1F("histBs_from_Bg", "histBs_from_Bg", 100, 0, 5);
    TH1F* chi2Diff = new TH1F("chi2Diff", "chi2Diff", 200, 0, 40);
	
	
    Double_t bgChi2 = 0;
    Double_t sigChi2 = 0;
    
    Double_t valNBgr = 0;
    Double_t valNEv = 0;
    
    RooFitResult* results;
    RooDataHist* BsHist;
    
	for(int ii=0;ii<nGenEvents;ii++)
	{
	    //Double_t valBsNumber = (Double_t)((RooArgSet*)AmplitudesSet->get(ii))->getRealValue("NBs");
	    valNBgr = (Double_t)((RooArgSet*)AmplitudesSet->get(ii))->getRealValue("NBg");
	    valNEv = (Double_t)((RooArgSet*)AmplitudesSet->get(ii))->getRealValue("NEv");
	    
        
        varBsNumber.setConstant(kTRUE);
	    varBsNumber.setVal(0);
	    varNBgr.setVal(valNBgr);
	    BsHist = pdfModel.generateBinned(varMass,Int_t(valNBgr), Extended());
        
        varBsNumber.setConstant(kFALSE);
	    results = pdfModel.chi2FitTo(*BsHist, Save(true), PrintLevel(0), Extended());

	    
        sigChi2 = results->minNll();
        
        varBsNumber.setConstant(kTRUE);
        varBsNumber.setVal(0);
        results = pdfModel.chi2FitTo(*BsHist, Save(true), PrintLevel(0), Extended());
        
        bgChi2 = results->minNll();

        
        chi2Diff->Fill(bgChi2-sigChi2);

        
        
//        
//	    RooPlot* frameBs = varMass.frame(Title("varMass"));
//	    BsHist->plotOn(frameBs);
//	    pdfModel->plotOn(frameBs);
//	    histBs_from_Bs->Fill(frameBs->chiSquare());
//	    
//	    results = pdfBgr.chi2FitTo(*BsHist, Save(true), PrintLevel(0), Extended());
//	   
//	    RooPlot* frameBg = varMass.frame(Title("varMass"));
//	    BsHist->plotOn(frameBg);
//	    pdfBgr->plotOn(frameBg);
//	    histBs_from_Bg->Fill(frameBg->chiSquare());

	}

    
//    TCanvas* canv5 = new TCanvas("canv5", "canv5", 800, 700);
//    chi2Diff->Draw("E");
//    canv5->SetLogy();
    
    char label[200];
    sprintf(label,"toysBlindExternal/%i.root",seedNumber);
    chi2Diff->SaveAs(label);
    
	
//	TCanvas* canv5 = new TCanvas("canv5", "canv5", 800, 700);
//	RooPlot* frame4 = varMass.frame(Title("varMass"));
//	BsHist->plotOn(frame4);
//	frame4->Draw();
//	canv5->SetLogy();
    
    
//	histBs_from_Bg->Draw();
//	TFile file("triPhiToys.root","recreate");
//	histBs_from_Bs->Write();
//	histBs_from_Bg->Write();
//	file.Close();
}
